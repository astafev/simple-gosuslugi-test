package ru.fcbp.autotests;


import ru.fcbp.autotests.framework.TestControlDescription;
import ru.fcbp.autotests.framework.TestFailedException;
import ru.fcbp.autotests.framework.TestImplementation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class instructions extends TestImplementation {

    public void init() {
        SERVICE_REGION = "33000000000";
        SERVICE_PROD_REGION = "номер продуктивного портала для тестирования на среде PROD";
        SERVICE_CONTEXT = "43";
        SERVICE_ID = "43001";
        SERVICE_TARGET_EXT_ID = "4301000020000050633";
        IS_COPRORATE_ENTITY = false; // данный параметр указывает, является ли услуга предназначенной для юридических лиц (true)
                                     // или для физических лиц (false)
    }

//      Данные для проверки наименований полей, шагов, услуги

    public void setReferenceData() {
        REFERENCE.SERVICE_NAME = "Название услуги";
        REFERENCE.STEPS_LIST = Arrays.asList("Список", "названий", "шагов", "услуги");

        REFERENCE.CONTROLS_LIST = new HashMap<Integer, List<TestControlDescription>>();
        REFERENCE.CONTROLS_LIST.put(1, Arrays.asList( //список полей на форме шага
            new TestControlDescription("Фамилия, имя, отчество (полностью):	#fio	Textfield	+") // заголовок поля #имя_поля тип обязательность(+: да, -: нет)
        ));
        REFERENCE.CONTROLS_LIST.put(2, Arrays.asList( //список полей на форме шага
            new TestControlDescription("адрес #address Textfield -") // заголовок поля #имя_поля тип обязательность(+: да, -: нет)
        ));
    }

    // Последовательность тестовых вызовов
    public void testSteps() {
        REFERENCE.doVerifyControlNames(1);  //метод проверяет наличие на форме шага всех полей, указанных в REFERENCE.CONTROLS_LIST
                                            //должен вызываться сразу после перехода на шаг
                                            //параметр - номер шага
        doNextStep(1); //переход вперед к указанному шагу
        doPreviousStep(1); //переход назад к указанному шагу
        doSendToDepartment(); //отправка заполненной формы в ведомство. Заключительный метод теста

        doClickFindButton();    //нажимает кнопку "найти"
        isResultPresent();      //проверяет наличие результата

        VERIFY.checkStaticText("текст"); //метод проверяет наличие на форме статического текста
        VERIFY.checkFileExtensions("имя поля", "допустимые расширения"); //метод проверяет, что указанное поле загрузки файлов допускает
                                                                         //только загрузку файлов указанного формата
        VERIFY.checkFileLoadField("имя поля", "допустимое расширение"); //метод проверяет возможность загрузки файла в указанное поле
        VERIFY.doFileExtensions("имя поля", "допустимые расширения");   //метод проверяет, что указанное поле загрузки файла допускает только
                                                                        //загрузку файлов указанного формата.
                                                                        //НЕ РЕКОМЕНДОВАН К ИСПОЛЬЗОВАНИЮ
        VERIFY.doIsDisabled("имя поля"); //проверка "поле запрещено для изменения"
        VERIFY.doIsNotVisible("имя поля"); //проверка "поле не видмо на форме"
        VERIFY.doIsReadOnly("имя поля"); // проверка "поле только для чтения"
        VERIFY.doIsRequired("имя поля", true); //проверка "поле обязательно для заполнения" если параметр true  и наоборот
        VERIFY.doIsVisible("имя поля"); //проверка "поле видимо на форме"
        VERIFY.doTypeTextExist("имя поля", "текст"); //метод проверяет, что текстовое поле содержит указанный текст
        VERIFY.isDateCurrent("имя поля"); //метод проверяет, что поле ввода аты содержит текущую дату
        VERIFY.returnCurrentDate(); //метод возвращает текущую дату строкой
        VERIFY.returnCurrentDateDate(); //метод возвращает текущую дату в виде экземпляра Date
        VERIFY.returnDateField(1, 2, 3, 4, 5); //метод возвращает объект Date, параметры - год, месяц, число, час, минута от текущего момента
        VERIFY.typeDateToDateField("имя поля", VERIFY.returnDateField(0, 0, 0, 0, 0)); //метод вносит дату в поле ввода даты.
                                                                                       // параметр - объект Date
        VERIFY.validateDateField("имя поля",VERIFY.returnDateField(-1, 0, 0, 0, 0), VERIFY.returnDateField(1, 0, 0, 0, 0));
                                           //метод проверяет, что поле ввода даты принимает только значения в указанном интервале.
                                           //параметры - объекты типа Date, "с даты 1" и "по дату 2"
        VERIFY.validateDateField("имя поля", -1, 1);  //то же самое, но параметры - годы от текущего момент
        VERIFY.validateDateInterval("имя поля 1", "имя поля 2"); //проверка на то, что дата в поле 1 не может быть больше даты в поле 2 и
                                                                 // дата в поле 2 не может быть меньше даты в поле 1
        VERIFY.validateDateFieldFrom("имя поля", VERIFY.returnDateField(1, 0, 0, 0, 0));
                                                 //проверка на то, что поле не может содержать дату ранее указанной
                                                 //параметр - тип Date
        VERIFY.validateDateFieldFrom("имя поля", "имя ограничивающего поля");   //проверяет, что дата в поле не может быть меньше
                                                                                // даты в ограничивающем поле
        VERIFY.validateDateFieldFrom("имя поля", 0, 0, 0); //проверка на то, что дата в поле не может быть меньше указанной.
                                                           //параметры - годы, месяцы, дни от теккущего момента
        VERIFY.validateDateFieldFromCurrent("имя поля"); // проверка на то, что дата в поле не может быть меньше текущей
        /*
        Аналогичные методы существуют для проверок "не позже". Синтаксис тот же, кроме замены From -> To  в имени метода
         */

        VERIFY.validateTypeText("имя поля", "имя валидатора", 100); //метод проверки текстового поля на ограничения,
                                                                    // установленные в соответствующем валдиторе
                                                                    //числовой параметр - ограничение поля на длину
        VERIFY.validateTypeTextRedexp("имя поля", "регулярное выражение");  //проверяет, что поле допускает только значения,
                                                                            // установленные регулярным выражением
        VERIFY.verificationDefaultOnCheckbox("имя поля", true); //метод проверяет default значение чекбокса на соответсиве заданному
        VERIFY.verificationDefaultOnRadiogroup("имя поля", "имя опции"); //метод проверяет default значение радиогруппы на соответствие указанному


        EDIT.doSetCheckbox("имя поля", true); //выставляет указанное значение указанному чекбоксу
        EDIT.checkboxGroupCheckOneRequired("имя поля"); //проверяет, что в группе чекбоксов хотябы один должен быть заполнен
        EDIT.checkboxGroupSetValues("имя_поля-checkboxGroup", true, "чекбокс 1", "чекбокс 2"); //метод выставляет в группе чекбоксов всем
                                                                                 // перечисленным чекбоксам соотвествующее значение
                                                                                 //true - проставить чекбоксы, false - снять чекбоксы
        EDIT.doCloneAdd("имя поля"); //метод нажимает кнопку "добавить" для клонируемых полей
        EDIT.doCloneDelete("имя поля", 2); //метод удаляет клонированное поле, соответствующее по счету указанному номеру
        EDIT.doSelectFromDictionaryLookup("имя поля", Arrays.asList("опция1", "опция2"));
                                        //метод заполняет справочники-словари

        EDIT.doSetRadiogroup("имя поля", "значение"); //метод устанавливает в радиогруппе указанное значение
        EDIT.doTypeText("имя поля", "текст"); //метод вносит указанный текст в указанное поле
        EDIT.selectAnyFromDictionaryLookup("имя поля", true);
                                        //метод заполняет справочники первым попавшимся значением
                                        //значение параметра - true если справочник является checkboxLookup или yearLookup, false - dictionaryLookup
        EDIT.doTypeTextdoSelectFromCheckboxLookup("имя переменной", Arrays.asList("значение_1","значение_2"));  //установка конкретного значения в виджете CheckboxLookup

        VERIFY.validStyle("текст", "15px");//проверяет размер заголовка

        doCreateDraft();//сохраняет черновики, используется вместо doSendToDepartment(); в юридическом ЛК
        }


}

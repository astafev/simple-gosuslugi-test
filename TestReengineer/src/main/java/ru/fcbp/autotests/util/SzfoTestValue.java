package ru.fcbp.autotests.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: nkornilova
 * Date: 05.04.13
 * Time: 18:51
 * To change this template use File | Settings | File Templates.
 */
public  final class SzfoTestValue {
    private  SzfoTestValue() {

    }

    public final static String NaselenniiPunktL1_Reg35 ="Амурская обл";
    public final static String NaselenniiPunktL2_Reg35 ="Райчихинск г";
    public final static String NaselenniiPunktL3_Reg35 ="Широкий пгт";

    public final static List<String> NaselenniiPunktReg35_3 = Arrays.asList(NaselenniiPunktL1_Reg35, NaselenniiPunktL2_Reg35, NaselenniiPunktL3_Reg35);
    public final static List<String> NaselenniiPunktReg35_2 = Arrays.asList(NaselenniiPunktL2_Reg35, NaselenniiPunktL3_Reg35);
    public final static List<String> NaselenniiPunktReg35_1 = Arrays.asList(NaselenniiPunktL1_Reg35);


    public final static String NaselenniiPunktL1_Reg39 ="Амурская обл";
    public final static String NaselenniiPunktL2_Reg39 ="Райчихинск г";
    public final static String NaselenniiPunktL3_Reg39 ="Широкий пгт";

    public final static List<String> NaselenniiPunktReg39_3 = Arrays.asList(NaselenniiPunktL1_Reg39, NaselenniiPunktL2_Reg39, NaselenniiPunktL3_Reg39);
    public final static List<String> NaselenniiPunktReg39_2 = Arrays.asList(NaselenniiPunktL2_Reg39, NaselenniiPunktL3_Reg39);
    public final static List<String> NaselenniiPunktReg39_1 = Arrays.asList(NaselenniiPunktL1_Reg39);


    public final static String NaselenniiPunktL1_Reg47 ="Амурская обл";
    public final static String NaselenniiPunktL2_Reg47 ="Райчихинск г";
    public final static String NaselenniiPunktL3_Reg47 ="Широкий пгт";

    public final static List<String> NaselenniiPunktReg47_3 = Arrays.asList(NaselenniiPunktL1_Reg47, NaselenniiPunktL2_Reg47, NaselenniiPunktL3_Reg47);
    public final static List<String> NaselenniiPunktReg47_2 = Arrays.asList(NaselenniiPunktL2_Reg47, NaselenniiPunktL3_Reg47);
    public final static List<String> NaselenniiPunktReg47_1 = Arrays.asList(NaselenniiPunktL1_Reg47);


    public final static String NaselenniiPunktL1_Reg51 ="Амурская обл";
    public final static String NaselenniiPunktL2_Reg51 ="Райчихинск г";
    public final static String NaselenniiPunktL3_Reg51 ="Широкий пгт";

    public final static List<String> NaselenniiPunktReg51_3 = Arrays.asList(NaselenniiPunktL1_Reg51, NaselenniiPunktL2_Reg51, NaselenniiPunktL3_Reg51);
    public final static List<String> NaselenniiPunktReg51_2 = Arrays.asList(NaselenniiPunktL2_Reg51, NaselenniiPunktL3_Reg51);
    public final static List<String> NaselenniiPunktReg51_1 = Arrays.asList(NaselenniiPunktL1_Reg51);


    public final static String NaselenniiPunktL1_Reg53 ="Амурская обл";
    public final static String NaselenniiPunktL2_Reg53 ="Райчихинск г";
    public final static String NaselenniiPunktL3_Reg53 ="Широкий пгт";

    public final static List<String> NaselenniiPunktReg53_3 = Arrays.asList(NaselenniiPunktL1_Reg53, NaselenniiPunktL2_Reg53, NaselenniiPunktL3_Reg53);
    public final static List<String> NaselenniiPunktReg53_2 = Arrays.asList(NaselenniiPunktL2_Reg53, NaselenniiPunktL3_Reg53);
    public final static List<String> NaselenniiPunktReg53_1 = Arrays.asList(NaselenniiPunktL1_Reg53);


    public final static String NaselenniiPunktL1_Reg78 ="Амурская обл";
    public final static String NaselenniiPunktL2_Reg78 ="Райчихинск г";
    public final static String NaselenniiPunktL3_Reg78 ="Широкий пгт";

    public final static List<String> NaselenniiPunktReg78_3 = Arrays.asList(NaselenniiPunktL1_Reg78, NaselenniiPunktL2_Reg78, NaselenniiPunktL3_Reg78);
    public final static List<String> NaselenniiPunktReg78_2 = Arrays.asList(NaselenniiPunktL2_Reg78, NaselenniiPunktL3_Reg78);
    public final static List<String> NaselenniiPunktReg78_1 = Arrays.asList(NaselenniiPunktL1_Reg78);


    public final static String NaselenniiPunktL1_Reg83 ="Амурская обл";
    public final static String NaselenniiPunktL2_Reg83 ="Райчихинск г";
    public final static String NaselenniiPunktL3_Reg83 ="Широкий пгт";

    public final static List<String> NaselenniiPunktReg83_3 = Arrays.asList(NaselenniiPunktL1_Reg83, NaselenniiPunktL2_Reg83, NaselenniiPunktL3_Reg83);
    public final static List<String> NaselenniiPunktReg83_2 = Arrays.asList(NaselenniiPunktL2_Reg83, NaselenniiPunktL3_Reg83);
    public final static List<String> NaselenniiPunktReg83_1 = Arrays.asList(NaselenniiPunktL1_Reg83);



}

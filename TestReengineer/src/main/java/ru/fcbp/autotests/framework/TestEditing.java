package ru.fcbp.autotests.framework;

import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static ru.fcbp.autotests.framework.TestImplementation.factory;

/**
 * @author: astaf_000
 * Date: 24.07.13:11:49
 */
enum Types {
    dic,
    text,
    checkbox,
    checkboxLookup,
    radio,
    file,
    button

}

public class TestEditing {
    public static final Date NULL_DATE = new Date();
    public static final DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
    public Step step;

    public void doSelectFromDictionaryLookup(String name, String value) {
        putValue(name, value).setType("dic");
    }

    public void doTypeText(String name, String value) {
        putValue(name, value).setType("text");
    }

    public void doPickDate(String name, Date value) {
        putValue(name, df.format(value)).setType("text");
    }

    public void doSelectFromDictionaryLookup(String name, List<String> strings) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = strings.iterator();
        for (; it.hasNext(); ) {
            String next = it.next();
            sb.append(next);
            if (it.hasNext()) {
                sb.append("/");
            }
        }
        putValue(name, sb.toString()).setType("dic");
    }

    public void doSetCheckbox(String name, boolean b) {
        putValue(name, Boolean.toString(b)).setType("checkbox");
    }

    public void checkboxGroupCheckOneRequired(String s) {
        //do nothing
    }

    public void selectAnyFromDictionaryLookup(String name, boolean b) {
        if (b)
            putValue(name, "").setType("checkboxLookup");
        else putValue(name, "").setType("dic");
    }

    Value putValue(String name, String value) {
/*
        for (Value val : step.getStepValues()) {
            if (val.getName().equals(name)) {
                val.setValue(value);
                return val;
            }
        }
*/
        Value val = factory.createValue();
        val.setName(name);
        val.setValue(value);
        step.getStepValues().add(val);
        return val;
    }

    private Value getValue(String name) {
        for (Value val : step.getStepValues()) {
            if (val.getName().equals(name)) {
                return val;
            }
        }
        Value val = factory.createValue();
        val.setName(name);
        return val;
    }

    public void doCloneAdd(String className) {
        putValue(className + "_add_button", "tr." + className + " span.buttonAddContent").setType("button");
    }

    public void doCloneDelete(String className, int i) {
        Value val = putValue(className + "_delete_button", "tr." + className + " span.buttonAddContent");
        val.setType("button");
        if (i != 0)
            val.setDicVal(Integer.toString(i));
    }

    public void doTypeTextdoSelectFromCheckboxLookup(String name, List<String> values) {
        StringBuilder value = new StringBuilder();
        for (String v : values) {
            value.append(v);
            value.append(" | ");
        }
        value.delete(value.length() - 3, value.length());
        putValue(name, value.toString()).setType("checkboxLookup");
    }

    public void doSetRadiogroup(String name, String value) {
        putValue(name, value).setType("radio");
    }

    public void checkboxGroupSetValues(String name, boolean b, String... values) {
        StringBuilder value = new StringBuilder();
        for (String v : values) {
            if (b) {
                value.append(" | ");
                value.append(v);
            }
        }
        putValue(name, value.toString()).setType("checkboxLookup");
    }

    public void doSetCheckboxIngnoreError(String name, boolean b) {
        doSetCheckbox(name, b);
    }

    public void doPickDate(String name, int i) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DAY_OF_YEAR, i);
        putValue(name, df.format(calendar.getTime())).setType("text");
    }
}

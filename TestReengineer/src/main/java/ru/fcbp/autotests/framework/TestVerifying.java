package ru.fcbp.autotests.framework;

import ru.atc.gosuslugi.minitest.jaxb.values.Step;

import java.util.Date;
import java.util.List;

public class TestVerifying {


    public TestEditing EDIT;
    public String sia_doc_serial = "1111";
    public String sia_doc_number = "111111";
    public String sia_doc_issued_by = "Выдан кем из СИА";
    public String sia_doc_issued_date = "01.01.1990";
    public String sia_lastName = "СИАФамилия";
    public String sia_firstName = "СИАИмя";
    public String sia_middleName = "СИАОтчество";
    public String sia_mail = "sia@mail.com";
    public String sia_register_address = "СИА регистрируй адрес";
    public String sia_inn = "123456789110";
    public String sia_snils = "000-000-000 00";
    public String sia_birthDate = "01.01.1980";
    public String sia_address_reg_index = "000000";
    public String sia_address_reg_city = "СИАГород";
    public String sia_address_reg_street = "СИАУлица";
    public String sia_address_reg_building2 = "0";
    public String sia_address_reg_apartment = "0";
    public String sia_live_address = "СИА живой адрес";
    public String sia_phone_home = "000-00-00";
    public String sia_address_reg_building1 = "0";
    public String sia_address_reg_house = "0";
    public String sia_address_reg_district = "СИАРайон";
    public String sia_address_reg_region = "СИАРегион";
    public String sia_sex = "М";
    public String sia_phone_mobile = "(123)123-123-00";
    public String sia_doc_type = "";
    public String sia_address_reg_town = "Город";
    public String sia_ogrn = "ХУЙ";
    public String sia_full_name = "СИА Полное Имя";

    /**
     * fileExtensions
     * Проверяет форматы загруженных файлов
     * @param control = Имя поля
     * @param extensions = список расширений
     */
    public void doFileExtensions(String control, String extensions) throws TestFailedException {
    }

    /**
     * fileExtensions
     * Проверяет форматы загруженных файлов
     * @param control = Имя поля
     * @param extensions = список расширений
     * @param len = длина файла
     */
    public void doFileExtensions(String control, String extensions, int len) throws TestFailedException {}

    /**
     * Метод сверяет предоставленные расширения файлов для загрузки с допустимыми расширениями, извлеченными из тела Java-скрипта со страницы.
     * @param control - указывает, какой элемент проверять
     * @param extensions - перечень расширений файлов, допустимых для загрузки. При обнаружении недостающих/лишних расширений будет выведено сообщение об ошибке.
     * @author Pavel Tkachev ptkachev@at-consulting.ru
     */
    public void checkFileExtensions(String control, ext... extensions) {
        EDIT.putValue(control, "").setType("file");
    }

    public void checkFileExtensions(String control, String extensionsString) throws TestFailedException {
        EDIT.putValue(control, "").setType("file");
    }



    /**
     * fileWriter
     * Создает файл с данными учитывая длину файла
     * @param ext = расширение
     * @param len = длина файла
     */

    public String fileWriter(String ext, int len){
        return null;
    }

    public void checkFileLoadField(String control, String extensions) throws TestFailedException {
        EDIT.putValue(control, "").setType("file");
    }

    /**
     * verificationDefaultOnCheckbox
     * Проверяет значение по умолчанию для чекбокса
     * @param control = Имя элемента
     * @param checkbox_value = значение по умолчанию true или false
     */
    public void verificationDefaultOnCheckbox(String control, boolean checkbox_value)  throws TestFailedException {
    }

    /**
     * verificationDefaultOnRadiogroup
     * Проверяет значение для Radiogroup
     * @param control = Имя элемента
     * @param value = значение по умолчанию

     */
    public void verificationDefaultOnRadiogroup(String control, String value) throws TestFailedException {
    }

    /**
     * validStyle
     * Проверяет стиль и размер наименования блоков
     * @param name = Имя элемента
     * @param stylePx = стиль заголовка
     */
    public void validStyle(String name, String stylePx) {
    }

    /**
     * doTypeTextExist
     * Проверяет, существует, ли текст
     * @param control = Имя элемента
     * @param text = текст
     */
    public void doTypeTextExist(String control, String text) {
    }

    /**
     * doIsVisible
     * Проверяет контрол на видимость
     * @param control = Имя элемента
     */
    public void doIsVisible(String control) {}

    /**
     * doIsNotVisible
     * Проверяет контрол на видимость
     * @param control = Имя элемента
     */
    public void doIsNotVisible(String control) {
    }

    /**
     * doIsReadOnly
     * Проверяет, контрол на редактируемость
     * @param control = Имя элемента
     */

    public void doIsReadOnly(String control) {
    }

    /**
     * doIsRequired
     * Проверяет обязательность
     * @param control = Имя элемента
     */

    public void doIsRequired(String control, boolean required) throws TestFailedException {
    }


    /**
     * doIsDisabled
     * Проверяет, контрол на disabled
     * @param control = Имя элемента
     */

    public void doIsDisabled(String control) {
    }

    /**
     * validateDictionaryOptions
     * Проверяет наличие опций в словаре. Общее количество опций не проверяет
     * @param checkboxLookup = true если справочник является checkboxLookup, false - dictionaryLookup
     */

    private void validateDictionaryOptions(String control, List<String> options_names, boolean checkboxLookup) throws TestFailedException {
    }

    /**
     * validateTypeText
     * Проверяет наличие корректных ограничений по полю
     * @param control = Имя поля
     * @param validation_method = тип валидации
     * @param maxlength = длина символов
     */
    public void validateTypeText(String control, String validation_method, Integer maxlength) throws TestFailedException {
    }

    public void validateTypeTextRedexp(String control, String regexp) {
    }

    /**
     * validateDateFieldFrom
     * Проверяет что дата не может быть меньше указанной
     * @param control = Имя поля
     * @param date_from = предел слева
     */
    public void validateDateFieldFrom(String control, Date date_from) {
    }

    /**
     * validateDateFieldTo
     * Проверяет что дата не может быть больше указанной
     * @param control = Имя поля
     * @param adate_to = предел справа
     */

    public void validateDateFieldTo(String control, Date adate_to) {
    }

    /**
     * validateDateField
     * Проверяет что дата не может быть больше date_to и меньше date_from
     * @param control = Имя поля
     * @param date_from = предел слева
     * @param date_to = предел справа
     */

    public void validateDateField(String control, Date date_from, Date date_to) throws TestFailedException {
    }

    /**
     * validateDateField
     * Проверяет что дата не может быть больше years_to и меньше years_from
     * @param control = Имя поля
     * @param years_from = год начала
     * @param years_to = год окончания
     */

    public void validateDateField(String control, int years_from, int years_to) throws TestFailedException {
    }

    /**
     * validateDateInterval
     * Проверяет что control_from не больше control_to
     * @param control_from = Имя поля
     * @param control_to = предел слева
     */

    public void validateDateInterval(String control_from, String control_to) throws TestFailedException {}


    public void validateDateFieldFrom(String control, int yearsFromToday, int monthsFromToday, int daysFromToday) {
    }

    public void validateDateFieldTo(String control, int yearsFromToday, int monthsFromToday, int daysFromToday) {
    }

    /**
     * Метод проверяет, что в поле ввода даты нельзя ввести дату больше текущей
     * @param control имя поля ввода даты
     */
    public void validateDateFieldToCurrent(String control) {
    }

    public void isDateCurrent(String control) {
    }

    /**
     * Метод проверяет, что в поле ввода даты нельзя ввести дату меньше текущей
     * @param control имя поля ввода даты
     */
    public void validateDateFieldFromCurrent(String control) {
    }

    public void validateDateFieldFrom(String control, String limitingControl) {
    }

    public void validateDateFieldTo(String control, String limitingControl) {
    }

    /**
     * returnDateField
     * Возвращает переменную типа Date
     * @param year = год
     * @param month = месяц
     */

    public Date returnDateField(int year, int month, int day, int hour, int minute) throws TestFailedException {
        return null;
    }

    public String returnCurrentDate(){
        return null;
    }

    public Date returnCurrentDateDate(){
        return null;
    }

    public String typeDateToDateField(String control, Date date) {
        return null;    }

    public void checkStaticText(String text) throws TestFailedException {
    }


    public enum ext {
        PDF ("pdf"),
        DOC ("doc"),
        DOCX ("docx"),
        XLS ("xls"),
        XLSX ("xlsx"),
        JPG ("jpg"),
        PNG ("png"),
        TIF ("tif"),
        MDI ("mdi"),
        TXT ("txt"),
        RTF ("rtf"),
        ODT ("odt");

        private final String extension;

        ext(String extension) {
            this.extension = extension;
        }

        public String toString() {
            return this.extension;
        }
    }
}

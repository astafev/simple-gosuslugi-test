package ru.fcbp.autotests.framework;

import ru.atc.gosuslugi.minitest.jaxb.values.Value;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: astaf_000
 * Date: 24.07.13:11:47
 */
public class TestControlDescription {
    public static final Map<String, String> map = new HashMap<String, String>();

    static {
        /*                            <xs:enumeration value="text"/>
                            <xs:enumeration value="radio"/>
                            <xs:enumeration value="dic"/>
                            <xs:enumeration value="checkbox"/>
                            <xs:enumeration value="file"/>
                            <xs:enumeration value="button"/>*/
        map.put("dictionaryLookup", "dic");
        map.put("dictionarylookup", "dic");
        map.put("staticlookup", "dic");
        map.put("customLookup", "dic");
        map.put("customlookup", "dic");
        map.put("Customlookup", "dic");
        map.put("CustomLookup", "dic");
        map.put("radiogroup", "radio");
        map.put("staticLookup", "dic");

        map.put("checkboxLookup", "checkboxLookup");

        map.put("textarea", "text");
        map.put("textfield", "text");
        map.put("textfield", "text");
        map.put("Textfield", "text");

        map.put("datefield", "text");
        map.put("Datetime-picker", "text");
        map.put("datetime-picker", "text");
        map.put("datetimepicker", "text");

        map.put("checkbox", "checkbox");
        map.put("сheckbox", "checkbox");    //какой-то мудак пишет с русской буквой с
        map.put("checkboxGroup", "checkbox");

        map.put("fileload", "file");

        map.put("fldsetlabel", "label");

    }

    Value value;
    String desc;
    String type;
    String name;
    public static int i = 0;

    public TestControlDescription(String s) {
        try {
            int i = s.indexOf("#");

        desc = s.substring(0, i).trim();
        String[] tail = s.substring(i).split("\\s");
        int j = 1;
        name = tail[0].trim().substring(1);
        while (tail[j] == null || tail[j].isEmpty()) {
            j++;
        }
        type = map.get(tail[j].trim());

        if (type == null) {
            System.err.println("Unknown type of input: " + tail[j] + "\nname: " + name);
            try {
                throw new RuntimeException();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            i++;
        }
        } catch (Exception e) {
            System.err.println(s);
            e.printStackTrace();
        }
//        type = map.get(tail[1].trim());
    }

    public Value getValue() {
        if (value == null) {
            value = TestImplementation.factory.createValue();
            value.setType(type);
            value.setType(desc);
            value.setType(name);
        }
        return value;
    }

    public void fillValue(Value value) {
        if (!value.getName().equals(name)) {
            throw new RuntimeException("different names " + value.getName() + " " + name);
        }
        value.setDescription(desc);
        //to_think Надо ли вообще здесь че-то с типами делать?
        if ((value.getType().equals("text") || value.getType().isEmpty()) && type != null)
            value.setType(type);
    }
}

package ru.fcbp.autotests.framework;

import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.jaxb.values.ValuesType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author astaf_000
 * Date: 24.07.13:11:48
 */
public class TestReference {
    public String SERVICE_NAME;
    public String PAGE_NAME;
    public List<String> STEPS_LIST;
    public Map CONTROLS_LIST = new HashMap<String, List<TestControlDescription>>();

    public void doVerifyControlNames(int i) {
        //do nothing
    }

    public void fillValues(ValuesType values) {
        fillValues(values.getSteps());
    }
    public void fillValues(List<Step> steps) {
        for(Step step: steps) {
            List<TestControlDescription> tcdList = (List<TestControlDescription>) CONTROLS_LIST.get(Integer.valueOf(step.getNumStep()));
            for(Value value : step.getStepValues()) {
                for(TestControlDescription tcd : tcdList) {
                    if(value.getName().equals(tcd.name)) {
                        tcd.fillValue(value);
                        break;
                    }
                }
            }
        }
    }

    public void doVerifyServiceName(String s) {
        //To change body of created methods use File | Settings | File Templates.
    }
}

package ru.fcbp.autotests.framework;

import ru.atc.gosuslugi.minitest.jaxb.values.*;

import java.util.List;

/**
 * @author: astaf_000
 * Date: 24.07.13:11:34
 */
public abstract class TestImplementation {
    public static final ObjectFactory factory = new ObjectFactory();
    // параметры текущей услуги
    public String SERVICE_REGION;
    public String SERVICE_CONTEXT;
    public String SERVICE_ID;
    public String SERVICE_TARGET_EXT_ID;
    public String SERVICE_PROD_REGION = "";
    public boolean IS_COPRORATE_ENTITY = false;
    // классы доступа к реализации функционала
    public TestReference REFERENCE = new TestReference();
    public TestEditing EDIT = new TestEditing();
    public TestVerifying VERIFY = new TestVerifying();
    Service service = factory.createService();
    Step step = factory.createStep();

    public TestImplementation() {
        EDIT.step = step;
        VERIFY.EDIT = EDIT;
        step.setNumStep((byte) 1);
    }

    /**
     * заполняются параметры услуги, вызываем вначале
     */
    public abstract void init();

    public abstract void testSteps();

    /**
     * заполняет всякие
     * REFERENCE.CONTROLS_LIST
     * REFERENCE.SERVICE_NAME
     * REFERENCE.CONTROLS_LIST
     */
    public abstract void setReferenceData();

    public void doNextStep(int nextStep) {
        if (service.getServiceValues() == null) {
            service.setServiceValues(factory.createValuesType());
        }
        service.getServiceValues().getSteps().add(step);
        step = factory.createStep();
        step.setNumStep((byte) nextStep);
        EDIT.step = step;
//        step.setDescription(REFERENCE.STEPS_LIST.get(nextStep));
    }

    public void doPreviousStep(int stepNum) {
        EDIT.putValue("submitBack", "").setType("button");
        doNextStep(stepNum);
    }

    public Step getStep() {
        return step;
    }

    public Service getService() {
        return service;
    }

    public Service doSendToDepartment() {
        if (service.getServiceValues() == null) {
            service.setServiceValues(factory.createValuesType());
        }
        service.getServiceValues().getSteps().add(step);
        List<Step> steps = service.getServiceValues().getSteps();
        for (Step step : steps) {
            List<TestControlDescription> tcdList = (List<TestControlDescription>) REFERENCE.CONTROLS_LIST.get(new Integer(step.getNumStep()));
            if(tcdList!=null)
                for(TestControlDescription tcd:tcdList) {
                    for(Value val:step.getStepValues()) {
                        if(val.getName().equals(tcd.name))
                            tcd.fillValue(val);
                    }
                }
            step.setDescription(REFERENCE.STEPS_LIST.get(step.getNumStep()-1));
        }
        return service;
    }

    public void doWait(int i) {
        //do nothing
    }

    public void doCreateDraft()//сохраняет черновики, используется вместо doSendToDepartment(); в юридическом ЛК
    {
    }

    public void doClickFindButton() {
    }    //нажимает кнопку "найти"

    public void isResultPresent() {
    }      //проверяет наличие результата

}

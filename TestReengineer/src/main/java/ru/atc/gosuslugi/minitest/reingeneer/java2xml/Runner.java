package ru.atc.gosuslugi.minitest.reingeneer.java2xml;

import org.reflections.Reflections;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;
import ru.fcbp.autotests.framework.TestImplementation;

import javax.xml.bind.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

public class Runner {
    public static final String[] regions = new String[]{
            "reg66"
    };
    public static Collection<String> tests = new LinkedHashSet<String>();

    static {
        tests.add("reg66.t66087_0");
        tests.add("reg66.t66035_0");
        tests.add("reg66.t66034_0");
    }

    public static Services ss = TestImplementation.factory.createServices();

    public static void main(String[] args) throws Exception {
        for (String reg : regions) {
            Reflections reflections = new Reflections("ru.fcbp.autotests." + reg);
            Set<Class<? extends TestImplementation>> tiSet = reflections.getSubTypesOf(TestImplementation.class);
            String[] strings = new String[tiSet.size()];
            Iterator<Class<? extends TestImplementation>> iter = tiSet.iterator();
            for(int i = 0; iter.hasNext();i++) {
                strings[i] = iter.next().getName();
            }
            Arrays.sort(strings);
            for (String className:strings)
                test((Class<? extends TestImplementation>) Class.forName(className));

            JAXBContext jaxbContext = JAXBContext
                    .newInstance("ru.atc.gosuslugi.minitest.generated.values");
//        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Marshaller marsh = jaxbContext.createMarshaller();
//        Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(source);
//        unmarshaller.setSchema(schema);
            OutputStream file = new BufferedOutputStream(new FileOutputStream(reg + ".xml"));
            marsh.marshal(TestImplementation.factory.createServices(ss), file);
            System.out.println("\n----------------" + reg + "---------------");
            ss = TestImplementation.factory.createServices();
            file.close();
        }
    }

    public static void test(Class<? extends TestImplementation> clazz) throws NoSuchMethodException, IllegalAccessException, InstantiationException, JAXBException {
        if (!(TestImplementation.class.isAssignableFrom(clazz))) {
            throw new RuntimeException("Unknown class" + clazz);
        }
        //TODO
        TestImplementation ti = clazz.newInstance();
        ti.init();
        ti.setReferenceData();
        ti.testSteps();
        Service service = ti.getService();
        service.setUrl("https://ufo-service-dev.egov.at-consulting.ru/" + ti.SERVICE_CONTEXT +
                "/services/s" + ti.SERVICE_ID + "/init?serviceTargetExtId=" + ti.SERVICE_TARGET_EXT_ID +
                "&userSelectedRegion=" + ti.SERVICE_REGION
        );
        service.setServiceid(String.valueOf(Integer.valueOf(ti.SERVICE_ID)));
        service.setTitle(ti.REFERENCE.SERVICE_NAME);

        if(service.getServiceValues()!=null && service.getServiceValues().getSteps()!=null && !service.getServiceValues().getSteps().isEmpty())
        ss.getServices().add(service);
        else{
            System.err.println("\n!!!!!!!!!!!!!!Unsupported service " + clazz.getName() + "\n");
        }
    }

}

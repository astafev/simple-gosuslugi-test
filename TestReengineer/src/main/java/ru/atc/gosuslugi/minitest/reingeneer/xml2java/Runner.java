package ru.atc.gosuslugi.minitest.reingeneer.xml2java;

import org.xml.sax.SAXException;
import ru.atc.gosuslugi.minitest.jaxb.values.ObjectFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;
import ru.atc.gosuslugi.minitest.util.ConfigUtils;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.net.URISyntaxException;

/**
 * Created by Евгений on 21.04.2014.
 */
public class Runner {
    public static final String USAGE =
            "You can choose to generate from single file or from directory\n" +
                    "-f <file>" +
                    "\n    xml file or directory with services" +
                    "-o <out directory>" +
                    "\n    directory with xml files";

    public static void main(String[] args) throws IOException, JAXBException, SAXException, URISyntaxException {
        File input = null;
        File outDir = null;
        for (int i = 0; i < args.length; i++) {
            try {
                switch (args[i]) {
                    case "-f":
                        i++;
                        input = new File(args[i]);
                    case "-o":
                        i++;
                        outDir = new File(args[i]);
                    default:
                        System.err.println("Unknown argument."+args[i] + " USAGE:\n" + USAGE);
                        return;
                }
            } catch (IndexOutOfBoundsException e) {
                System.err.println("Wrong usage!\n" + USAGE);
                return;
            }
        }
        if (input == null) {
            input = new File("in");
        }
        if (outDir == null) {
            outDir = new File("out");
        }

        if (!input.exists()) {
            System.err.println("can't find input file/directory " + input.getName());
            return;
        }
        if (outDir.exists() && !outDir.isDirectory()) {
            System.err.println("can't find out directory " + input.getName());
            return;
        }
        if (!outDir.exists()) {
            outDir.mkdir();
        }

        Services services = readServices(input);
        for (Service service : services.getServices()) {
            if (service.getUrls() != null && service.getUrls().getUrl().size() > 0) {
                service.setUrl(service.getUrls().getUrl().get(0));
            }
            if(service.getServiceid().equals("unknown")) {
                service.setServiceid(service.getUrl().split("/")[5].substring(1));
            }
            File file = new File(outDir, String.format("t%s.java", service.getServiceid()));
            JavaTestGenerator javaTestGenerator = new JavaTestGenerator();
            Writer fileWriter = new BufferedWriter(new FileWriter(file));
            try {
                javaTestGenerator.generate(service, fileWriter);
            } catch (Exception e) {
                System.err.println(service.getServiceid());
                e.printStackTrace();
            } finally {
                System.out.println("saving " + service.getServiceid() + " to " + file.getAbsolutePath());
                fileWriter.close();
            }
        }
        System.out.println("DONE");
    }

    public static Services readServices(File input) throws FileNotFoundException, JAXBException, SAXException {
        if (input.isDirectory()) {
            File[] files = input.listFiles();
            Services services = new ObjectFactory().createServices();
            for (File file : files) {
                services.getServices().addAll(ConfigUtils.readServices(new FileInputStream(file), true).getServices());
            }
            return services;
        } else {
            return ConfigUtils.readServices(new FileInputStream(input), true);
        }
    }
}

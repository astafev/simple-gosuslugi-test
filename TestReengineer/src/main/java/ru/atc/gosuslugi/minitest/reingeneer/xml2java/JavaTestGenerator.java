package ru.atc.gosuslugi.minitest.reingeneer.xml2java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;

import java.io.PrintWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;

public class JavaTestGenerator {

    public static Logger log = LoggerFactory.getLogger(JavaTestGenerator.class);

    public static final String INPUTS =
            "import java.util.*;\n" +
                    "import ru.fcbp.autotests.framework.*;\n";

    URI uri;

    public void generate(Service service, Writer writer) throws Exception {
        PrintWriter printWriter = new PrintWriter(writer);

        String region = getRegion(service);
        String format = "package ru.fcbp.autotests.%s;%n";
        if (region.startsWith("r")) {
            region = "reg" + region.substring(1);
        }
        printWriter.printf(format, region);
        printWriter.println(INPUTS);

        printWriter.println(String.format("public class t%s extends TestImplementation {", service.getServiceid()));

        printInit(service, printWriter);
        printSetReferenceData(service, printWriter);
        printTestSteps(service, printWriter);
        printWriter.print("}");
    }

    private void printSetReferenceData(Service service, PrintWriter writer) throws URISyntaxException {
        writer.println("    @Override");
        writer.println("    public void setReferenceData() {");
        writer.println("    }");
    }

    private void printTestSteps(Service service, PrintWriter writer) throws URISyntaxException, ParseException {
        writer.println("    @Override");
        writer.println("    public void testSteps() {");
        for (int i = 0; i < service.getServiceValues().getSteps().size(); i++) {
            Step step = service.getServiceValues().getSteps().get(i);
            if (i != 0) {
                writer.printf("%n        doNextStep(%d);%n", step.getNumStep());
            }
            for (Value value : step.getStepValues()) {
                writer.print("        ");
                InstructionMapper.printJavaInstruction(value, writer);
            }
        }
        writer.println("        doSendToDepartment();\n    }");
    }

    private void printInit(Service service, PrintWriter writer) throws URISyntaxException {
        writer.println("    public void init() {");
        if (uri.getQuery() == null)
            throw new IllegalStateException("unsupported link format");
        String userSelectedRegion = getParameter("userSelectedRegion", uri.getQuery());

        if (userSelectedRegion == null) {
            log.warn("Can't determine user selected region from uri");
        } else {
            writer.printf("        SERVICE_REGION = \"%s\";%n", userSelectedRegion);
        }
        writer.printf("        SERVICE_CONTEXT = \"%s\";%n", getRegion(service));
        writer.printf("        SERVICE_ID = \"%s\";%n", service.getServiceid());
        writer.printf("        SERVICE_TARGET_EXT_ID = \"%s\";%n", getParameter("serviceTargetExtId", uri.getQuery()));
        writer.println("    }");
    }

    public static String getParameter(String name, String query) {
        String[] params = query.split("&");

        for (String param : params) {
            if (param.contains(name))
                return param.substring(param.indexOf("=") + 1);
        }
        return null;
    }


    private String getRegion(Service service) throws URISyntaxException {
        if (this.uri == null)
            this.uri = new URI(service.getUrl());
        String part = uri.getPath().split("/")[1];
        try {
            Integer.parseInt(part);
            return "r" + part;
        } catch (NumberFormatException e) {
            return part;
        }
    }
}

package ru.atc.gosuslugi.minitest.reingeneer.xml2java;

import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.util.ValueUtils;
import ru.atc.gosuslugi.minitest.web.portal.InputType;

import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InstructionMapper {

    static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static void printJavaInstruction(Value value, PrintWriter writer) throws ParseException {

        switch (InputType.valueOf(value.getType())) {
            case text:
                writer.printf("EDIT.doTypeText(\"%s\", \"%s\");%n", value.getName(), value.getValue().trim());
                break;
            case date:
                Long delta;
                Date date = ValueUtils.stringToDate(value.getValue());
                delta = (date.getTime() - System.currentTimeMillis()) / 1000 / 60 / 60 / 24;
                writer.printf("EDIT.doPickDate(\"%s\", %d);%n", value.getName(), delta);
                break;
            case checkbox:
                writer.printf("EDIT.doSetCheckbox(\"%s\", %s);%n", value.getName(), "on".equals(value.getValue()));
                break;
            case checkboxGroup:
                writer.printf("EDIT.doSetCheckbox(\"%s\"", value.getName());
                for (String item : value.getValue().split("\\|")) {
                    writer.printf(", \"%s\"", item);
                }
                writer.println(");%n");
                break;
            case fieldDropDown:
            case dic:
            case staticDic:
                writer.printf("EDIT.doSelectFromDictionaryLookup(\"%s\", \"%s\");%n", value.getName(), value.getValue().trim());
                break;
            case hierarchyDic:
                writer.printf("EDIT.doSelectFromDictionaryLookup(\"%s\", Arrays.asList(", value.getName());
                for (String item : value.getValue().split("\\\\")) {
                    writer.printf(", \"%s\"", item);
                }
                writer.println("));%n");
                break;
            case radio:
                if (value.getValue().trim().isEmpty())
                    writer.printf("EDIT.doSetRadiogroup(\"%s\", \"%s\");%n", value.getName(), value.getValue().trim());
                else writer.printf("EDIT.doSetRadiogroup(\"%s\", \"%s\");%n", value.getName(), value.getValue().trim());
                break;
            case file:
                String fileEx = "pdf";
                if (value.getValue() != null && !value.getValue().isEmpty() && value.getValue().split("\\.").length > 0) {
                    fileEx = value.getValue().split("\\.")[value.getValue().split("\\.").length - 1];
                }
                writer.printf("VERIFY.checkFileLoadField(\"%s\",\"%s\");%n", value.getName(), fileEx);
                break;
            default:
                throw new UnsupportedOperationException("not yet implemented " + value.getType());
        }
    }
}

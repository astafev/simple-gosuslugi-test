package ru.atc.gosuslugi.minitest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.ComplexValue;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.web.portal.InputType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Date: 11.11.13
 * Author: astafev
 */
public class ValueUtils {
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    public static final Pattern P_DATE_FORMAT = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}");
    private static Logger log = LoggerFactory.getLogger(ValueUtils.class);

    public static String dateToString(Date val) throws ParseException {
        return DATE_FORMAT.format(val);
    }
    public static Date stringToDate(String val) throws ParseException {
        if (val == null || val.isEmpty()) {
            return new Date();
        }
        Matcher matcher = P_DATE_FORMAT.matcher(val);
        if (matcher.matches())
            return DATE_FORMAT.parse(val);
        List<String> vals = Arrays.asList(val.split("\\s*"));
        Calendar calendar = new GregorianCalendar();
        Iterator<String> iter = vals.iterator();
        while (iter.hasNext()) {
            String part = iter.next();
            if (P_DATE_FORMAT.matcher(part).matches()) {
                calendar.setTime(DATE_FORMAT.parse(part));
                iter.remove();
            }
        }

        iter = vals.iterator();
        while (iter.hasNext()) {
            String part = iter.next();
            int addVal = Integer.valueOf(part.substring(0, part.length() - 2), 10);
            String s = part.substring(part.length() - 2);
            if (s.equals("s")) {
                calendar.add(Calendar.SECOND, addVal);
            } else if (s.equals("m")) {
                calendar.add(Calendar.MINUTE, addVal);
            } else if (s.equals("h")) {
                calendar.add(Calendar.HOUR, addVal);
            } else if (s.equals("d")) {
                calendar.add(Calendar.DAY_OF_MONTH, addVal);
            } else if (s.equals("M")) {
                calendar.add(Calendar.MONTH, addVal);
            } else if (s.equals("y")) {
                calendar.add(Calendar.YEAR, addVal);
            } else if (s.equals("w")) {
                calendar.add(Calendar.WEEK_OF_YEAR, addVal);
            } else throw new ParseException("Unknown type " + s, part.length() - 2);
        }
        return calendar.getTime();
    }


    public static ComplexValue parseKladr(Value value) {
        //TODO

        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    /**
     * Как-то херово получается. потому что передается все без разбора в одной строке:<br/>
     * "620000, Свердловская обл., Район, Город, Внутригородской район, Населенный пункт,
     * Улица, Доп. территория, Улица на доп. территории, д. Дом, стр. Корпус, корп. Строение, кв. Квартира"
     */
    public static ComplexValue generateFiasValueFromAddressString(String addressString, ComplexValue value) {
        log.debug("Parsing fias string: " + addressString);
        if (value == null) {
            value = new ComplexValue();
            value.setType(InputType.fias.name());
        }
        String[] parts = addressString.split(", ?");

        List<String> list = Arrays.asList(parts);

        Iterator<String> iterator = list.iterator();
        value.setPost_index(iterator.next());
        value.setRegion(iterator.next());
        value.setAddressString(value.getRegion());

        List<String> middle = new LinkedList<String>();

        while (iterator.hasNext()) {
            String next = iterator.next().trim();
            if (next.startsWith("д. ")) {
                value.setHouse(next.substring(3));
                break;
            } else middle.add(next);

        }

        while (iterator.hasNext()) {
            String next = iterator.next().trim();
            if (next.startsWith("д. ")) {
                value.setHouse(next.substring(3));
                continue;
            }
            if (next.startsWith("стр. ")) {
                value.setBuilding1(next.substring(5));
                continue;
            }
            if (next.startsWith("корп. ")) {
                value.setBuilding2(next.substring(6));
                continue;
            }
            if (next.startsWith("кв. ")) {
                value.setApartment(next.substring(4));
                continue;
            }
        }

        //костылина
        switch (middle.size()) {
            case 1:
                value.setTown(middle.get(0));
                break;
            case 2:
                value.setCity(middle.get(0));
                value.setStreet(middle.get(1));
                break;
            case 3:
                value.setDistrict(middle.get(0));
                value.setCity(middle.get(1));
                value.setStreet(middle.get(2));
                break;
            case 4:
                value.setDistrict(middle.get(0));
                value.setCity(middle.get(1));
                value.setTown(middle.get(2));
                value.setStreet(middle.get(3));
                break;
            default:
                Iterator<String> iter = middle.iterator();
                while (iter.hasNext()) {
                    String next = iter.next();
                    for (String prop : ComplexValue.fiasOrder) {
                        try {
                            if (ReflectUtils.getterMethod(prop, ComplexValue.class).invoke(value) == null) {
                                ReflectUtils.setterMethod(prop, ComplexValue.class).invoke(value, next);
                                break;
                            }
                        } catch (ReflectiveOperationException e) {
                            //Impossible error
                            throw new RuntimeException(e);
                        }
                    }
                }
                break;
        }



        return value;
    }

    public static ComplexValue parseFIAS(Value value) {
        return generateFiasValueFromAddressString(value.getValue(), null);
    }
}

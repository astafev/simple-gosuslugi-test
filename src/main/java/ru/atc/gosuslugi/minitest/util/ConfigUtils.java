package ru.atc.gosuslugi.minitest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import ru.atc.gosuslugi.minitest.jaxb.config.Config;
import ru.atc.gosuslugi.minitest.jaxb.config.Environment;
import ru.atc.gosuslugi.minitest.jaxb.config.ValueFile;
import ru.atc.gosuslugi.minitest.jaxb.values.After;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.run.Starter;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.util.*;


public class ConfigUtils {
    public static final Set<Environment> ENVIRONMENTS = new LinkedHashSet<Environment>();

    static {
        Environment UFO = new Environment();
        UFO.setLogin("000-000-000 00");
        UFO.setName("UFO");
        UFO.setPartOfURL("egov.at-consulting.ru");
        UFO.setPassword("11111");
        ENVIRONMENTS.add(UFO);

        Environment UAT = new Environment();
        UAT.setLogin("000-000-000 00");
        UAT.setName("UAT");
        UAT.setPartOfURL("demo gosuslugi.ru");
        UAT.setPassword("11111");
        ENVIRONMENTS.add(UAT);

        /*Environment PROD = new Environment();
        PROD.setLogin("000-000-000 29");
        PROD.setName("PROD");
        PROD.setPartOfURL("!demo gosuslugi.ru");
        PROD.setPassword("kuzin1975");
        ENVIRONMENTS.add(PROD);*/
    }

    public static final Logger log = LoggerFactory.getLogger(ConfigUtils.class);
    public static final String usage = "Usage:\nPreferred way is to set system properties.\n" +
            "possible command line arguments:\n" +
            "    -configFile <fileName>\n";
    public static List<String> ssFiles = new LinkedList<String>();
    public static Config config;



    public static void readConfig() throws JAXBException, SAXException {
        String fileName = System.getProperty("configFile", "config.xml");
        InputStream is = ConfigUtils.class.getClassLoader().getResourceAsStream(fileName);
        try {
            if (is.available() > 0)
                readConfig(fileName);
        } catch (IOException e) {
            log.warn("Couldn't read config file '" + fileName + "'");
        } catch (NullPointerException e) {
            log.warn("Didn't find config file '" + fileName + "'");
        }

    }
    public static void readConfigAndServices() throws JAXBException, SAXException {
        readConfig();
        if (ssFiles.isEmpty()) {
            ssFiles.add(System.getProperty("values.xml", "values.xsd.xml"));
        }
        for (String file : ssFiles) {
            try {
                Services ss = readServices(new BufferedInputStream(ConfigUtils.class.getClassLoader().getResourceAsStream(file)), true);
                for(Service service:ss.getServices()) {
                    String error = service.checkCorrectness();
                    if(error!=null) {
                        throw new IllegalStateException(error);
                    }
                }
                if (Starter.services == null)
                    Starter.services = ss;
                else
                    Starter.services.getServices().addAll(ss.getServices());
            } catch (FileNotFoundException e) {
                log.error("Didn't find file with services '" + file + "'!");
            }
        }

    }

    /**
     * Читает конфиг файл, ставит соотв системные переменные и в классе Starter добавляет Services
     */
    public static void readConfig(String configFile) throws JAXBException, SAXException {
        log.debug("Reading config...");
        String xsdFileName = System.getProperty("config.xsd", "config.xsd");
        InputStream xsdInputStream = new BufferedInputStream(ConfigUtils.class.getClassLoader().getResourceAsStream(xsdFileName));
        Source source = new StreamSource(xsdInputStream);

        JAXBContext jaxbContext = JAXBContext
                .newInstance("ru.atc.gosuslugi.minitest.jaxb.config");
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(source);
        unmarshaller.setSchema(schema);

        JAXBElement<?> element = (JAXBElement<?>) unmarshaller
                .unmarshal(new BufferedInputStream(ConfigUtils.class.getClassLoader().getResourceAsStream(configFile)));
        //TODO
        Config config = (Config) element.getValue();
        ConfigUtils.config = config;
        if (!(config.getNumberOfThreads() == null) && System.getProperty("numberOfThreads") == null) {
            System.setProperty("numberOfThreads", config.getNumberOfThreads().toString());
        }
        if (config.getEnvironments() != null && config.getEnvironments().getEnvironment() != null)
            for (Environment environmentsInConfig : config.getEnvironments().getEnvironment()) {
                if (ENVIRONMENTS.contains(environmentsInConfig)) {
                    for (Iterator<Environment> it = ENVIRONMENTS.iterator(); it.hasNext(); ) {
                        Environment envInCollection = it.next();
                        if (environmentsInConfig.equals(envInCollection)) {
                            if (environmentsInConfig.getLogin() != null && !environmentsInConfig.getLogin().isEmpty()) {
                                envInCollection.setLogin(environmentsInConfig.getLogin());
                            }
                            if (environmentsInConfig.getPartOfURL() != null && !environmentsInConfig.getPartOfURL().isEmpty()) {
                                envInCollection.setPartOfURL(environmentsInConfig.getPartOfURL());
                            }
                            if (environmentsInConfig.getPassword() != null && !environmentsInConfig.getPassword().isEmpty()) {
                                envInCollection.setPassword(environmentsInConfig.getPassword());
                            }
                            break;
                        }
                    }
                } else {
                    ENVIRONMENTS.add(environmentsInConfig);
                }
            }
        for (ValueFile valueFile : config.getValueFiles().getValueFile())
            for (int i = 0; i < valueFile.getNumOfRuns(); i++)
                ssFiles.add(valueFile.getValue());
        if (config.getImplicitWaitPeriod() != null) {
            System.setProperty("webdriver.implicitWaitPeriod", config.getImplicitWaitPeriod().toString());
        }

        if (config.getWebdriver() != null) {
            System.setProperty("webdriver", config.getWebdriver());
        }

        if (config.getBeforeAfterActionTimeout() != null) {
            System.setProperty("ru.atc.beforeAfterWaitPeriod", config.getBeforeAfterActionTimeout().toString());
        }

        if (config.getsFinQueueStatusWaitPeriod() != null) {
            System.setProperty("ru.atc.SF.waitPeriod:Queue_status", config.getsFinQueueStatusWaitPeriod().toString());
        }

        if (config.getLoadingFile() != null && !config.getLoadingFile().isEmpty()) {
            System.setProperty("loadingFile", config.getLoadingFile());
        }

        if (config.getWebdriverChromeDriver() != null && !config.getWebdriverChromeDriver().isEmpty()) {
            File file = new File(config.getWebdriverChromeDriver());
            if(!file.exists() || file.isDirectory()) {
                throw new IllegalStateException("Не могу найти драйвер для хрома по адресу " + config.getWebdriverChromeDriver());
            } else System.setProperty("webdriver.chrome.driver", config.getWebdriverChromeDriver());
        }

        if (config.getSIR() != null) {
            System.setProperty("ru.atc.SIR_URL", config.getSIR().getValue());
        }

        if (!config.getSIR().getType().equalsIgnoreCase("PROD")) {
            System.setProperty("ru.atc.SIR_TYPE", config.getSIR().getType());
        }

        Iterator<String> iter = Arrays.asList(config.getAllowedStatuses().split("\\|")).iterator();
        while (iter.hasNext()) {
            String status = iter.next();
            if (status != null && !status.isEmpty())
                PortalDriver.allowedStatuses.add(status.trim());
        }


        if (config.getFirefoxPath() != null && config.getFirefoxPath().length() > 0) {
            System.setProperty("firefoxPath", config.getFirefoxPath());
        }
        if (System.getProperty("firefoxProfile")==null && config.getFirefoxProfile() != null && !config.getFirefoxProfile().isEmpty()) {
            File profDir = new File(config.getFirefoxProfile());
            if(!profDir.exists() && !profDir.isDirectory())
                throw new IllegalStateException("Can't find directory with FirefoxProfile " + profDir.getAbsolutePath());
            System.setProperty("firefoxProfile", config.getFirefoxProfile());
        }
    }

    public static void init(String[] args) throws JAXBException, SAXException {
        if (System.getProperty("webdriver.implicitWaitPeriod") == null)
            System.setProperty("webdriver.implicitWaitPeriod", "5");
        if (args != null && args.length != 0)
            parseInputArgs(args);
        readConfigAndServices();
    }


    private static void parseInputArgs(String[] args) {
        for (int i = 0; i < args.length; i++) {
            try {
                if (args[i].equals("-configFile")) {
                    System.setProperty("configFile", args[++i]);
                }
            } catch (RuntimeException e) {
                log.error("Something wrong with input arguments", e);
                System.err.println(usage);
            }
        }
    }

    public static Services readServices(InputStream xmlInputStream, boolean checkXSD) throws JAXBException, SAXException, FileNotFoundException {
        log.debug("Reading services...");
        String xsdFileName = System.getProperty("values.xsd", "values.xsd");
        InputStream xsdInputStream = new BufferedInputStream(ConfigUtils.class.getClassLoader().getResourceAsStream(xsdFileName));
        Source source = new StreamSource(xsdInputStream);

        JAXBContext jaxbContext = JAXBContext
                .newInstance("ru.atc.gosuslugi.minitest.jaxb.values");
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        if(checkXSD) {
            Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(source);
            unmarshaller.setSchema(schema);
        }

        JAXBElement<?> element = (JAXBElement<?>) unmarshaller.unmarshal(xmlInputStream);
        Services services = (Services) element.getValue();
        log.debug("Read services: \n" + services.toString());
        if (System.getProperty("ru.atc.SIR_URL") == null) {
           for (Service s : services.getServices()) {
                if (s.getAfter() != null && s.getAfter().equals(After.SIR_CANCEL.name())) {
                    throw new IllegalStateException("Property 'ru.atc.SIR_URL' must be set if you are trying to work with SIR.");
                }
            }
        }
        return services;
    }
}

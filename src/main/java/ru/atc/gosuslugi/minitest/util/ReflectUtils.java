package ru.atc.gosuslugi.minitest.util;


import java.lang.reflect.Method;

/**
 * Date: 23.10.13
 * Author: astafev
 */
public class ReflectUtils {

    public static String getterMethodName(String property) {
        char firstLetter = property.charAt(0);
        return "get" + Character.toUpperCase(firstLetter) + property.substring(1);
    }

    public static String setterMethodName(String property) {
        char firstLetter = property.charAt(0);
        return "set" + Character.toUpperCase(firstLetter) + property.substring(1);
    }

    public static Method getterMethod(String property, Class clazz) throws NoSuchMethodException {
        return clazz.getMethod(getterMethodName(property));
    }

    public static Method setterMethod(String property, Class clazz) throws NoSuchMethodException {
        return clazz.getMethod(setterMethodName(property), String.class);
    }
}

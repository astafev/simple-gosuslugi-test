package ru.atc.gosuslugi.minitest.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.UnsynchronizedAppenderBase;

import javax.swing.*;

/**
 * Date: 30.09.13
 * Author: astafev
 */
public class LogErrorAppender<E> extends UnsynchronizedAppenderBase<E> {
    @Override
    protected void append(E o) {
        if (o.getClass() == LoggingEvent.class) {
            LoggingEvent e = (LoggingEvent) o;
            JOptionPane.showMessageDialog(null, splitMessage(e.getMessage()), e.getLevel().toString(), JOptionPane.ERROR_MESSAGE);
        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private static final int legthLimit = 200;

    private String splitMessage(String message) {
        if(message == null) {
            return message;
        }
        try {
            StringBuilder result = new StringBuilder();

            String[] lines = message.split("\n");
            for (String line : lines) {
                if (line.length() > legthLimit) {
                    int size = line.length() / legthLimit;
                    int index1 = 0;
                    int index2 = legthLimit;
                    for (int i = 0; i < size; i++) {
                        if (i > 0)
                            result.append("    ");

                        index2 = line.substring(index1, index2).lastIndexOf(' ');
                        result.append(line.substring(index1, index2)).append('\n');
                        index1 = index2;
                        index2 = Math.min(index2 + legthLimit, line.length());
                    }
                } else {
                    result.append(line).append('\n');
                }
            }
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return message;
        }
    }
}

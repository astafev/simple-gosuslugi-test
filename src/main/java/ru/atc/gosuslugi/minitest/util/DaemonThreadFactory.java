package ru.atc.gosuslugi.minitest.util;

import java.util.concurrent.ThreadFactory;

/**
 * Date: 30.09.13
 * Author: astafev
 */
public class DaemonThreadFactory implements ThreadFactory {
    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setDaemon(true);
        return thread;
    }
}

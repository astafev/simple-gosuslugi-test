package ru.atc.gosuslugi.minitest.util;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date: 28.10.13
 * Author: astafev
 */
public class CommonUtil {
    /**
     * ["1", "2"]*/


    public static boolean isEmpty(String string) {
        return string==null || string.isEmpty();
    }


    private static final SimpleDateFormat FILE_SYSTEM_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy'T'HH-mm-ss");
    /**
     * Возвращает репрезентацию даты в формате, пригодном для файловой системы
     * */
    public static String dateToString4FileSystem(Date date) {
        return FILE_SYSTEM_DATE_FORMAT.format(date);
    }

}

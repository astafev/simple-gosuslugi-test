package ru.atc.gosuslugi.minitest.recording.sf;

import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.proxy.ProxyServer;
import org.json.JSONException;
import org.json.JSONObject;
import ru.atc.gosuslugi.minitest.jaxb.values.ComplexValue;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.web.portal.InputType;
import ru.atc.gosuslugi.minitest.recording.AbstractDictionary;
import ru.atc.gosuslugi.minitest.recording.AbstractHandler;
import ru.atc.gosuslugi.minitest.recording.MessageType;
import ru.atc.gosuslugi.minitest.util.CommonUtil;
import ru.atc.gosuslugi.minitest.util.ReflectUtils;
import ru.atc.gosuslugi.minitest.util.ValueUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Date: 30.10.13
 * Author: astafev
 */
public class ServerFormHandler extends AbstractHandler {

    public ServerFormHandler(ProxyServer proxy) {
        super(proxy);
    }

    @Override
    protected void handleDictionary(Map<String, AbstractDictionary> dictionaries, HarEntry entry) {
        //do nothing
    }


    @Override
    protected void handlePage(Map<String, Value> stepMap, HarEntry entry, Map<String, AbstractDictionary> dictionaries) throws JSONException {
        JSONObject object = new JSONObject(getRequestString(entry));
        JSONObject content = object.getJSONObject("form").getJSONObject("content");
        log.debug("step inputs: " + stepMap.toString() + "\n\nform: " + content);
//        Iterator<Map.Entry<String, Value>> mapIterator = stepMap.entrySet().iterator();
        Map<String, Value> map2add = new HashMap<String, Value>();

        for (Value value : stepMap.values()) {
//        while (mapIterator.hasNext()) {
//            Value value = mapIterator.next().getValue();
            try {
                if (value.getType().equals(InputType.hierarchyDic.name())) {
                    String name = value.getName().substring(0, value.getName().length() - 3);
                    JSONObject submittedValue = content.getJSONObject(name);
                    value.setValue(submittedValue.getString("text_value"));
                    value.setDicVal(submittedValue.getString("value"));
                    int i = 0;
                    while (true) {
                        try {
                            String indexedName = name + "[" + ++i + "]";
                            submittedValue = content.getJSONObject(indexedName);
                            Value newValue = new Value();
                            newValue.setType(InputType.hierarchyDic.name());
                            newValue.setName(indexedName);
                            newValue.setValue(submittedValue.getString("text_value"));
                            newValue.setDicVal(submittedValue.getString("value"));

//                            mapIterator.
                            map2add.put(indexedName, newValue);
                        } catch (JSONException e) {
                            //кончились
                            break;
                        }
                    }
                } else {
                    JSONObject submittedValue = content.getJSONObject(value.getName());
                    try {
                        switch (InputType.valueOf(value.getType())) {
                            case radio:
                                value.setValue(submittedValue.getString("textValue"));
                                value.setDicVal(submittedValue.getString("value"));
                                break;
                            case dic:
                            case fieldDropDown:
                                value.setValue(submittedValue.getString("text_value"));
                                value.setDicVal(submittedValue.getString("value"));
                                break;
                            case kladr:
                                ComplexValue value1 = (ComplexValue) value;
                                Field[] fields = ComplexValue.class.getDeclaredFields();
                                for (Field field : fields) {
                                    try {
                                        String s = submittedValue.getString(field.getName());
                                        if (!(s == null || s.equals("null") || s.isEmpty()))
                                            ReflectUtils.setterMethod(field.getName(), ComplexValue.class).invoke(value1, s);
                                    } catch (JSONException e) {
                                        continue;
                                    } catch (NoSuchMethodException e) {
                                        log.warn("Unable to find getter for property " + field.getName(), e);
                                        continue;
                                    } catch (InvocationTargetException e) {
                                        log.error(e.getMessage(), e);
                                        continue;
                                    } catch (IllegalAccessException e) {
                                        log.warn("Unable to find getter for property " + field.getName(), e);
                                        continue;
                                    }
                                }
                                break;
                            case fias:
                                ValueUtils.generateFiasValueFromAddressString(submittedValue.getString("value"), (ComplexValue) value);
//                            Field[] fields = ComplexValue.class.getDeclaredFields();
                                break;
                            case file:
                                if ("".equals(submittedValue.getString("value"))) {
                                    value.setValue(null);
                                } else value.setValue("");
                                break;
                            case checkbox:
                            default:
                                value.setValue(submittedValue.getString("value"));
                        }

                    } catch (IllegalArgumentException e) {
                        value.setValue(content.getJSONObject(value.getName()).getString("value"));
                    }
                }
            } catch (JSONException e) {
                log.warn("Didn't find value " + value + "in form\n" + content.toString(), e);
            }
        }
        stepMap.putAll(map2add);
        trimStepMap(stepMap);
        log.debug("stepMap: " + stepMap.toString());
    }


    @Override
     protected void trimStepMap(Map<String, Value> stepMap) {
        Iterator<Map.Entry<String, Value>> mapIterator = stepMap.entrySet().iterator();
        while (mapIterator.hasNext()) {
            Value value = mapIterator.next().getValue();
            if (value instanceof ComplexValue) {
                if (
                        (value.getType().equals(InputType.kladr.name()) || value.getType().equals(InputType.fias.name()))
                                && CommonUtil.isEmpty(((ComplexValue) value).getPost_index())
                        ) {
                    log.debug("deleting " + value.getName() + ":" + value.getType() + " because it is not set");
                    mapIterator.remove();
                }
            } else if (value.getValue() == null ||
                    (
                            ("text".equals(value.getType()) || value.getType() == null) && "".equals(value.getValue())
                    ) ||
                    (
                            "radio".equals(value.getType()) && CommonUtil.isEmpty(value.getValue()) && CommonUtil.isEmpty(value.getDicVal())
                    )
                    ) {
                log.debug("deleting " + value.getName() + ":" + value.getType() + " because it is not set");
                mapIterator.remove();
            }
        }
    }


    @Override
    protected MessageType getContentType(HarEntry entry) {
        String url = entry.getRequest().getUrl();
        if (url.contains("/htdocs/") || url.substring(0, 15).contains("sia")) {
            return MessageType.UNKNOWN;
        }

        if (entry.getResponse().getStatus() <= 299) {
            if (entry.getRequest().getMethod().equals("POST")) {
                if (url.contains("service/wsapi/refsQueryService/listRefItems")) {
                    return MessageType.Dictionary;
                } else if (url.contains("/service/wsapi/processingService/process")) {
                    return MessageType.Form;
                } else if (url.contains("/service/wsapi/processingService/saveDraft")) {
                    return MessageType.Form;
                } else if (url.contains("/service/fupload/doFileUploadSecure")) {
                    return MessageType.FileUpload;
                }
            }
        }
        return MessageType.UNKNOWN;
    }
}

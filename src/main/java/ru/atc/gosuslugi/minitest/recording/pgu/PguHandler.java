package ru.atc.gosuslugi.minitest.recording.pgu;

import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarNameValuePair;
import net.lightbody.bmp.core.har.HarPostDataParam;
import net.lightbody.bmp.proxy.ProxyServer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.web.portal.InputType;
import ru.atc.gosuslugi.minitest.recording.AbstractDictionary;
import ru.atc.gosuslugi.minitest.recording.AbstractHandler;
import ru.atc.gosuslugi.minitest.recording.MessageType;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Date: 25.10.13
 * Author: astafev
 */
public class PguHandler extends AbstractHandler {


    public static Logger log = LoggerFactory.getLogger(PguHandler.class);

    public PguHandler(ProxyServer proxyServer) {
        super(proxyServer);
    }

    @Override
    protected void handleDictionary(Map<String, AbstractDictionary> dictionaries, HarEntry entry) {
        PguDictionary dictionary = new PguDictionary(entry);
        log.debug("found dictionary " + dictionary.getDictionaryCode());
        dictionaries.put(dictionary.getDictionaryCode(), dictionary);
    }


    private String getStringFromHarPostDataParams(List<HarPostDataParam> list) {
        StringBuilder sb = new StringBuilder("[");
        for (HarPostDataParam param : list) {
            sb.append(param.getName()).append(':').append(param.getValue());
        }
        sb.append("]");
        return sb.toString();
    }


    /**
     * @param stepMap
     * @param entry        - POST запрос формы;
     * @param dictionaries
     */
    @Override
    protected void handlePage(Map<String, Value> stepMap, HarEntry entry, Map<String, AbstractDictionary> dictionaries) {
        List<HarPostDataParam> params = entry.getRequest().getPostData().getParams();

        if (log.isDebugEnabled())
            log.debug("step inputs: " + stepMap.toString() + "\n\nform: " + getStringFromHarPostDataParams(params));

        for (HarPostDataParam param : params) {
            Value value = stepMap.get(param.getName());

            if (value == null) {
                String paramName = param.getName();
                if (paramName.equals("id")) {
                    break;
                }
                log.debug("value " + paramName + " not found in step but was in the form. Value: " + param.getValue());
                continue;
            } else log.debug("setting " + param.getName() + " to " + param.getValue());
            try {
                if (param.getValue() != null && !param.getValue().isEmpty()) {
                    switch (InputType.valueOf(value.getType())) {
                        case checkboxGroup:
                            handleCheckboxGroup(value, param);
                            break;
                        case checkboxLookup:
                            handleCheckboxLookup(value, dictionaries, param);
                            break;
                        case staticDic:
                        case dic:
                            handleDic(value, dictionaries, param);
                            break;
                        case file:
                            if (value.getValue() == null)
                                value.setValue("");
                            break;
                        case radio:
                            value.setDicVal(param.getValue());
                            try {
                                if (value.getDescription() != null) {
                                    JSONArray jsonArray = new JSONArray(value.getDescription());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                        if (jsonObject.has(param.getValue())) {
                                            value.setValue((String) jsonObject.get(param.getValue()));
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                log.warn("something wrong", e);
                            }
                            break;
                        case checkbox:
                            if (param.getValue() == null) {
                                log.debug("deleting checkbox " + param.getName() + " because it is not set");
                                stepMap.remove(param.getName());
                            }
                            value.setValue(param.getValue());
                            break;
                        default:
                            value.setValue(param.getValue());
                    }
                    log.trace(value.toString());
                } else {
                    stepMap.remove(value.getName());
                    log.debug("deleting " + value.getName());
                }
            } catch (IllegalArgumentException e) {
                //Не поставлен type
                value.setValue(param.getValue());
            } catch (NullPointerException e) {
                throw e;
            }
        }

        trimStepMap(stepMap);
    }

    @Override
    protected void trimStepMap(Map<String, Value> stepMap) {
        Iterator<Map.Entry<String, Value>> mapIterator = stepMap.entrySet().iterator();
        while (mapIterator.hasNext()) {
            Value value = mapIterator.next().getValue();
            if (value.getValue() == null && value.getDicVal() == null) {
                log.debug("deleting " + value.getName() + ":" + value.getType() + " because it is not set");
                mapIterator.remove();
            }
        }
    }

    private void handleCheckboxGroup(Value value, HarPostDataParam param) {
        //to_test
        try {
            JSONArray jsonArray = new JSONArray(param.getValue());
            value.setValue(makePipeDelimitedFromJSONArray(jsonArray));
        } catch (JSONException e) {
            log.warn("can't make an json array from string " + param.getValue() + " for value " + value.getName() + ":" + value.getType(), e);
            value.setDicVal(null);
        }
    }

    private String makePipeDelimitedFromJSONArray(JSONArray arr) throws JSONException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length(); i++) {
            sb.append(arr.get(i)).append(" | ");
        }
        sb.delete(sb.length() - 3, sb.length());
        return sb.toString();
    }

    private void handleCheckboxLookup(Value value, Map<String, AbstractDictionary> dictionaries, HarPostDataParam param) {
        try {
            JSONArray values = new JSONArray(param.getValue());
            if (values.length() > 0) {
                StringBuilder stringValue = new StringBuilder();
                StringBuilder dicValue = new StringBuilder();
                for (int i = 0; i < values.length(); i++) {

                    stringValue.append(dictionaries.get(value.getDescription()).getDicNameForValue(values.getString(i))).append(" | ");
                    dicValue.append(values.getString(i)).append(" | ");
//                                value.setValue(param.getValue());
                }
                dicValue.delete(dicValue.length() - 3, dicValue.length());
                stringValue.delete(stringValue.length() - 3, stringValue.length());
                value.setDicVal(dicValue.toString());
                value.setValue(stringValue.toString());
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleDic(Value value, Map<String, AbstractDictionary> dictionaries, HarPostDataParam param) {
        try {
            if (value.getType().equals(InputType.staticDic.name())) {
//                value.setDicVal(param.getValue());
                if (value.getValue() == null) {
                    if (value.getDescription().startsWith("STAT ") && value.getDescription().length() > 5) {
                        JSONArray array = new JSONArray(value.getDescription().substring(5));
                        for (int i = 0; i < array.length(); i++) {
                            if (array.getJSONObject(i).getString("code").equals(param.getValue())) {
                                value.setValue(array.getJSONObject(i).getString("text"));
                                return;
                            }
                        }
                    }
                }
                return;
            }
            String valInDic = dictionaries.get(value.getDescription()).getDicNameForValue(param.getValue());
            if (valInDic == null) {
                log.warn("Can't find value " + param.getValue() + " in dictionary " + dictionaries.get(value.getDescription()));
            }
            value.setValue(valInDic);
        } catch (NullPointerException e) {
            StringBuilder error = new StringBuilder("Unable to find dictionary ");
            error.append(value.getDescription()).append("\n\nAvailable: ").append(dictionaries.keySet());
            log.error(error.toString());
            log.warn("Dictionaries: " + dictionaries);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        value.setDicVal(param.getValue());
    }

    @Override
    protected MessageType getContentType(HarEntry entry) {
        String url = entry.getRequest().getUrl();
        if (url.contains("/htdocs/") || url.substring(0, 15).contains("sia")) {
            return MessageType.UNKNOWN;
        }
        List<HarNameValuePair> respheaders = entry.getResponse().getHeaders();
        List<HarNameValuePair> reqheaders = entry.getRequest().getHeaders();
        if (entry.getResponse().getStatus() <= 299) {
            if (entry.getRequest().getMethod().equals("POST")) {
                //скорее всего наша форма
                if (entry.getRequest().getUrl().contains("fupload/doFileUpload")) {
                    return MessageType.FileUpload;
                }
                String responseType = null;
                String requestType = null;
                for (HarNameValuePair header : respheaders) {
                    if (header.getName().equalsIgnoreCase("content-type")) {
                        responseType = header.getValue();
                        break;
                    }
                }
                for (HarNameValuePair header : reqheaders) {
                    if (header.getName().equalsIgnoreCase("content-type")) {
                        requestType = header.getValue();
                        break;
                    }
                }
                if (responseType.contains(MessageType.Form.content_type_response) && requestType.contains(MessageType.Form.content_type)) {
                    return MessageType.Form;
                } else if (responseType.contains(MessageType.FormWithFile.content_type_response) && requestType.contains(MessageType.FormWithFile.content_type)) {
                    return MessageType.FormWithFile;
                } else return MessageType.UNKNOWN;
            } else if (entry.getRequest().getMethod().equals("GET")) {
                //DICTIONARY
                if (!url.contains("/dictionary"))
                    return MessageType.UNKNOWN;
                for (HarNameValuePair header : respheaders) {
                    if (header.getName().equalsIgnoreCase("content-type")) {
                        String respType = header.getValue();
                        if (respType.contains(MessageType.Dictionary.content_type_response)) {
                            return MessageType.Dictionary;
                        } else
                            return MessageType.UNKNOWN;
                    }
                }
            }
        }
        return MessageType.UNKNOWN;
    }
}

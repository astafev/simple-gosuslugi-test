package ru.atc.gosuslugi.minitest.recording.sf;

import net.lightbody.bmp.core.har.HarEntry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.atc.gosuslugi.minitest.recording.AbstractDictionary;

/**
 * Date: 30.10.13
 * Author: astafev
 */
public class ServerFormDictionary extends AbstractDictionary {
    final String dictionaryCode;
    JSONArray items;

    public ServerFormDictionary(HarEntry entry) throws JSONException {
        JSONObject object = new JSONObject(entry.getRequest().getPostData().getText());
        dictionaryCode = object.getString("refName");
        String response = entry.getResponse().getContent().getText();
        JSONObject jsonObject = new JSONObject(response);
        JSONObject error = jsonObject.getJSONObject("error");
        if(error.getInt("code")!=0) {
            throw new JSONException("Error in dictionary response " + error.getString("message"));
        }
        this.items = jsonObject.getJSONArray("items");
        int total = jsonObject.getInt("total");
        if (items.length() != total) {
            log.warn("Items size doesn't equal 'total'. items.length()=" + items.length() + "   total" + total + "\n" + response);
        }
    }

    @Override
    public String getDictionaryCode() {
        return dictionaryCode;
    }

    @Override
    public String getDicNameForValue(String value) throws JSONException {
        for (int i = 0; i < items.length(); i++) {
            if(value.equals(items.getJSONObject(i).getString("value"))) {
                return items.getJSONObject(i).getString("title");
            }
        }
        return null;
    }
}

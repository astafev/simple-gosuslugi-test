package ru.atc.gosuslugi.minitest.recording.pgu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.web.portal.InputType;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.recording.RecordingDriver;

import java.util.*;

/**
 * Date: 25.10.13
 * Author: astafev
 */
public class RecordingPortalDriver extends PortalDriver implements RecordingDriver {
    public static Logger log = LoggerFactory.getLogger(RecordingPortalDriver.class);

    public RecordingPortalDriver(WebDriver driver1) {
        super(driver1);
    }

    private Value setNameAndTypeOfDateOrTextOrDictionary(WebElement element, Value value) {
        String id = element.getAttribute("id");
        String cssClass = element.getAttribute("class");
        int indexOf = id.indexOf("-lookup-form-text");
        if (indexOf != -1) {
            if (cssClass.contains("json")) {
                value.setType(InputType.checkboxLookup.name());
            } else {
                value.setType(InputType.dic.name());
            }
            value.setName(id.substring(0, indexOf));
            String dictCode = this.getDictionaryCode(value.getName());
            if (dictCode != null) {
                value.setDescription(this.getDictionaryCode(value.getName()));
            } else {
                value.setType(InputType.staticDic.name());
                log.trace("handling static dictionary");
                String dicValue = getDictionaryValue(value.getName());
                if (dicValue == null || dicValue.equals("")) {
                    try {
                        value.setDescription("STAT " + new JSONArray(getDictionaryStaticOpts(value.getName())));
                    } catch (Exception e) {
                        value.setDescription("STAT ");
                    }
                } else {
                    value.setDicVal(dicValue);
                    value.setValue(getDictionaryTextValue(value.getName()));
                }
            }
        } else {
            value.setName(id);
            if (cssClass.contains("date-field")) {
                value.setType(InputType.date.name());
            } else value.setType(InputType.text.name());
        }
        return value;
    }

    public List<Map<String, String>> getDictionaryStaticOpts(String name) {
        try {
            return (List<Map<String, String>>) this.executeScript("return LookupWidget.registry.getRecord('" + name + "').config.sourceData.staticData.items");
        } catch (org.openqa.selenium.WebDriverException e) {
            throw new StaleElementReferenceException(e.getMessage(), e);
        }

    }

    public String getDictionaryCode(String name) {
        try {
            return (String) this.executeScript("return LookupWidget.registry.getRecord('" + name + "').config.sourceData.dictionaryCode");
        } catch (org.openqa.selenium.WebDriverException e) {
            throw new StaleElementReferenceException(e.getMessage(), e);
        }
    }

    public String getDictionaryValue(String name) {
        try {
            return (String) this.executeScript("return document.getElementById('" + name + "').value");
        } catch (org.openqa.selenium.WebDriverException e) {
            throw new StaleElementReferenceException(e.getMessage(), e);
        }
    }

    public String getDictionaryTextValue(String name) {
        try {
            return (String) this.executeScript("return document.getElementById('" + name + "-lookup-form-text').value");
        } catch (org.openqa.selenium.WebDriverException e) {
            throw new StaleElementReferenceException(e.getMessage(), e);
        }
    }

    public Map<String, Value> stepMap(boolean init) {
        Map<String, Value> map = new LinkedHashMap<String, Value>();
        try {
            long t = System.currentTimeMillis();
            List<WebElement> possibleElems = getDisplayedList(findElement(By.cssSelector("#content table.rubberWhiteBox ")), By.cssSelector("tr.fieldrow"));
            for (WebElement element : possibleElems) {
                long t2 = System.currentTimeMillis();
                List<Value> values = detectTypeAndFill(element);
                if (values != null && !values.isEmpty()) {
                    for (Value value : values)
                        map.put(value.getName(), value);
                    log.trace("{} ms to create value with field {}", System.currentTimeMillis() - t2, values.get(0).getName());
                }
            }
            log.trace("{} ms took creating step map", System.currentTimeMillis() - t);
        } catch (StaleElementReferenceException e) {
            log.debug(e.getMessage());
            return null;
        } catch (NoSuchElementException e) {
            log.debug(e.getMessage(), e);
            return null;
        }
        if (log.isTraceEnabled())
            log.trace("stepMap: " + map);
        return map;
    }

    private List<Value> detectTypeAndFill(WebElement row) {
        List<Value> result = new ArrayList<Value>();


        List<WebElement> textAreaWrap = row.findElements(By.className("textAreaWrap"));
        if (textAreaWrap.size() > 0) {
            Value value = createValueFromTextArea(row, row.findElement(By.tagName("textarea")));
            if(value!=null)
                result.add(value);
        }

        List<WebElement> checkboxes = row.findElements(By.cssSelector("input[type='checkbox']"));
        if (checkboxes.size() > 0) {
            if (checkboxes.size() > 1) {
                log.warn("something wrong found many checkboxes:\n" + row.getAttribute("innerHTML"));
            }
            Value value = createValueFromCheckbox(row, checkboxes.get(0));
            if(value!=null)
                result.add(value);;
        }

        List<WebElement> inputs = row.findElements(By.className("inputBasicWrap"));
        if (inputs.size() > 0) {
            Value[] values = new Value[inputs.size()];
            for (int i = 0; i < inputs.size(); i++) {
                values[i] = createValueFromTextOrDateOrDic(row, inputs.get(i));
            }
            if(values[0]!=null)
                result.addAll(Arrays.asList(values));

        }

        List<WebElement> radios = row.findElements(By.cssSelector("ul.radioListNew"));
        if (radios.size() > 0) {
            if (radios.size() > 1) {
                log.warn("something wrong found many radios:\n" + row.getAttribute("innerHTML"));
            }
            Value value = createValueFromRadio(row, radios.get(0));
            if(value!=null)
                result.add(value);;
        }

        List<WebElement> files = row.findElements(By.name("fileDataStub"));
        if (files.size() > 0) {
            if (files.size() > 1) {
                log.warn("something wrong found many files:\n" + row.getAttribute("innerHTML"));
            }
            Value value = createValueFromFile(row, files.get(0));
            if(value!=null)
                result.add(value);;
        }

        List<WebElement> checkboxGroups = row.findElements(By.cssSelector("input.checkboxGroup_HiddenInput.json"));
        if (checkboxGroups.size() > 0) {
            if (checkboxGroups.size() > 1) {
                log.warn("something wrong found many checkboxGroups:\n" + row.getAttribute("innerHTML"));
            }
            Value value = createValueFromCheckboxGroup(row, checkboxGroups.get(0));
            if(value!=null)
                result.add(value);;
        }

        return result;
    }

    private Value createValueFromTextArea(WebElement row, WebElement area) {
        if (isAvailable(area)) {
            Value value = new Value();
            value.setType(InputType.text.name());
            value.setName(area.getAttribute("id"));
            value.setDescription(row.findElement(By.cssSelector(".fieldprompt label")).getText());
            return value;
        } else return null;
    }

    private Value createValueFromCheckbox(WebElement row, WebElement checkbox) {
        if (checkbox.findElement(By.xpath("..")).isDisplayed() && isAvailable(checkbox)) {
            Value value = new Value();
            value.setType(InputType.checkbox.name());
            value.setName(checkbox.getAttribute("id"));
            value.setDescription(row.getText().trim());
            return value;
        } else return null;
    }

    private Value createValueFromRadio(WebElement row, WebElement ul) {
        try {
            Value value = new Value();
            value.setName(ul.findElement(By.xpath("../input")).getAttribute("id"));
            value.setType(InputType.radio.name());
            JSONArray values = new JSONArray();
            for (WebElement li : ul.findElements(By.tagName("li"))) {
                JSONObject object = new JSONObject();
                String key = li.findElement(By.tagName("input")).getAttribute("value");
                String desc = li.getText().trim();
                object.put(key, desc);
                values.put(object);
            }
            value.setDescription(values.toString());
            return value;
        } catch (NoSuchElementException e) {
            log.warn("didn't find radiogroup input!", e);
        } catch (JSONException e) {
            log.error("Something wrong!", e);
            throw new RuntimeException(e);
        }
        return null;
    }

    private Value createValueFromTextOrDateOrDic(WebElement row, WebElement inputDiv) {
        if (inputDiv.findElement(By.xpath("..")).isDisplayed()) {
            WebElement input = inputDiv.findElement(By.tagName("input"));
            if(!isAvailable(input)) {
                return null;
            }
            //TODO check availability
            Value value = new Value();
            setNameAndTypeOfDateOrTextOrDictionary(input, value);
            if (value.getDescription() == null)
                try {
                    value.setDescription(row.findElement(By.id(value.getName() + "-label")).getText());
                } catch (NoSuchElementException e) {

                }
            return value;
        } else return null;
    }

    private Value createValueFromFile(WebElement row, WebElement file) {
        WebElement div = row.findElement(By.id(file.getAttribute("id") + ".fileID-upl"));
        String idDiv = div.getAttribute("id");
        if (!idDiv.endsWith(".fileID-upl")) {
            log.warn("Something wrong in detecting file input. Didn't find div with right id. Found " + idDiv);
        } else if (div.isDisplayed() && isAvailable(div)) {
            //do nothing
            Value value = new Value();
            value.setType(InputType.file.name());
            value.setName(file.getAttribute("id"));
            try {
                value.setDescription(findElement(By.id(value.getName() + "-label")).getText());
            } catch (NoSuchElementException e) {
                //do nothing
            }
            return value;
        }
        return null;
    }

    private Value createValueFromCheckboxGroup(WebElement row, WebElement file) {
        WebElement div = row.findElement(By.id(file.getAttribute("id") + ".fileID-upl"));
        String idDiv = div.getAttribute("id");
        if (!idDiv.endsWith(".fileID-upl")) {
            log.warn("Something wrong in detecting file input. Didn't find div with right id. Found " + idDiv);
        } else if (div.isDisplayed() && isAvailable(div)) {
            //do nothing
            Value value = new Value();
            value.setType(InputType.file.name());
            value.setName(file.getAttribute("id"));
            try {
                value.setDescription(findElement(By.id(value.getName() + "-label")).getText());
            } catch (NoSuchElementException e) {

            }
            return value;
        }
        return null;
    }

    public boolean isAvailable(WebElement element) {
        //TODO исключать disabled поля, read-only и т.п.
        String tag = element.getTagName();

        if (tag.equals("div")) {
            String cssClass = element.getAttribute("class");
            if (cssClass.contains("fieldDisabled"))
                return false;
        } else if (tag.equals("input")) {
            return isAvailable(element.findElement(By.xpath("../..")));
        }
        return true;
    }

    /** + от метода проверки по строкам то что все поля по порядку.
     * Возможно и работать будет быстрее, если правильно сделать
     *
     * @return null если поле disabled либо если */
    /*public List<Value> getRowValue (WebElement row) {
        //TODO to implement
        //div.textAreaWrap - textArea
        //div.qq-uploader - файл
        WebElement div = row.findElement(By.cssSelector("div.inputBasicWrap"));
        if(div.getAttribute("class").contains("fieldDisabled")) {
            return null;
        }
        List<WebElement> inputs = row.findElements(By.tagName("input"));
        if(inputs.size() == 0) {
            return null;
        }
        List<Value> values = new LinkedList<Value>();
        Value value = new Value();
        if(inputs.size() == 1) {
            setNameAndType(inputs.get(0), value);
            values.add(value);
            return values;
        }

        throw new UnsupportedOperationException("Not implemented");


    }

    private Map<String, Value> findTextArea(WebElement page) {
        LinkedHashMap<String, Value> map = new LinkedHashMap<String, Value>();
        List<WebElement> textareas = getDisplayedList(page, By.tagName("textarea"));
        for (WebElement area : textareas) {
            if (isAvailable(area)) {
                Value value = new Value();
                value.setType(InputType.text.name());
                value.setName(area.getAttribute("id"));
                map.put(value.getName(), value);
            }
        }
        return map;  //To change body of created methods use File | Settings | File Templates.
    }

    private Map<String, Value> findCheckboxes(WebElement page) {
        LinkedHashMap<String, Value> map = new LinkedHashMap<String, Value>();
        List<WebElement> checkboxes = page.findElements(By.cssSelector("input[type=\"checkbox\"]"));
        for (WebElement checkbox : checkboxes) {
            if (checkbox.findElement(By.xpath("..")).isDisplayed() && isAvailable(checkbox)) {
                Value value = new Value();
                value.setType(InputType.checkbox.name());
                value.setName(checkbox.getAttribute("id"));
                map.put(value.getName(), value);
            }
        }
        return map;
    }

    private Map<String, Value> findTextAndDateAndDictionaries(WebElement page) {
        List<WebElement> inputs = getDisplayedList(page, By.tagName("input"));
        LinkedHashMap<String, Value> map = new LinkedHashMap<String, Value>();
        for (WebElement input : inputs) {
            if (isAvailable(input)) {
                Value value = new Value();
                setNameAndTypeOfDateOrTextOrDictionary(input, value);
                map.put(value.getName(), value);
            }
        }
        return map;  //To change body of created methods use File | Settings | File Templates.
    }

    private Map<String, Value> findRadioGroups(WebElement page) {
        Map<String, Value> map = new LinkedHashMap<String, Value>();
        List<WebElement> uls = getDisplayedList(page, By.cssSelector("ul.radioListNew"));
        for (WebElement ul : uls) {
            try {
                Value value = new Value();
                value.setName(ul.findElement(By.xpath("../input")).getAttribute("id"));
                value.setType(InputType.radio.name());
                map.put(value.getName(), value);
            } catch (NoSuchElementException e) {
                log.warn("didn't find radiogroup input!", e);
            }
        }

        return map;
    }

private Map<String, Value> findCheckboxGroups(WebElement page) {
        Map<String, Value> map = new LinkedHashMap<String, Value>();
        List<WebElement> checkboxGroups = page.findElements(By.cssSelector("input.checkboxGroup_HiddenInput.json"));
        Iterator<WebElement> iterator = checkboxGroups.iterator();
        while (iterator.hasNext()) {
            WebElement checkboxGroup = iterator.next();
            if (checkboxGroup.findElement(By.xpath("..")).isDisplayed()) {
                //TODO check availability
                Value value = new Value();
                value.setType(InputType.checkboxGroup.name());
                value.setName(checkboxGroup.getAttribute("id"));
                map.put(value.getName(), value);
            }
        }
        return map;
    }

    private Map<String, Value> findFiles(WebElement page) {
        Map<String, Value> map = new LinkedHashMap<String, Value>();
        List<WebElement> files = page.findElements(By.name("fileDataStub"));
        Iterator<WebElement> elementIterator = files.iterator();
        while (elementIterator.hasNext()) {
            WebElement file = elementIterator.next();
            WebElement div = page.findElement(By.id(file.getAttribute("id") + ".fileID-upl"));
            String idDiv = div.getAttribute("id");
            if (!idDiv.endsWith(".fileID-upl")) {
                log.warn("Something wrong in detecting file input. Didn't find div with right id. Found " + idDiv);
            } else if (div.isDisplayed() && isAvailable(div)) {
                //do nothing
                Value value = new Value();
                value.setType(InputType.file.name());
                value.setName(file.getAttribute("id"));
                map.put(value.getName(), value);
            }
        }
        return map;
    }
    */
}

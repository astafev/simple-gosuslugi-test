package ru.atc.gosuslugi.minitest.recording.sf;

import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.ComplexValue;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.web.portal.InputType;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriverSF;
import ru.atc.gosuslugi.minitest.recording.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Date: 25.10.13
 * Author: astafev
 */
public class RecordingSFPortalDriver extends PortalDriverSF implements RecordingDriver {
    public static Logger log = LoggerFactory.getLogger(RecordingSFPortalDriver.class);

    public RecordingSFPortalDriver(WebDriver driver1) {
        super(driver1);
        this.driver = new PortalDriverSF(driver1);
    }

    public Map<String, Value> stepMap(boolean init) {
        Map<String, Value> map = new LinkedHashMap<String, Value>();

        try {
            WebElement step = getDisplayed(By.cssSelector("div.PGU-FormStep"));
            if(step == null) {
                try{
                    if(findElement(By.cssSelector("div.PGU-NavPanel")).isDisplayed()) {
                        return map;
                    } else throw new PortalException(this, "unknown page");
                } catch (NoSuchElementException e) {
                    throw new PortalException(this, "unknown page", e);
                }
            }
            List<WebElement> panels = getDisplayedList(step, By.cssSelector("div.ITEMS_CONTAINER"));
            for(WebElement panel: panels) {
                if(!panel.isDisplayed()) {
                    throw new StaleElementReferenceException("Probably moved to next step");
                }
                List<WebElement> fields = getDisplayedList(panel, By.xpath("div"));
                for(WebElement field: fields) {
                    String cssClass = field.getAttribute("class");
                    if(cssClass.startsWith("PGU-Panel ") || cssClass.startsWith("PGU-Panel-cloneButton") || cssClass.startsWith("PGU-disabled ") || cssClass.startsWith("PGU-Label "))
                        continue;
                    else {
                        Value value = new Value();
                        if(cssClass.startsWith("PGU-FieldComboBox ")) {

                            try {
                                field.findElement(By.cssSelector("div.PGU-hierarchy"));
                                value.setType(InputType.hierarchyDic.name());
                            } catch (NoSuchElementException e) {
                                value.setType(InputType.dic.name());
                            }
                        } else if(cssClass.startsWith("PGU-FieldTextDate")) {
                            value.setType(InputType.date.name());
                        } else if(cssClass.startsWith("PGU-FieldText")) {
                            value.setType(InputType.text.name());
                        } else if(cssClass.startsWith("PGU-FieldUpload")) {
                            value.setType(InputType.file.name());
                        } else if(cssClass.startsWith("PGU-FieldCheckBox")) {
                            value.setType(InputType.checkbox.name());
                        }
                        //TO_TEST
                        else if(cssClass.startsWith("PGU-FieldComboBoxMultiple")) {
                            value.setType(InputType.checkboxLookup.name());
                        } else if(cssClass.startsWith("PGU-Kladr")) {
                            value = new ComplexValue();
                            value.setType(InputType.kladr.name());
                        } else if(cssClass.startsWith("PGU-Fias")) {
                            value = new ComplexValue();
                            value.setType(InputType.fias.name());
                        } else if(cssClass.startsWith("PGU-FieldRadio")) {
                            value.setType(InputType.radio.name());
                        } else {
                            log.warn("Unknown type! " + cssClass + " " + field.getAttribute("id"));
                            continue;
                        }
                        value.setName(field.getAttribute("id"));
                        try{
                            value.setDescription(field.findElement(By.cssSelector("label.PGU-FieldLabel")).getText());
                        } catch (NoSuchElementException e) {
                            try{
                                value.setDescription(field.findElement(By.cssSelector("label.PGU-FieldLabel-List")).getText());
                            } catch (NoSuchElementException e1) {
                                log.debug("Can't find description of " + value.getName());
                            }
                        }
                        map.put(value.getName(), value);
                    }
                }
            }
        } catch (StaleElementReferenceException e) {
            log.debug(e.getMessage());
            return null;
        } catch (NoSuchElementException e) {
            log.debug(e.getMessage());
            return null;
        }
        if(log.isTraceEnabled())
            log.trace("stepMap: " + map);
        return map;

    }

    public boolean checkAvailability(WebElement element) {
        //TODO исключать disabled поля, read-only и т.п.
        String tag = element.getTagName();

        if (tag.equals("div")) {
            String cssClass = element.getAttribute("class");
            if (cssClass.contains("fieldDisabled"))
                return false;
        } else if(tag.equals("input")) {
            return checkAvailability(element.findElement(By.xpath("../..")));
        }
        return true;
    }

}

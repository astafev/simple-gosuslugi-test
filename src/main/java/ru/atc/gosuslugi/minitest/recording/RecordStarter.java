package ru.atc.gosuslugi.minitest.recording;

import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;

import java.io.*;
import java.nio.charset.Charset;

public class RecordStarter {
    private static org.slf4j.Logger log = LoggerFactory.getLogger(RecordStarter.class);
    public static final String USAGE =
            "-s <serviceUrl>\n" +
                    "[-sf <true/false>]\n" +
                    "[-f <outFileName>]\n" +
                    "[-waitPeriod <wait period in ms>]\n" +
                    "[-local.port <proxyPort>]";
    public static Boolean sf = null;

    public static void main(String[] args) {
        File serviceFile = new File("service.xml");
        String serviceUrl = "https://pgu-service-dev.egov.at-consulting.ru/service/66197/6600000010000535876?userSelectedRegion=65000000000";
        for (int i = 0; i < args.length; i++) {
            try {
                if (args[i].equals("-f")) {
                    serviceFile = new File(args[++i]);
                } else if (args[i].equals("-s")) {
                    serviceUrl = args[++i];
                } else if (args[i].equals("-sf")) {
                    sf = Boolean.valueOf(args[++i]);
                } else if (args[i].equals("-local.port")) {
                    RecordRunner.portNum = Integer.valueOf(args[++i]);
                } else if (args[i].equals("-waitPeriod")) {
                    RecordRunner.waitPeriod = Integer.valueOf(args[++i]);
                } else {
                    log.error("Unknown option" + args[i] + "!\n" + USAGE);
                    return;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                log.error("WRONG USAGE!\n" + USAGE);
                return;
            } catch (NumberFormatException e) {
                log.error("WRONG USAGE!\n" + e.getMessage() + "\n" + USAGE);
                return;
            }
        }
        log.debug("serviceFile: " + serviceFile);

        if (serviceUrl == null) {
            log.error("serviceURL is mandatory parameter!\n" + USAGE);
            return;
        }

        RecordRunner runner = RecordRunner.getInstance();
        Service s = RecordRunner.factory.createService();
        try {
            runner.init();
            if (sf != null)
                runner.sf = sf;

            runner.runService(s, serviceUrl);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (s != null) {
                try {
                    Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(serviceFile), Charset.forName("UTF-8")));
                    s.writeXML(fw);
                    fw.close();
                    log.info("Service saved to " + serviceFile.getAbsolutePath());
                } catch (Exception e) {
                    log.error("Error writing service", e);
                }
            }
            runner.shutDown();
        }
    }
}

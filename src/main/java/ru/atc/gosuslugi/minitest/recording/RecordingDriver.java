package ru.atc.gosuslugi.minitest.recording;

import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.web.portal.IPortalDriver;

import java.util.Map;

/**
 * Date: 30.10.13
 * Author: astafev
 */
public interface RecordingDriver extends IPortalDriver {

    Map<String, Value> stepMap(boolean init);
}

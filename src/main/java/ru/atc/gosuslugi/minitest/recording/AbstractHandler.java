package ru.atc.gosuslugi.minitest.recording;

import net.lightbody.bmp.core.har.*;
import net.lightbody.bmp.proxy.ProxyServer;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.util.CommonUtil;

import java.io.File;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Date: 25.10.13
 * Author: astafev
 */
public abstract class AbstractHandler {
    protected final ProxyServer proxy;
    public static Logger log = LoggerFactory.getLogger(AbstractHandler.class);

    public static File traceDir = new File(System.getProperty("java.io.tmpdir") + File.pathSeparator + "hars");
    volatile public int counter = 0;


    protected AbstractHandler(ProxyServer proxy) {
        this.proxy = proxy;
    }

    public void writeHar(Har har) {
        try {
            if (!traceDir.exists()) {
                traceDir.mkdir();
                counter = 0;
            }
            if (traceDir.isDirectory()) {
                File harFile = new File(traceDir, counter + "-" + CommonUtil.dateToString4FileSystem(new Date()));
                har.writeTo(harFile);
            } else {
                log.error(String.format("%s is not a directory. Impossible to write har", traceDir.getAbsolutePath()));
            }
        } catch (Exception e) {
            log.error("Can't write har!", e);
        }
    }

    /**
     * вызывается когда перешли на след. шаг. Нужно взять с прокси все параметры.
     *
     * @param step    - шаг. пока только с номером
     * @param stepMap
     */
    public void handleStepMove(Step step, Map<String, Value> stepMap) throws JSONException {
        log.info("detected step move " + step.getNumStep());

        Har har = proxy.getHar();
        if (log.isTraceEnabled()) {
            writeHar(har);
        }
        proxy.newHar("Step after " + step.getNumStep());

        Map<String, AbstractDictionary> dictionaries = new HashMap<String, AbstractDictionary>();
        HarLog harLog = har.getLog();
        List<HarEntry> entries = harLog.getEntries();
        log.debug(entries.size() + " har entries");
        boolean pageHandled = false;
        for (int i = 0; i < entries.size(); i++) {
            HarEntry entry = entries.get(i);
            switch (getContentType(entry)) {
                case Dictionary:
                    handleDictionary(dictionaries, entry);
                    break;
                case Form:
                    log.debug("handling page");
                    handlePage(stepMap, entry, dictionaries);
                    pageHandled = true;
                    break;
                case FormWithFile:
                    log.debug("handling page with files");
                    handlePage(stepMap, handleEntryWithMimeBoundary(entry), dictionaries);
                    pageHandled = true;
                    break;
                case FileUpload:
                    log.debug("handling file upload");
                    handleFileUpload(stepMap, entry);
                    break;
                default:
                    continue;
            }
        }
        if (pageHandled)
            step.getStepValues().addAll(stepMap.values());
        else
            log.info("Page wasn't handled. deleting all values");
        log.info("Step " + step.getNumStep() + " '" + step.getDescription() + "' proceed");
        if (log.isDebugEnabled())
            log.debug("Step " + step.getNumStep() + " - " + step.getStepValues().size() + ":\n" + step.getStepValues());
    }

    protected void handleFileUpload(Map<String, Value> stepMap, HarEntry entry) {
        String fieldName = null;
        String fileName = "test";
        for (HarNameValuePair param : entry.getRequest().getQueryString()) {
            if (param.getName().equals("qqfile")) {
                fileName = param.getValue();
            } else if (param.getName().equals("fieldName")) {
                fieldName = param.getValue();
            }
        }
        if (stepMap.get(fieldName) == null) {
            log.warn("Can't find field " + fieldName + " in step map");
        } else {
            stepMap.get(fieldName).setValue(fileName);
        }
    }

    protected abstract void handleDictionary(Map<String, AbstractDictionary> dictionaries, HarEntry entry);

    /**
     * Обрабатывает переход шага, ставит в values значения из сообщения. Незаполненные поля удаляются из карты
     *
     * @param stepMap      - карта пар "id поля":"Value"
     * @param dictionaries - все справочники, пойманные на момент посыла
     * @param entry        - все что прокси поймало
     */
    protected abstract void handlePage(Map<String, Value> stepMap, HarEntry entry, Map<String, AbstractDictionary> dictionaries) throws JSONException;

    protected abstract MessageType getContentType(HarEntry entry);

    private HarEntry handleEntryWithMimeBoundary(HarEntry entry) {

        String mimeType = entry.getRequest().getPostData().getMimeType();
        if (!mimeType.startsWith("multipart/form-data")) {
            throw new IllegalArgumentException("Unknown mime type of request: " + mimeType);
        }
        List<HarPostDataParam> paramsList = new LinkedList<HarPostDataParam>();
        entry.getRequest().getPostData().setParams(paramsList);

        Charset charset = Charset.defaultCharset();
        for (HarNameValuePair header : entry.getResponse().getHeaders()) {
            if (header.getName().equalsIgnoreCase("Content-Type")) {
                int index = header.getValue().indexOf("charset=");
                String encoding = header.getValue().substring(index + 8);
                charset = Charset.forName(encoding);
                break;
            }
        }

        String boundary = mimeType.substring(mimeType.indexOf("boundary=") + 9);
        String delimiter = "--" + boundary;
        String end = delimiter + "--";

        String request;
        if (charset == Charset.defaultCharset()) {
            request = entry.getRequest().getPostData().getText();
        } else {
            request = charset.decode(ByteBuffer.wrap(entry.getRequest().getPostData().getText().getBytes())).toString();
        }
        Scanner scanner = new Scanner(new StringReader(request));
        HarPostDataParam harPostDataParam = null;
        int counter = 0;
        StringBuilder valueBuilder = null;
        while (scanner.hasNext()) {
            String nextLine = scanner.nextLine();
            counter++;
            if (nextLine.equals(delimiter)) {
                if (valueBuilder != null) {
                    String name = harPostDataParam.getName();
                    if (
                            name.equals("fileDataStub") ||
                                    name.equals("file") ||
                                    name.endsWith(".fileHash") ||
                                    name.endsWith(".fileID")
                            ) {

                        //do nothing
                    } else {
                        if (name.endsWith(".fileName")) {
                            harPostDataParam.setName(name.substring(0, name.length() - 9));
                        }
                        harPostDataParam.setValue(valueBuilder.toString());
                        paramsList.add(harPostDataParam);
                    }
                }
                valueBuilder = new StringBuilder();
                harPostDataParam = new HarPostDataParam();

                String nameContainer = scanner.nextLine();
                counter++;
                int start = nameContainer.indexOf("name=\"");
                if (start == -1) {
                    throw new RuntimeException("Row after delimiter must contain 'name=\\\"'. " + nameContainer + "\nrow = " + counter);
                }
                harPostDataParam.setName(nameContainer.substring(start + 6, nameContainer.indexOf(34, start + 6)));

                String empty = scanner.nextLine();
                counter++;
                if (empty.startsWith("Content-Type")) {
                    if (!harPostDataParam.getName().equals("file")) {
                        log.warn("!!! Unknown type of message! Error is very probable.\n" + nameContainer + "\n" + empty + "\n---------REQUEST---------\n" + request + "\n--------END REQUEST---------");
                    }
                    empty = scanner.nextLine();
                    counter++;
                }
                if (!empty.equals("")) {
                    throw new RuntimeException("Row after Content-Disposition must be empty String. Was: '" + empty + "'.\nrow = " + counter);
                }

            } else if (nextLine.equals(end)) {
                harPostDataParam.setValue(valueBuilder.toString());
                paramsList.add(harPostDataParam);
                entry.getRequest().getPostData().setText(null);
                break;
            } else {
                valueBuilder.append(nextLine);
            }
        }
        return entry;
    }


    /**
     * Returns request data in appropriate encoding.
     *
     * @param entry POST-request
     */
    protected String getRequestString(HarEntry entry) {
        Charset charset = Charset.defaultCharset();

        List<HarNameValuePair> headers = entry.getRequest().getHeaders();
        for (HarNameValuePair header : headers) {
            if (header.getName().equals("Content-Type")) {
                int index = header.getValue().indexOf("charset=");
                if (index != -1 && index != header.getValue().length()) {
                    charset = Charset.forName(header.getValue().substring(index + 8));
                    log.debug("Using encoding " + charset);
                }
                break;
            }
        }

        return charset.decode(ByteBuffer.wrap(entry.getRequest().getPostData().getText().getBytes())).toString();

    }

    /**
     * Удаляет незаполненные элементы
     */
    protected abstract void trimStepMap(Map<String, Value> stepMap);
}
package ru.atc.gosuslugi.minitest.recording;

/**
 * Date: 25.10.13
 * Author: astafev
 */
public enum MessageType {
    Dictionary("application/x-www-form-urlencoded", "application/json"),
    Form("application/x-www-form-urlencoded", "text/html"),
    FormWithFile("multipart/form-data", ""),
    UNKNOWN("unknown", "unknown"),
    FileUpload("application/octet-stream", "");

    public String content_type;
    public String content_type_response;

    MessageType(String content_type, String content_type_response) {
        this.content_type = content_type;
        this.content_type_response = content_type_response;

    }
}

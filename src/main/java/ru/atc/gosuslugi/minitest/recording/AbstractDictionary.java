package ru.atc.gosuslugi.minitest.recording;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Date: 25.10.13
 * Author: astafev
 */
public abstract class AbstractDictionary {
    public static Logger log = LoggerFactory.getLogger(AbstractDictionary.class);
    public abstract String getDictionaryCode();
    public abstract String getDicNameForValue(String value) throws JSONException;
}

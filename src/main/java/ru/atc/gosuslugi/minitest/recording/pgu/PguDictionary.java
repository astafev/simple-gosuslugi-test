package ru.atc.gosuslugi.minitest.recording.pgu;

import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.atc.gosuslugi.minitest.recording.AbstractDictionary;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Date: 25.10.13
 * Author: astafev
 */
public class PguDictionary extends AbstractDictionary {
    String json;
    String dictionaryCode;

    public PguDictionary(HarEntry entry) {
        Charset charset = Charset.defaultCharset();
        for (HarNameValuePair header : entry.getResponse().getHeaders()) {
            if (header.getName().equalsIgnoreCase("Content-Type")) {
                int index = header.getValue().indexOf("charset=");
                String encoding = header.getValue().substring(index + 8);
                charset = Charset.forName(encoding);
            }
        }
        this.json = charset.decode(ByteBuffer.wrap(entry.getResponse().getContent().getText().getBytes())).toString();
        List<HarNameValuePair> list = entry.getRequest().getQueryString();
        for (HarNameValuePair pair : list) {
            if (pair.getName().equals("dictionaryCode")) {
                this.dictionaryCode = pair.getValue();
                break;
            }
        }
    }

    @Override
    public String getDictionaryCode() {
        return dictionaryCode;
    }

    @Override
    public String getDicNameForValue(String value) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        int total = jsonObject.getInt("total");
        JSONObject errorObject = jsonObject.getJSONObject("error");
        int errorCode = errorObject.getInt("code");
        if (errorCode != 0) {
            log.warn("error code is not 0!\nerrorCode=" + errorCode + "\nObject: " + json);
        }
        JSONArray array = jsonObject.getJSONArray("items");
        if (total != array.length()) {
            log.warn("actual length doesn't equal 'total'.\ntotal=" + total + ", actual=" + array.length() + "\nObject: " + json);
        }
        for (int i = 0; i < array.length(); i++) {
            if (array.getJSONObject(i).getString("code").equals(value)) {
                return array.getJSONObject(i).getString("text");
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "PguDictionary{" +
                "json='" + json + '\'' +
                ", dictionaryCode='" + dictionaryCode + '\'' +
                '}';
    }
}

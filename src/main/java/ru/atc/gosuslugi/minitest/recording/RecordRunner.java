package ru.atc.gosuslugi.minitest.recording;

import net.lightbody.bmp.proxy.ProxyServer;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.ObjectFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.recording.pgu.PguHandler;
import ru.atc.gosuslugi.minitest.recording.pgu.RecordingPortalDriver;
import ru.atc.gosuslugi.minitest.recording.sf.RecordingSFPortalDriver;
import ru.atc.gosuslugi.minitest.recording.sf.ServerFormHandler;
import ru.atc.gosuslugi.minitest.util.ConfigUtils;
import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.web.portal.*;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

/**
 * Singleton
 * Date: 25.10.13
 * Author: astafev
 */
public class RecordRunner {


    private ProxyServer proxy;
    private RecordingDriver recordDriver;
    static ObjectFactory factory = new ObjectFactory();
    public static Logger log = LoggerFactory.getLogger(RecordRunner.class);
    static int portNum = 4444;
    public Boolean sf = null;
    public static Integer waitPeriod = 1000;

    private RecordRunner() {
    }

    private static RecordRunner recorder;

    public static RecordRunner getInstance() {
        if (recorder == null) {
            recorder = new RecordRunner();
        }
        return recorder;
    }


    public void init() throws Exception {
        ConfigUtils.readConfig();
        proxy = new ProxyServer(portNum);
        try {
            proxy.start();
        } catch (Exception e) {
            throw new RuntimeException("Couldn't init proxy: " + e.getMessage(), e);
        }
        proxy.setCaptureHeaders(true);
        proxy.setCaptureHeaders(true);
        proxy.setCaptureBinaryContent(false);
        System.setProperty("webdriver.proxy", "true");
        System.setProperty("webdriver.proxy.port", String.valueOf(portNum));
    }

    public Service runService(Service service, String url) throws BrokenServiceException, MalformedURLException {
        service.setUrl(url);
        if (sf != null) {
            service.setSf(sf);
        }
        service.parseUrl(url);

        log.debug("service url: " + url + " sf: " + sf);
        log.debug("Recording service " + service);

        Map<String, Value> stepMap = initService(service);

        List<Step> list = service.getServiceValues().getSteps();
        Step step = createStep();
        list.add(step);


        proxy.newHar("step 1");
        proxy.setCaptureContent(true);
        proxy.setCaptureHeaders(true);
        proxy.setCaptureBinaryContent(false);
        AbstractHandler handler;
        if (service.isSf())
            handler = new ServerFormHandler(proxy);
        else handler = new PguHandler(proxy);
        try {
            while (true) {
                new WebDriverWait(recordDriver, 10).until(new WaitIfClockIsDisplayed());
                try {
                    int currentStep;
                    try {
                        currentStep = recordDriver.getStepNum();
                    } catch (UnknownStepException e) {
                        currentStep = recordDriver.getStepNum();
                    }
                    if (currentStep != step.getNumStep()) {
                        try {
                            long t = System.currentTimeMillis();
                            if (stepMap != null) {
                                handler.handleStepMove(step, stepMap);
                            } else {
                                log.warn("Unable to find values on step " + step);
                            }
                            stepMap = null;
                            log.debug(System.currentTimeMillis() - t + "ms took to proceed step");
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                        }

                        step = createStep();
                        if (step == null) {
                            throw new UnknownStepException("Probably ended");
                        }
                        stepMap = recordDriver.stepMap(true);
                        list.add(step);
                    } else {
                        Map<String, Value> newMap = recordDriver.stepMap(false);
                        if (newMap != null) {
                            stepMap = newMap;
                        }
                        Thread.sleep(waitPeriod);
                    }
                } catch (UnknownStepException e) {
                    //TODO проверять, закончили ли Или ошибка какая
                    if (step != null && stepMap != null)
                        handler.handleStepMove(step, stepMap);
                    try {
                        new WebDriverWait(recordDriver, 20).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.form-text-common")));
//                        recordDriver.getApplicationInfo();
                    } catch (TimeoutException e1) {
                        log.warn("Application wasn't send");
                    }

                    break;
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (Exception e) {
//            log.error(e.getMessage(), e);
            if (e instanceof PortalException) {
                throw (PortalException) e;
            } else throw new PortalException(recordDriver, e.getMessage(), e);
        }
        return service;
    }

    /**
     * Starts service with browser and parses first step
     */
    private Map<String, Value> initService(Service service) throws BrokenServiceException {
        String url = service.getUrl();
        if (this.recordDriver != null) {
            if (service.isSf() && recordDriver instanceof RecordingSFPortalDriver
                    || !service.isSf() && recordDriver instanceof RecordingPortalDriver) {
                //do nothing
            } else {
                WebDriver driver = recordDriver;
                while (driver instanceof PortalDriver) {
                    driver = ((PortalDriver) driver).getDriver();
                }
                if (service.isSf())
                    recordDriver = new RecordingSFPortalDriver(new PortalDriverSF(driver));
                else recordDriver = new RecordingPortalDriver(new PortalDriver(driver));
            }
        }
        if (this.recordDriver == null)
            if (service.isSf())
                this.recordDriver = new RecordingSFPortalDriver(Driver.createNewWebDriver());
            else this.recordDriver = new RecordingPortalDriver(Driver.createNewWebDriver());

//        service.setSf(this.sf);

        service.setServiceValues(factory.createValuesType());
        recordDriver.get(url);

        if (!recordDriver.isLoggedIn()) {
            try {
                recordDriver.logIn();
            } catch (Exception e) {
                if (!recordDriver.isLoggedIn()) {
                    recordDriver.logIn();
                }
            }

        }
        log.debug("logged in");
        if (recordDriver.getTitle().contains("Личный кабинет")) {
            log.debug("going to " + url + ". again...");
            recordDriver.get(url);
        }

        log.debug("starting new declaration");
        recordDriver.startNewDeclaration();
        recordDriver.manage().timeouts().implicitlyWait(0, java.util.concurrent.TimeUnit.SECONDS);
        log.info("OrderId: " + recordDriver.getOrderId());
        return recordDriver.stepMap(true);
    }


    private Step createStep() {
        return createStep(recordDriver);
    }

    /**
     * @return null если на шаге "Предпросмотр формы"
     */
    private Step createStep(IPortalDriver driver) {
        String description = driver.getStepDescription();
        if (description.equals("Предпросмотр формы")) {
//            return null;
            log.info("Препросмотр формы");
//            throw new UnknownStepException("Предпросмотр формы");
            return null;
        }
        Step step = factory.createStep();
        step.setNumStep((byte) driver.getStepNum());
        step.setDescription(description);
        return step;
    }


    public void shutDown() {
        log.info("shutting down");
        try {
            proxy.stop();
        } catch (Exception e) {
            log.warn("Error while stopping proxy", e);
        }
        try {
            recordDriver.close();
        } catch (Exception e) {
            log.warn("Error while shutting down browser", e);
        }
    }
}

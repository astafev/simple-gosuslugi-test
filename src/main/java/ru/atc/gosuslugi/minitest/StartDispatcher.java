package ru.atc.gosuslugi.minitest;

import java.lang.reflect.InvocationTargetException;

/**
 * Date: 15.10.13
 * Author: astafev
 */
public class StartDispatcher {
    public static StartAppType type = StartAppType.pgu;
    public static final String USAGE =
            "-t <type> <args>\n" +
            "    select type. Available types:\n" +
            "        'pgu' - run pgu application. Default. Aliases: '1'\n" +
            "        'sir' - run pgu application. Default. Aliases: '2' \n" +
            "        'record' - run pgu application. Default. Aliases: '3'";
    public static void main(String[] args) {
        try{
            if(args.length>=2) {
                if(args[0].equals("-t")) {
                    if(args[1].equals("pgu")|| args[1].equals("1"))
                        type = StartAppType.pgu;
                    else if(args[1].equals("sir")|| args[1].equals("2"))
                        type = StartAppType.sir;
                    else if(args[1].equals("record")|| args[1].equals("3"))
                        type = StartAppType.record;
                    else throw new Exception("Unknown type: " + args[1] + "\nUsage: " + USAGE);
                    String[] newArgs = new String[args.length-2];
                    System.arraycopy(args, 2, newArgs, 0, args.length-2);
                    args = newArgs;
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(USAGE);
            return;
        }
        System.out.printf("\n---------------------\nRun type: %s\n-------------------%n", type.name());
        try {
            type.mainMethod.invoke(null, (Object)args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvocationTargetException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}

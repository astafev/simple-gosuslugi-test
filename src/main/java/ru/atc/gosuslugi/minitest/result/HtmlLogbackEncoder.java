package ru.atc.gosuslugi.minitest.result;

import ch.qos.logback.core.Context;
import ch.qos.logback.core.encoder.Encoder;
import ch.qos.logback.core.status.Status;

import java.io.IOException;
import java.io.OutputStream;


public class HtmlLogbackEncoder implements Encoder {

    @Override
    public void init(OutputStream os) throws IOException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doEncode(Object event) throws IOException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void close() throws IOException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setContext(Context context) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Context getContext() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addStatus(Status status) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addInfo(String msg) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addInfo(String msg, Throwable ex) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addWarn(String msg) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addWarn(String msg, Throwable ex) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addError(String msg) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addError(String msg, Throwable ex) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void start() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void stop() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isStarted() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}

package ru.atc.gosuslugi.minitest.result.db;

import freemarker.template.TemplateException;
import ru.atc.gosuslugi.minitest.result.ResultUtils;
import ru.atc.gosuslugi.minitest.result.Results;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Date: 20.12.13
 * Author: astafev
 */
public class Reader {
    public static void main(String[] args) throws IOException, URISyntaxException, TemplateException {
        ResultsDAO resultsDAO = new ResultsDAO();
        resultsDAO.init();
        List<Results> resultsList = resultsDAO.readAll();
        System.out.println(resultsList);

        File dir = new File("reports");
        if(dir.exists()) {
            if(!dir.isDirectory()) {
                System.err.println("Not directory");
                return;
            }
        } else {
            dir.mkdir();
        }
        for(Results results : resultsList) {
            File file = new File(dir, "report_" + results.getId() + ".html");
            Writer writer = new FileWriter(file);
            ResultUtils.writeFtlReport(writer, resultsList.get(resultsList.size()-1));
        }
    }
}

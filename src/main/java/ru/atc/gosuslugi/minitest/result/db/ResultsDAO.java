package ru.atc.gosuslugi.minitest.result.db;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistryBuilder;
import ru.atc.gosuslugi.minitest.result.Results;

import java.util.List;

/**
 * Date: 20.12.13
 * Author: astafev
 */
@SuppressWarnings("unchecked")
public class ResultsDAO {
    public SessionFactory sessionFactory;
//    private static ResultsDAO instance;

    public ResultsDAO() {

    }

    public void init() {
        Configuration configuration = new Configuration();
        configuration.configure(System.getProperty("hibernate.cfg.xml", "hibernate.cfg.xml"));
        ServiceRegistryBuilder serviceRegistryBuilder = new ServiceRegistryBuilder().applySettings(configuration
                .getProperties());
        sessionFactory = configuration
                .buildSessionFactory(serviceRegistryBuilder.buildServiceRegistry());
    }

    private Session session;

    private Session getSession() {
        if (session == null || !session.isOpen())
            session = sessionFactory.openSession();
        return session;
    }

    private void commit(Session session) {
        session.flush();
    }

    public void saveOrUpdate(Results results) {
        Session session = getSession();
        session.beginTransaction();
        session.saveOrUpdate(results);
        session.getTransaction().commit();
    }

    public Results read(Integer id) {
        Criteria criteria = getSession().createCriteria(Results.class)
                .add(Restrictions.eq("id", id));
        return (Results) criteria.uniqueResult();
    }

    public List<Results> readAll() {
        Criteria criteria = getSession().createCriteria(Results.class);
        List<Results> resultsList = criteria.list();
        return resultsList;
    }

    public List<Integer> readAllId() {
        Criteria criteria = getSession().createCriteria(Results.class).setProjection(Projections.property("id"));
        return (List<Integer>) criteria.list();
    }

    public void close() {
        if(session!=null && session.isOpen()) {
            session.flush();
            session.close();
        }
    }
}

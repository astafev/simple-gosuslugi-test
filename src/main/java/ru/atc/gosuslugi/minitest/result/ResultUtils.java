package ru.atc.gosuslugi.minitest.result;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;

import java.io.*;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Date: 23.09.13
 * Author: astafev
 */
public class ResultUtils {

    public static final Logger log = LoggerFactory.getLogger(ResultUtils.class);

    public static String toHTMLTable(Collection<Result> results) {
        StringWriter sw = new StringWriter();
        try {
            writeHtmlReport(sw, results);
        } catch (IOException e) {
            log.error("Impossible error. You can't see this message", e);
        }
        return sw.toString();
    }

    public static void writeSummaryTable(Writer writer, Collection<Result> results) throws IOException {
        writer.write("<table rules=\"all\" border=\"1\"><thead><tr><th width=\"90\">Услуга</th><th width=\"90\">Отправлено всего</th>" +
                "<th width=\"90\">успешно</th><th width=\"90\">с ошибками</th></tr></thead><tbody>");
        int error = 0;
        int all = results.size();
        Map<String, Integer> allSentServices = new LinkedHashMap<String, Integer>();
        Map<String, Integer> errorSent = new LinkedHashMap<String, Integer>();
        for (Result result : results) {
            String key = result.getServiceId().split("\\.")[0];
            if (!result.isSuccess()) {
                error++;
            }
            if (allSentServices.containsKey(key)) {
                allSentServices.put(key, allSentServices.get(key) + 1);
                if (!result.isSuccess())
                    errorSent.put(key, errorSent.get(key) + 1);
            } else {
                allSentServices.put(key, 1);
                errorSent.put(key, result.isSuccess() ? 0 : 1);
            }
        }
        writer.write("<tr><td>");
        writer.write("Всего");
        writer.write("</td><td style=\"text-align:right;\">");
        writer.write(String.valueOf(all));
        writer.write("</td><td style=\"text-align:right;\">");
        writer.write(String.valueOf(all - error));
        writer.write("</td><td style=\"text-align:right;\">");
        writer.write(String.valueOf(error));
        writer.write("</td></tr>");
        for (String service : allSentServices.keySet()) {
            writer.write("<tr><td>");
            writer.write(service.toString());
            writer.write("</td><td style=\"text-align:right;\">");
            writer.write(String.valueOf(allSentServices.get(service)));
            writer.write("</td><td style=\"text-align:right;\">");
            writer.write(String.valueOf(allSentServices.get(service) - errorSent.get(service)));
            writer.write("</td><td style=\"text-align:right;\">");
            writer.write(String.valueOf(errorSent.get(service)));
            writer.write("</td></tr>");
        }
        writer.write("</tbody></table>");
    }

    /**
     * <table rules="all" border="1"><thead><tr><th>Услуга</th><th>Результат</th><th>Скриншот</th></tr></thead><tbody>
     * <tr><td style="background-color:green;" colspan="2">11111 успешно отправлена</td><td></td></tr>
     * <tr><td style="background-color:red;" colspan="1">11111</td><td>{Exception}</td><td><a href="C:\tmp\screenshot_66077_1373272019616.png"></></td></tr>
     * </tbody></table>
     */
    public static void writeHtmlReport(Writer writer, Collection<Result> results) throws IOException {
        writer.write("<!DOCTYPE html><html><head><meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\"><title>Отчет " + new Date() + "</title></head><body><h1>тесты</h1>");
        writeSummaryTable(writer, results);
        writer.write("<br/><br/>");
        writer.write("<table rules=\"all\" border=\"1\"><thead><tr><th>Услуга</th><th>Скриншот</th><th>мэссадж</th><th>stack trace</th></tr></thead><tbody>");
        for (Result result : results) {
            writer.write("<tr>");
            if (result.isSuccess()) {
                writer.write("<td style=\"background-color:green;\" >");
                writer.write(result.getServiceId());
                if (result.getScreenshotPath() != null)
                    writer.write("<td><a href=\"file:///" + result.getScreenshotPath() + "\">" + result.getUrl() + "</a></td>");
                else writer.write("<td>Успешно отправлена</td>");
                writer.write("<td>" + result.getUrl() + "</td>");
                writer.write("<td/>");
            } else {
                writer.write("<td style=\"background-color:red;\" colspan=\"1\">");
                writer.write(result.getServiceId());
                writer.write("</td>");
                if (result.getScreenshotPath() != null)
                    writer.write("<td><a href=\"file:///" + result.getScreenshotPath() + "\">" + result.getUrl() + "</a></td>");
                else
                    writer.write("<td>Не удалось снять скриншот! Вероятно, возникли проблемы с браузером.\n" + result.getUrl() + "</td>");
                writer.write("<td>");
                writer.write(result.getException().getMessage());
                writer.write("</td><td><pre>");
                result.getException().printStackTrace(new PrintWriter(writer));
                writer.write("</pre></td>");
            }
            writer.write("</tr>");
        }
        writer.write("</tbody></table>");
        writer.write("</body></html>");
        writer.flush();
    }

    public static void writeFtlReport(Writer writer, Results results) throws IOException, URISyntaxException, TemplateException {
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("results", results);
        Service service = new Service();
        service.setUrl("Couldn't retrieve result");
        root.put("emptyService", service);
        writeFtlReport(writer, root, System.getProperty("ftl.reportFile", "report_template.ftl"));
    }

    public static void writeFtlReport(Writer writer, Map<String, Object> map, String template) throws IOException, URISyntaxException, TemplateException {
//        File templateFile = new File(ClassLoader.getSystemResource(System.getProperty("ftl.reportFile", "report_template.ftl")).toURI());
//        if(!templateFile.exists()) {
//            throw new FileNotFoundException("Can't locate report template " + templateFile.getAbsolutePath());
//        }
        freemarker.template.Configuration ftlConfig = new Configuration();
        ftlConfig.setDefaultEncoding("UTF-8");
        ftlConfig.setClassForTemplateLoading(System.class, "/");
        Template ftlTemplate = ftlConfig.getTemplate(template);
        ftlTemplate.process(map, writer);
    }



    public static void setResult(Exception e, Status status, Result result, Driver portal) {
        if (e != null) {
            if (!(e instanceof PortalException))
                e = new PortalException(portal, "Произошла ошибка! " + e.getMessage(), e);
            if(((PortalException)e).brokenBrowser) {
                log.info("Going to re-init browser");
            }
            result.setException(e);
            result.setScreenshotPath(((PortalException) e).screenshotFile);
            result.setUrl(((PortalException) e).url);
        }
        result.setEndDate(new Date());
        result.setStatus(status);
    }

    public static void writeFtlReport(Writer writer, List<Result> results) throws TemplateException, IOException, URISyntaxException {
        Results results1 = new Results();
        results1.setResults(results);
        results1.setEndDate(new Date());
        writeFtlReport(writer, results1);
    }
}

package ru.atc.gosuslugi.minitest.result;


import org.slf4j.*;
import ru.atc.gosuslugi.minitest.result.db.ResultsDAO;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;

import javax.persistence.*;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

@Entity(name = "AUTOTEST_RUNS")
public class Results implements Iterable<Result>{
    public static final Logger log = LoggerFactory.getLogger(Results.class);

    private List<Result> results;

    public Integer id;
    public Date startDate;
    public Date endDate;

    public ResultsDAO resultsDAO;

    public void init(Services services) {
        init();
        services = services.splitAllByUrl().splitAllByUserSelectedRegion();
        for (Service service : services.getServices()) {
            if (service.getUrls() != null && service.getUrls().getUrl() != null && service.getUrls().getUrl().size() > 0) {
                for (Service service1 : service.splitByURL().getServices()) {
                    results.add(new Result(service1, this));
                }
            } else {
                results.add(new Result(service, this));
            }

        }
    }

    public void init() {
        results = new ArrayList<Result>();
        this.startDate = new Date();
        this.resultsDAO = new ResultsDAO();
        this.resultsDAO.init();
//        this.updateDB();
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_AUTOTEST_RUN")
    @SequenceGenerator(name = "SEQ_AUTOTEST_RUN", allocationSize = 1)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "STARTDATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ENDDATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Results() {

    }

    //    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
//    @JoinColumn(name = "RUNID")
//    @ForeignKey(name="FK_RESULT")
//    @Transient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "results", cascade = CascadeType.ALL)
    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public void updateDB() {
        resultsDAO.saveOrUpdate(this);
    }

    public void addResult(Result result) {
        result.setResults(this);
        getResults().add(result);
        updateDB();
    }

    public void over() {
        setEndDate(new Date());
        resultsDAO.saveOrUpdate(this);
        resultsDAO.close();

    }

    @Override
    public Iterator<Result> iterator() {
        return getResults().iterator();
    }

    @Transient
    public String getSummaryTable() throws IOException {
        StringWriter sw = new StringWriter();
        ResultUtils.writeSummaryTable(sw, results);
        return sw.toString();
    }

    @Override
    public String toString() {
        return "Results{" +
                "\nresults=" + results +
                ",\n id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}

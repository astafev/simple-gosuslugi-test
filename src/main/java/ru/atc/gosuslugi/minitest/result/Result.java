package ru.atc.gosuslugi.minitest.result;

import org.apache.tools.ant.util.ReaderInputStream;
import org.hibernate.annotations.Type;
import org.postgresql.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import ru.atc.gosuslugi.minitest.util.ConfigUtils;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;

import javax.persistence.*;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Entity
@Table(name = "AUTOTEST_RESULTS")
public class Result {
    public Integer id;
    public Exception exception;
    public String serviceId;

    public String screenshotPath;
    public String url;
    public String orderId;
    public boolean SF;
    String errorMessage;

    public Service service;
    public Map<String, String> application;

    private Date endDate;
    private Date startDate;

    private Results results;

    private String additionalInfo;
    public static Logger log = LoggerFactory.getLogger(Results.class);

    @Column(name = "serviceid")
    public String getServiceId() {
        if (serviceId == null) {
            if (service == null) {
                return null;
            }
            this.serviceId = service.getServiceid();
            return serviceId;
        } else return serviceId;

    }

    public void setServiceId(String serviceid) {
        this.serviceId = serviceid;
    }

    public Result(Service service, Results results) {
        this.service = service;
        this.results = results;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RUNID")
    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public Result() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
    @SequenceGenerator(name = "SEQ_STORE")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Result(String serviceId, Service service) {
        this.serviceId = serviceId;
        this.service = service;
        this.SF = service.isSf();
    }

    public Result(Service service) {
        this(service.getServiceid(), service);
    }

    public Result(Service service, Exception e) {
        this(service);
        this.exception = e;
        if (e instanceof PortalException) {
            screenshotPath = System.getProperty("screenshotsPath") + ((PortalException) e).screenshotFile;
            url = ((PortalException) e).url;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Service: " + serviceId);
        if (exception == null) {
            sb.append("\nNo errors found");
        } else {
            StringWriter stringWriter = new StringWriter();
            exception.printStackTrace(new PrintWriter(stringWriter));
            sb.append("\nERROR: " + stringWriter);
            sb.append("\nscreenshot: \"" + screenshotPath + "\"");
        }
        return sb.toString();
    }


    @Type(type = "org.hibernate.type.StringClobType")
    @Column(name = "exception")
    public String getSerializedException() throws IOException {
        if (exception == null)
            return null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(exception);
        baos.close();
        String result = Base64.encodeBytes(baos.toByteArray());
        return result;
    }

    public void setSerializedException(String serializedException) throws IOException, ClassNotFoundException {
        if (serializedException != null) {
            byte[] bytes = Base64.decode(serializedException);
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
            this.exception = (Exception) ois.readObject();
        } else this.exception = null;
    }

    @Transient
    public Exception getException() {
        return exception;
    }

    public void setException(Exception e) {
        this.exception = e;

    }


    @Transient
    public Service getService() {
        return service;
    }

    //    @Lob
    @Type(type = "org.hibernate.type.StringClobType")
    @Column(name = "serviceAsXML")
    public String getServiceAsXML() throws JAXBException {
        if (service == null)
            return null;
        else {
            service.setUrls(null);
            return service.toXML();
        }
    }


    public void setServiceAsXML(String xml) throws JAXBException, FileNotFoundException, SAXException {
        try{
            ConfigUtils.readServices(new ReaderInputStream(new StringReader(xml)), false);
        } catch (Exception e) {
            log.error("Can't set service\n" + xml, e);
        }
    }


    @Column(name = "screenshot", length = 255)
    public String getScreenshotPath() {
        return screenshotPath;
    }

    public void setScreenshotPath(String screenshotPath) {
        this.screenshotPath = screenshotPath;
    }

    @Column(name = "URL", length = 256)
    public String getUrl() {
        if(url == null || url.length()<=256)
            return url;
        else return url.substring(0, 255);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "orderId", length = 50)
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Transient
    public Map<String, String> getApplication() {
        return application;
    }

    @Type(type = "org.hibernate.type.StringClobType")
    @Column(name = "applicationMap")
    public String getApplicationMapAsString() throws IOException {
        /*if (getApplication() == null)
            return null;

        StringBuilder result = new StringBuilder();
        for (String key : application.keySet()) {
            result.append(key).append(" : ").append(application.get(key)).append("\n");
        }
        return result.toString();                     /***/
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(application);
        baos.close();
        String result = Base64.encodeBytes(baos.toByteArray());
        return result; /**/
    }

    public void setApplicationMapAsString(String mapAsString) throws IOException, JAXBException, SAXException, ClassNotFoundException {
//        byte[] bytes = Base64.decode(mapAsString);
//        try {
//            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        String[] rows = mapAsString.split("\n");
        Map<String, String> appMap = new LinkedHashMap<String, String>(rows.length);
        for(int i = 0; i<rows.length; i++) {
            String[] row = rows[i].split(" : ");
            if(row.length == 2) {
                appMap.put(row[0], row[1]);
            } else {
                log.warn("Couldn't parse string: " + Arrays.asList(row));
            }
        }
        this.setApplication(appMap);
    }

    public void setApplication(Map<String, String> application) {
        this.application = application;
        if (orderId == null)
            orderId = application.get("номер заявления");
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startdate")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "enddate")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date date) {
        this.endDate = date;
    }

    public Status status;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 25)
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Transient
    public boolean isSuccess() {
        if (exception == null) {
            return true;
        } else
            return false;
    }

    @Column(name = "ERRORMESSAGE", length = 256)
    public String getErrorMessage() {
        if (exception == null)
            return null;
        else {
            if (exception.getLocalizedMessage().length() > 255)
                return exception.getLocalizedMessage().substring(0, 255);
            else return exception.getLocalizedMessage();
        }
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    @Column(name = "AFTER", length = 50)
    public String getAfterAction() {
        if (service == null)
            return null;
        else return service.getAfter();
    }

    public void setAfterAction(String afterAction) {
        //do nothing
    }

    @Type(type = "org.hibernate.type.StringClobType")
    @Column(name = "ADD_INFO")
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}

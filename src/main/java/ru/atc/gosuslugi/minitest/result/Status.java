package ru.atc.gosuslugi.minitest.result;

/**
 * Date: 23.09.13
 * Author: astafev
 */
public enum Status {
    RUNNING,            //сейчас заполняется
    TO_RUN,             //работа над услугой впереди (возможно не используется)
    SENT,               //Отправлено
    ERROR_IN_SENDING,   //Ошибка при отправке
    IN_QUEUE,           //Статус заявления ПГУ "Поставлено в очередь на обработку"

    ERROR_WHILE_FILLING,
    ERROR_WHILE_LOGIN,

    BROKEN_SERVICE,

    CANCELED_IN_SIR,
    ERROR_CANCELING_IN_SIR,

    ERROR_CHECKING_IN_SIR,

    UNKNOWN_ERROR,
    WRONG_USAGE_ERROR
}

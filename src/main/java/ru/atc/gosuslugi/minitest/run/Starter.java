package ru.atc.gosuslugi.minitest.run;

import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.result.Results;
import ru.atc.gosuslugi.minitest.util.ConfigUtils;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.ResultUtils;

import java.awt.Desktop;
import java.io.*;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

public class Starter {
    public static Services services;
    public static Logger log = LoggerFactory.getLogger(Starter.class);
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH mm ss", Locale.getDefault());
    public static String reportFile = "report %date%.html";

    public static void main(String[] args) {
        AbstractRunner runner = null;
        try {
            log.info("-----------------------------------------\n                 Starting ... Date: " + new Date());
            ConfigUtils.init(args);

            if (services.getServices().size() == 0) {
                log.error("Didn't found services in specified files.");
            }
            if (log.isDebugEnabled())
                log.debug("System properties: " + System.getProperties().toString());

            Integer version;
            try{
                version = Integer.valueOf(System.getProperty("autotest.runner.version", "2"));
            } catch (NumberFormatException e) {
                log.warn("wrong property \"autotest.runner.version\"", e);
                version = 2;
            }
            if(version == 1)
                runner = Runner.getInstance();
            else
                runner = RunnerV2.getInstance();
            Results results = new Results();
            results.init();

            runner.createDrivers();
            runner.setResults(results);
            runner.run(services);
            runner.waitFor();
            log.info("\n\n-----------------------------------------------------------------------\n                   RESULTS\n" + runner.getResults());
            runner.quit();

            log.info("Runner quited");
            File reports = new File("reports");
            if(!reports.exists()) {
                if(!reports.mkdir()) {
                    log.error("unable to create directory \"reports\"");
                    reports = null;
                }
            }
            File report = new File(reports, reportFile.replaceAll("%date%", DATE_FORMAT.format(new Date())));
            OutputStream os = new BufferedOutputStream(new FileOutputStream(report));
            Writer writer = new OutputStreamWriter(os, Charset.forName("UTF-8"));
            try{
                if(runner instanceof RunnerV2)
                    ResultUtils.writeFtlReport(writer, ((RunnerV2)runner).getResult());
                else
                    ResultUtils.writeFtlReport(writer, runner.getResults());
            } catch (Exception e) {
                log.error("Ошибка при создании ftl-отчета", e);
                ResultUtils.writeHtmlReport(writer, runner.getResults());
            }
            writer.close();
            runner = null;
            if(args.length!=0 && (args[0].equalsIgnoreCase("showReport") || args[0].equalsIgnoreCase("openReport")))
                Desktop.getDesktop().open(report);

        } catch (WebDriverException e) {
            log.error("ERROR!!! " + e.getLocalizedMessage() + "\nПроверьте правильность установки браузера", e);
        } catch (Exception e) {
            log.error("ERROR!!! " + e.getLocalizedMessage(), e);
        } finally {
            if(runner!=null)
                runner.quit();
        }
    }



    private static Map<String,Object> genModel(List<Result> results) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("reports", results);
        return model;
    }


}

package ru.atc.gosuslugi.minitest.run;

import org.xml.sax.SAXException;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.Results;

import javax.xml.bind.JAXBException;
import java.util.LinkedList;
import java.util.List;

/**
 * Date: 24.09.13
 * Author: astafev
 */
public class StartTest {
    public static void main(String[] args) throws JAXBException, SAXException {
        Results results = new Results();
        List<Result> resultsList = new LinkedList<Result>();
        resultsList.add(new Result());
        resultsList.add(new Result());
        results.init();
        results.addResult(new Result());
//        results.setResults(resultsList);

        results.updateDB();
        /*ConfigUtils.init(args);
        Results results = new Results();
        results.init(Starter.services);
        ((List<Result>)results.getResults()).get(0).setException(new RuntimeException("ХУЙ!"));
        results.updateDB();
        Session session = results.sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Result");
        List list = query.list();
        System.out.println(list);*/
    }
}

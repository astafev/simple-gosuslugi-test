package ru.atc.gosuslugi.minitest.run;

/**
 * Date: 27.09.13
 * Author: astafev
 */
public abstract class AfterActionRunner {

    public abstract void run() throws Exception;

    public abstract void quit();

    public abstract void waitFor() throws InterruptedException;
}

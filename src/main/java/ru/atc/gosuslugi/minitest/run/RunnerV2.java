package ru.atc.gosuslugi.minitest.run;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.After;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.Results;
import ru.atc.gosuslugi.minitest.result.Status;
import ru.atc.gosuslugi.minitest.web.sir.SirRunner;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Date: 23.09.13
 * Author: astafev
 */
public class RunnerV2 extends AbstractRunner {

    public static final Logger log = LoggerFactory.getLogger(Runner.class);
    private static RunnerV2 instance;
    ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
    List<Future<Result>> futures = new LinkedList<Future<Result>>();

    private RunnerV2() throws Exception {
        super();
    }

    public static RunnerV2 getInstance() throws Exception {
        if (instance == null) {
            instance = new RunnerV2();
        }
        return instance;
    }

    public void run(Services services) throws InterruptedException {
        services = services.splitAllByUrl().splitAllByUserSelectedRegion();
        if (!this.isOver()) {
            throw new RuntimeException("Previous process is not ended yet");
        }
        for (int i = 0; i < services.getServices().size(); i++) {
            if (Thread.currentThread().isInterrupted()) {
                this.quit();
                return;
            }
            runService(services.getServices().get(i));
            log.debug(i + 1 + " of services started, " + (services.getServices().size() - i - 1) + " to do");
        }
    }

    public void runService(Service s) throws InterruptedException {
        //ждем тут!
        final PortalDriver driver = freeDrivers.take();
        if(driver.brokenDriver) {
            driver.reinit();
        }
        Callable<Result> runInstance = new RunServiceInstanceV2(s, driver);
//            runInstanceQueue.add(runInstance);
        Future<Result> futureResult = executor.submit(runInstance);
        futures.add(futureResult);
    }

    @Override
    public void quit() {
        executor.shutdown();
        killAllBrowsers();
        try {
            if(results!=null)
                results.updateDB();
        } catch (Exception e) {
            log.error("error while updating db", e);
            log.info(results.getResults().toString());
        }
        instance = null;
    }

    @Override
    public void waitFor() throws InterruptedException {
        while (futures.size() > 0) {
            Iterator<Future<Result>> iterator = futures.iterator();
            while (iterator.hasNext()) {
                Future<Result> future = iterator.next();
                if (future.isDone()) {
                    iterator.remove();
                    try {
                        results.addResult(future.get());
                    } catch (ExecutionException e) {
                        log.error("impossible error. It shouldn't happen", e);
                    } catch (HibernateException e) {
                        log.warn("Couldn't update database", e);
                    }
                }
            }
            Thread.sleep(3000);
        }

        //ждем, т.к. заявки в проде могут стартовать не скоро
        String beforeAfterTimeout = System.getProperty("ru.atc.beforeAfterWaitPeriod");
        if(beforeAfterTimeout!=null) {
            try {
                int sec2wait = Integer.parseInt(beforeAfterTimeout);
                Thread.sleep(sec2wait*1000);
            } catch (NumberFormatException e) {
                log.error("Property ru.atc.beforeAfterWaitPeriod has wrong format. " + e.getLocalizedMessage(), e);
            }
        }

        for (Result result : results) {
            String afterAction = result.getAfterAction();

            if (afterAction != null && !afterAction.isEmpty() && !afterAction.equals("NOTHING") && result.getStatus() == Status.SENT) {
                AfterActionRunner sirRunner;
                switch (After.valueOf(afterAction)) {
                    case SIR_CANCEL:
                    case SIR_CANCEL_JS:
                        log.debug("going to cancel " + result.getApplication() + "");
                    case SIR_CHECK:
                        log.debug("going to check " + result.getApplication() + "");
                        sirRunner = new SirRunner(freeDrivers, drivers, results);
                        break;
                    default:
                        throw new UnsupportedOperationException("Not implemented yet");
                }

                try {
                    sirRunner.run();
                    sirRunner.waitFor();
                } catch (Exception e) {
                    log.error("Error occured during executing SIR_CANCEL action!", e);
                    if (result.isSuccess()) {
                        result.setStatus(Status.UNKNOWN_ERROR);
                        result.setException(e);
                    }
                }
                break;
            }
        }
        log.debug("Ending...");
        try {
            results.over();
        } catch (HibernateException e) {
            log.warn("Couldn't update database! " + results, e);
        }
    }

    @Override
    public List<Result> getResults() {
        return results.getResults();
    }

    public Results getResult() {
        return results;
    }


}
package ru.atc.gosuslugi.minitest.run;

import ru.atc.gosuslugi.minitest.result.Result;

/**
 * Date: 18.12.13
 * Author: astafev
 */
public interface ICustomTest {

    public void test(Result result);
}

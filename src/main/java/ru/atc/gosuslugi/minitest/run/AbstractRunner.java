package ru.atc.gosuslugi.minitest.run;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.internal.Killable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.Results;
import ru.atc.gosuslugi.minitest.util.DaemonThreadFactory;

import java.util.*;
import java.util.concurrent.*;
import static ru.atc.gosuslugi.minitest.web.Driver.createNewWebDriver;
/**
 * Date: 23.09.13
 * Author: astafev
 */
public abstract class AbstractRunner {
    public static final Logger log = LoggerFactory.getLogger(AbstractRunner.class);
    protected int numberOfThreads;
    protected final List<PortalDriver> drivers;
    protected final BlockingQueue<PortalDriver> freeDrivers;
    protected Results results;

    protected AbstractRunner() {
        try {
            numberOfThreads = Integer.valueOf(System.getProperty("numberOfThreads", "1"));
        } catch (NumberFormatException e) {
            numberOfThreads = 1;
        }

        drivers = new ArrayList<PortalDriver>(numberOfThreads);
        freeDrivers = new LinkedBlockingQueue<PortalDriver>(numberOfThreads);
    }

    public void createDrivers() throws Exception {

        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads, new DaemonThreadFactory());
        List<Future<PortalDriver>> futures = new LinkedList<Future<PortalDriver>>();

        for (int i = 0; i < numberOfThreads; i++) {
            Callable<PortalDriver> callable = new Callable<PortalDriver>() {
                @Override
                public PortalDriver call() {
                    return createNewWebDriver();
                }
            };
            futures.add(executor.submit(callable));
        }
        try {
            for (Future<PortalDriver> future : futures) {

                PortalDriver driver = future.get();
                drivers.add(driver);
                freeDrivers.add(driver);
            }
        } catch (Exception e) {
            killAllBrowsers();
//            if (e instanceof ExecutionException && false)
//                throw (RuntimeException) e.getCause();
            throw e;
        } finally {
            executor.shutdownNow();
        }
    }

    public abstract void waitFor() throws InterruptedException;

    public abstract void run(Services services) throws Exception;




    public abstract void quit();

    public abstract List<Result> getResults();

    public boolean isOver() {
        if (freeDrivers.size() == numberOfThreads) {
            return true;
        }
        return false;
    }

    public void killAllBrowsers() {
        if (drivers != null) {
            for (WebDriver driver : drivers) {
                try {
                    driver.quit();
                } catch (Exception e) {
                    log.warn("error occured during closing browser ", e);
                    if (driver instanceof Killable) {
                        ((Killable) driver).kill();
                    }
                }
            }
        }
    }

    public void setResults(Results results) {
        this.results = results;
    }
}

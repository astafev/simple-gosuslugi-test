package ru.atc.gosuslugi.minitest.run.customTest;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.web.portal.WaitIfClockIsDisplayed;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.ResultUtils;
import ru.atc.gosuslugi.minitest.result.Status;
import ru.atc.gosuslugi.minitest.run.ICustomTest;

/**
 * Date: 18.12.13
 * Author: astafev
 */
public class LibCustomTest implements ICustomTest {
    final PortalDriver driver;
    public static Logger logger = LoggerFactory.getLogger(LibCustomTest.class);


    public LibCustomTest(PortalDriver driver) {
        this.driver = driver;
    }

    @Override
    public void test(Result result) {
        try {
            Service service = result.getService();

            driver.findElement(By.id("ajax")).click();
            new WebDriverWait(driver, 30).until(new WaitIfClockIsDisplayed());

            String css = service.getCustomParamValue("messageCss");
            WebElement message;
            By by;
            if (css==null) {
                String id = service.getCustomParamValue("messageId");
                by = By.id(id);
            } else by = By.cssSelector(css);

            message = new WebDriverWait(driver, 5).until(ExpectedConditions.presenceOfElementLocated(by));

            String css2 = service.getCustomParamValue("messageAddCss");
            if(css2!=null)
                message = message.findElement(By.cssSelector(css2));

            if (message.isDisplayed()) {
                result.setStatus(Status.SENT);
                result.setAdditionalInfo(message.getText());
            }
        } catch (NoSuchElementException e) {
            ResultUtils.setResult(e, Status.ERROR_IN_SENDING, result, driver);
        } catch (TimeoutException e) {
            ResultUtils.setResult(e, Status.ERROR_IN_SENDING, result, driver);
        } catch (NullPointerException e) {
            ResultUtils.setResult(e, Status.WRONG_USAGE_ERROR, result, driver);
        } catch (RuntimeException e) {
            ResultUtils.setResult(e, Status.WRONG_USAGE_ERROR, result, driver);
        }
    }
}

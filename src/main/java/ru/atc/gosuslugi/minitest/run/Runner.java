package ru.atc.gosuslugi.minitest.run;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriverSF;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Services;
import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.web.portal.UnsendException;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.ResultUtils;
import ru.atc.gosuslugi.minitest.result.Status;

import java.util.*;

public class Runner extends AbstractRunner{

    private static Runner instance;
    public static final Logger log = LoggerFactory.getLogger(Runner.class);
    //    private final ExecutorService executor;
    private List<Result> results;

    protected Runner() throws Exception {
    }


    public static Runner getInstance() throws Exception {
        if (instance == null) {
            instance = new Runner();
        }
        return instance;
    }



    public List<Result> getResults() {
        return results;
    }

    @Override
    public void waitFor() throws InterruptedException {
        while (!isOver()) {
            Thread.sleep(5000);
        }
    }

    public void run(Services services) throws InterruptedException {
        results = Collections.synchronizedList(new ArrayList<Result>(services.getServices().size()));
        for (int i = 0; i < services.getServices().size(); i++) {
            if (Thread.currentThread().isInterrupted()) {
                this.quit();
                return;
            }
            Service service = services.getServices().get(i);
            //ждем тут!
            final PortalDriver driver = freeDrivers.take();
            try {
                log.debug(i + " of runs overed, " + (services.getServices().size() - i) + " to do");
                runService(service, driver);
            } catch (UnreachableBrowserException e) {

                log.error("", e);
                driver.close();
//                freeDrivers.add(createNewWebDriver());

            } catch (Exception e) {
                log.error("", e);
            }
        }
    }

    private void runService(Service service, PortalDriver driver) {
        RunInstance runInstance;
        if (service.isSf()) {
            runInstance = new RunInstance(new PortalDriverSF(driver), service);
        } else
            runInstance = new RunInstance(driver, service);

        Thread thread = new Thread(runInstance);
//            thread.setDaemon(true);
        log.debug("created new thread. " + thread.getName() + "\nwebDriver: " + driver);
        thread.start();
    }


    public void quit() {
        killAllBrowsers();
        instance = null;
    }

    class RunInstance implements Runnable {
        final Service service;
        PortalDriver driver;
        Exception exception;

        RunInstance(final WebDriver loggedInDriver, final Service service) {
            if (loggedInDriver == null) {
                driver = Runner.instance.freeDrivers.peek();
            } else {
                if (loggedInDriver instanceof PortalDriver) {
                    this.driver = (PortalDriver) loggedInDriver;
                } else this.driver = new PortalDriver(loggedInDriver);
            }
            this.service = service;
        }

        public Exception getException() {
            return exception;
        }

        @Override
        public void run() {
            if (service.getUrl() == null || service.getUrl().equals("")) {
                if (service.getUrls() != null)
                    for (int i = 0; i < service.getUrls().getUrl().size(); i++) {
//                        service.setServiceid();
                        //TODO что-нибудь сделать чтоб в конечном отчете отличались друг от друга
                        service.setUrl(service.getUrls().getUrl().get(i));
                        runServiceWithSetUrl(service);
                    }
            } else runServiceWithSetUrl(service);
            Runner.instance.freeDrivers.add(driver);
        }


        public Result runServiceWithSetUrl(Service service) {
            try {
                driver.get(service.getUrl());
                if (!driver.isLoggedIn()) {
                    driver.logIn();
                }
            } catch (Exception e) {
                log.error("Error occured during login.", e);

                if (!(e instanceof PortalException)) {
                    e = new PortalException(driver, "Error occurred during login. " + e.getMessage(), e);
                }
                Result result = new Result(service, e);
                Runner.instance.results.add(result);
                freeDrivers.add(driver);
                return result;
            }

            log.debug("Starting run of service " + service.getServiceid());
            Result result = null;
            try {
                if (service.getUrl() == null) {
                    throw new NullPointerException("URL of service can't be empty!");
                }
                try {
                    driver.get(service.getUrl());
                } catch (org.openqa.selenium.TimeoutException e) {
                    //типа обновим
                    driver.get(service.getUrl());
                }
                if (service.getUserSelectedRegion() != null && !service.getUserSelectedRegion().getRegions().isEmpty())
                    driver.selectRightRegion((String[]) service.getUserSelectedRegion().getRegions().toArray());
//                web.run(service.getUserSelectedRegion().getRegions().toArray(new String[0]));
                driver.startNewDeclaration();
                List<Step> steps = service.getServiceValues().getSteps();
                for (int i = 0; i < steps.size(); i++) {
                    Step thisStep = steps.get(i);
                    driver.fillPage(thisStep);
                    if (thisStep.getStepValues().size() > 0 && thisStep.getStepValues().get(thisStep.getStepValues().size() - 1).getType().equals("button")) {
                        //Если последнее - какая-нибудь кнопка ниче не делаем
                        if (service.getServiceValues().getSteps().size() - 1 == i) {
                            result = new Result(service);
                            result.setUrl(driver.getCurrentUrl());
                            String screenshot = null;
                            try {
                                screenshot = service.getServiceid() + "." + System.currentTimeMillis() + ".png";
                                driver.takeScreenshot(screenshot);
                            } catch (Exception e) {
                                log.error("", e);
                            }
                            result.setScreenshotPath(screenshot);
                        }
                    } else if (service.getServiceValues().getSteps().size() - 1 != i)
                        driver.goToNextStep(thisStep.getNumStep(), steps.get(i + 1).getNumStep());
                    else {
                        result = new Result(service);
                        Map<String, String> applicationInfo = driver.sendApplication();
                        result.setUrl(driver.getCurrentUrl());
                        String status = applicationInfo.get("статус");
                        try {
                            if (status.equals("Поставлено в очередь на обработку")) {
                                Thread.sleep(3000);
                                driver.navigate().refresh();
                                driver.addApplicationInfo(applicationInfo);
                                result.setApplication(applicationInfo);
                                status = applicationInfo.get("статус");
                            }
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        if (!PortalDriver.allowedStatuses.contains(status)) {
                            ResultUtils.setResult(new UnsendException(driver, "Status: " + status), Status.ERROR_IN_SENDING, result, driver);
                        } else if (status.equals("Поставлено в очередь на обработку")) {
                            ResultUtils.setResult(new UnsendException(driver, "Поставлено в очередь на обработку спустя 3 секунды"), Status.IN_QUEUE, result, driver);
                        }

                    }

                }
            } catch (PortalException e) {
                exception = e;
//                log.error("Exception: ", exception);
            } catch (Exception e) {
                PortalException e1 = new PortalException(driver, "", e);
                exception = e1;
//                log.error("Exception: ", exception);
            } finally {
                if (result == null) {
                    result = new Result(service, exception);
                }
                result.setException(exception);
                Runner.instance.results.add(result);
                Throwable realException = exception;
                if (realException instanceof PortalException) {
                    realException = exception.getCause();
                } else log.error("\n\n\n--------------------------------------\nNOT A PORTAL EXCEPTION!!!!!!!!!!!!!!!!\n", realException);
                if (realException instanceof org.openqa.selenium.NoSuchWindowException || realException instanceof UnreachableBrowserException) {
                    driver.quit();
                    log.error("Проблема с окном, перезапускаю браузер");
                    driver = Driver.createNewWebDriver();
                }
                log.info("Service run shutting down.\nService" + service +
                        "\n-------------------------------\nResult: " + result + "\n---------------------------------\nweb:" + driver);
                return result;
            }
        }
    }
}



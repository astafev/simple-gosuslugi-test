package ru.atc.gosuslugi.minitest.run;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.IDriver;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriverSF;
import ru.atc.gosuslugi.minitest.web.portal.UnsendException;
import ru.atc.gosuslugi.minitest.web.portal.WaitIfClockIsDisplayed;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.ResultUtils;
import ru.atc.gosuslugi.minitest.result.Status;

import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Date: 25.09.13
 * Author: astafev
 */
public class RunServiceInstanceV2 implements Runnable, Callable<Result> {
    final Result result;
    final PortalDriver portal;
    Service service;
    public static final Logger log = LoggerFactory.getLogger(RunServiceInstanceV2.class);

    public RunServiceInstanceV2(Result result, PortalDriver portal) {
        this.result = result;
        this.portal = portal;
        this.service = result.getService();
    }

    /**
     * Для использования с Callable интерфейсом
     */
    public RunServiceInstanceV2(Service service, PortalDriver portal) {
        result = new Result(service);
        if (service.isSf())
            this.portal = new PortalDriverSF(portal);
        else
            this.portal = portal;
        this.service = result.getService();
    }

    @Override
    public void run() {
        if (result == null) {
            log.error("result is not initialized. WRONG USAGE! Nobody can't see results. Fuck you!");
        } else call();
    }

    public void setResult(Exception e, Status status) {
        ResultUtils.setResult(e, status, result, portal);
        log.debug("setting result " + result, e);
        try {
            RunnerV2.getInstance().freeDrivers.add(portal);
        } catch (Exception e1) {
            log.error("Impossible error Couldn't add web to free drivers! " + e1.getMessage(), e1);
        }
    }

    public void checkAndLogIn() {
        portal.get(service.getUrl());
        if (!portal.isLoggedIn()) {
            portal.logIn();
        } else {
            log.debug("already logged in");
        }
    }

    @Override
    public Result call() {
        Thread.currentThread().setName(String.format("%s %d", service.getServiceid(), Thread.currentThread().getId()));
        result.setStartDate(new Date());
        portal.get(service.getUrl());
        if(result.getApplication() == null) {
            result.setApplication(new LinkedHashMap<String, String>());
        }


        if (portal.getTitle().contains("Информация об услуге")) {
            try {
                //отображается ли ошибка на карточке услуги
                WebElement element = portal.getDisplayed(By.className("title-error"));
                if (element!=null) {
                    log.error(element.getText());
                    String text = element.findElement(By.xpath("..")).getText();
                    setResult(new PortalException(portal, text), Status.BROKEN_SERVICE);
                    return result;
                }
            } catch (Exception e) {
                log.debug("error while getting info about service", e);
                setResult(e, Status.UNKNOWN_ERROR);
                return result;
            }
        }
        //логинимся, если надо
        try {
            checkAndLogIn();
        } catch (Exception e) {
            log.error("Error occured during login.", e);
            setResult(e, Status.ERROR_WHILE_LOGIN);
            return result;
        }

        //стартуем услугу
        log.debug("Starting run of service " + service.getServiceid());
        try {
            portal.get(service.getUrl());
            portal.addApplicationInfo(result.getApplication());
            if (service.getUserSelectedRegion() != null && !service.getUserSelectedRegion().getRegions().isEmpty())
                portal.selectRightRegion((String[]) service.getUserSelectedRegion().getRegions().toArray());
            portal.startNewDeclaration();
        } catch (org.openqa.selenium.TimeoutException e) {
            log.debug("error while starting service", e);
            if (!Boolean.valueOf(System.getProperty("fuckingStable", "true"))) {
                setResult(e, Status.BROKEN_SERVICE);
                return result;
            } else {//типа обновим
                log.warn("Going to try to skip possible portal error");
                if(portal.getCurrentUrl().equals(service.getUrl()))
                    portal.navigate().refresh();
                else portal.get(service.getUrl());
            }
        } catch (Exception e) {
            setResult(e, Status.BROKEN_SERVICE);
            return result;
        }
        //Заполняем поля
        try {
            List<Step> steps = service.getServiceValues().getSteps();
            for (int i = 0; i < steps.size(); i++) {
                Step thisStep = steps.get(i);
                List<Value> values = steps.get(i).getStepValues();
                (new WebDriverWait(portal, 20))
                        .until(new WaitIfClockIsDisplayed());
                portal.fillPage(values);
                if (result.getOrderId() == null)
                    result.setOrderId(portal.getOrderId());
                if (values.size() > 0
                        && steps.size() - 1 != i                                     //если последний шаг - идем дальше
                        && !values.get(values.size() - 1).getType().equals("button") //Если последнее - какая-нибудь кнопка ниче не делаем
                        ) {
                    portal.goToNextStep(thisStep.getNumStep(), steps.get(i + 1).getNumStep());
                }
            }
        } catch (Exception e) {
            log.debug("error while filling service steps " + service.getServiceid(), e);
            setResult(e, Status.ERROR_WHILE_FILLING);
            return result;
        }
        //Отправляем заявление
        if (service.getCustomTest() != null) {
            customTest(result, service.getCustomTest());
        } else {
            try {
                Map<String, String> applicationInfo = portal.sendApplication();
                result.setApplication(applicationInfo);
                String status = applicationInfo.get("статус");
                if (status.equals("Поставлен в очередь на обработку") && portal instanceof PortalDriverSF) {
                    try {
                        //Поставлен в очередь на обработку - ждем
                        if (System.getProperty("ru.atc.SF.waitPeriod:Queue_status") == null)
                            System.setProperty("ru.atc.SF.waitPeriod:Queue_status", "10");
                        int waitPeriod = 10;
                        try {
                            waitPeriod = Integer.parseInt(System.getProperty("ru.atc.SF.waitPeriod:Queue_status"));
                        } catch (NumberFormatException e) {
                            log.error("wrong property format ru.atc.SF.waitPeriod:Queue_status", e);
                            System.setProperty("ru.atc.SF.waitPeriod:Queue_status", "10");
                        }
                        for (int i = 0; i < 5; i++) {
                            if (status.equals("Поставлен в очередь на обработку") && portal instanceof PortalDriverSF) {
                                log.debug("status is 'Поставлен в очередь на обработку'... waiting...");
                                Thread.sleep(waitPeriod * 200);
                                status = ((PortalDriverSF) portal).refreshPageAndGetStatus();
                            } else break;
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                log.debug("Application info: " + result.getApplication().toString());
                if (!PortalDriver.allowedStatuses.contains(status)) {
                    setResult(new UnsendException(portal, "Status: " + status), Status.ERROR_IN_SENDING);
                } else if (status.equals("Поставлено в очередь на обработку")) {
                    setResult(null, Status.IN_QUEUE);
                    result.setErrorMessage("Статус \"Поставлено в очередь на обработку\" спустя таймвут 3 секунды");
                } else {
                    result.setUrl(portal.getCurrentUrl());
                    setResult(null, Status.SENT);
                }
            } catch (Exception e) {
                setResult(e, Status.ERROR_IN_SENDING);
            }
        }
        log.debug("Finished! " + result);
        log.info(result.getServiceId() + " : " + result.getOrderId() + " status:" + result.getStatus());
        return result;
    }


    private void customTest(Result result, String className) {
        try {
            Class<?> customTestClass = Class.forName(className);
            Constructor<?>[] constructors = customTestClass.getConstructors();
            ICustomTest instance = null;
            for (int i = 0; i < constructors.length; i++) {
                Class<?>[] params = constructors[i].getParameterTypes();
                if (params.length == 0) {
                    instance = (ICustomTest) constructors[i].newInstance();
                    break;
                }
                if (params.length == 1) {
                    if (IDriver.class.isAssignableFrom(params[0]))
                        instance = (ICustomTest) constructors[i].newInstance(this.portal);
                }
            }
            if (instance == null)
                throw new RuntimeException("Couldn't find appropriate constructor: ");
            instance.test(result);
            try {
                RunnerV2.getInstance().freeDrivers.add(portal);
            } catch (Exception e1) {
                log.error("Impossible error!", e1);
            }
        } catch (Exception e) {
            setResult(e, Status.UNKNOWN_ERROR);
            log.error("Error while invoking custom test: " + e.getMessage(), e);
        }
    }
}

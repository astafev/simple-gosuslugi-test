package ru.atc.gosuslugi.minitest;

import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.recording.RecordStarter;
import ru.atc.gosuslugi.minitest.run.Starter;
import ru.atc.gosuslugi.minitest.web.sir.cancelmodule.SirCancelStarter;

import java.lang.reflect.Method;

/**
 * Date: 30.10.13
 * Author: astafev
 */
public enum StartAppType {

    sir(SirCancelStarter.class), pgu(Starter.class), record(RecordStarter.class);

    public Method mainMethod;


    private StartAppType(Class mainClass) {
        try {
            mainMethod = mainClass.getMethod("main", String[].class);
        } catch (NoSuchMethodException e) {
            mainMethod = null;
            LoggerFactory.getLogger(StartAppType.class).error("Impossible error", e);
        }
    }
}

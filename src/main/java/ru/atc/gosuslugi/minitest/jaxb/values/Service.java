
package ru.atc.gosuslugi.minitest.jaxb.values;

import java.io.StringWriter;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.run.ICustomTest;

import java.io.Writer;

/**
 * <p>Java class for Service complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Service">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="after" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="SIR_CANCEL"/>
 *               &lt;enumeration value="SIR_CHECK"/>
 *               &lt;enumeration value="NOTHING"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="urls">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="userSelectedRegion" type="{}RegionIerarchy" minOccurs="0"/>
 *           &lt;element name="userSelectedRegions">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="userSelectedRegion" type="{}RegionIerarchy" maxOccurs="unbounded"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="type" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="find"/>
 *               &lt;enumeration value="send"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="customTest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customParam" type="{}Param" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="values" type="{}valuesType"/>
 *         &lt;element name="sirConfig" type="{}sirConfig" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="sf" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Service", propOrder = {
        "serviceid",
        "title",
        "description",
        "after",
        "url",
        "urls",
        "userSelectedRegion",
        "userSelectedRegions",
        "type",
        "customTest",
        "customParam",
        "serviceValues",
        "sirConfig"
})
public class Service {
    public static Logger log = LoggerFactory.getLogger("ru.atc.gosuslugi.minitest.jaxb.values");

    @XmlElement(required = true)
    protected String serviceid;
    protected String title;
    protected String description;
    protected String after;
    protected String url;
    protected Service.Urls urls;
    protected RegionIerarchy userSelectedRegion;
    protected Service.UserSelectedRegions userSelectedRegions;
    @XmlElement(defaultValue = "send")
    protected String type;
    protected String customTest;
    protected List<Param> customParam;
    @XmlElement(name = "values", required = true)
    protected ValuesType serviceValues;
    protected SirConfig sirConfig;
    @XmlAttribute(name = "sf")
    protected Boolean sf;

    /**
     * Gets the value of the serviceid property.
     *
     * @return possible object is
     *         {@link String }
     */
    public String getServiceid() {
        return serviceid;
    }

    /**
     * Sets the value of the serviceid property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setServiceid(String value) {
        this.serviceid = value;
    }

    /**
     * Gets the value of the title property.
     *
     * @return possible object is
     *         {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is
     *         {@link String }
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the after property.
     *
     * @return possible object is
     *         {@link String }
     */
    public String getAfter() {
        return after;
    }

    /**
     * Sets the value of the after property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAfter(String value) {
        this.after = value;
    }

    /**
     * Gets the value of the url property.
     *
     * @return possible object is
     *         {@link String }
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the urls property.
     *
     * @return possible object is
     *         {@link Service.Urls }
     */
    public Service.Urls getUrls() {
        return urls;
    }

    /**
     * Sets the value of the urls property.
     *
     * @param value allowed object is
     *              {@link Service.Urls }
     */
    public void setUrls(Service.Urls value) {
        this.urls = value;
    }

    /**
     * Gets the value of the userSelectedRegion property.
     *
     * @return possible object is
     *         {@link RegionIerarchy }
     */
    public RegionIerarchy getUserSelectedRegion() {
        return userSelectedRegion;
    }

    /**
     * Sets the value of the userSelectedRegion property.
     *
     * @param value allowed object is
     *              {@link RegionIerarchy }
     */
    public void setUserSelectedRegion(RegionIerarchy value) {
        this.userSelectedRegion = value;
    }

    /**
     * Gets the value of the userSelectedRegions property.
     *
     * @return possible object is
     *         {@link Service.UserSelectedRegions }
     */
    public Service.UserSelectedRegions getUserSelectedRegions() {
        return userSelectedRegions;
    }

    /**
     * Sets the value of the userSelectedRegions property.
     *
     * @param value allowed object is
     *              {@link Service.UserSelectedRegions }
     */
    public void setUserSelectedRegions(Service.UserSelectedRegions value) {
        this.userSelectedRegions = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     *         {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the serviceValues property.
     *
     * @return possible object is
     *         {@link ValuesType }
     */
    public ValuesType getServiceValues() {
        return serviceValues;
    }

    /**
     * Sets the value of the serviceValues property.
     *
     * @param value allowed object is
     *              {@link ValuesType }
     */
    public void setServiceValues(ValuesType value) {
        this.serviceValues = value;
    }

    /**
     * Gets the value of the sirConfig property.
     *
     * @return possible object is
     *         {@link SirConfig }
     */
    public SirConfig getSirConfig() {
        return sirConfig;
    }

    /**
     * Sets the value of the sirConfig property.
     *
     * @param value allowed object is
     *              {@link SirConfig }
     */
    public void setSirConfig(SirConfig value) {
        this.sirConfig = value;
    }

    /**
     * Gets the value of the sf property.
     *
     * @return possible object is
     *         {@link Boolean }
     */
    public boolean isSf() {
        if (sf == null) {
            return false;
        } else {
            return sf;
        }
    }

    /**
     * Sets the value of the sf property.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setSf(Boolean value) {
        this.sf = value;
    }

    public void parseUrl(String url) throws MalformedURLException {
        URL url1 = new URL(url);
        String[] path = url1.getPath().split("/");
//        boolean sf = this.sf;
        String id;
        if (!path[1].equals("pgu")) {
            if (path[1].equals("service")) {
                sf = true;
            } else {
                sf = false;
            }
            this.setSf(sf);
        }

        if (getServiceid() == null || getServiceid().isEmpty()) {
            if (path[1].equals("pgu")) {
                int index1 = path[3].indexOf("_");
                int index2 = path[3].indexOf(".");
                if (index1 == -1 || index2 == -1) {
                    throw new MalformedURLException("Unknown url type! " + url + "\n problem part: " + path[3]);
                }
                id = path[3].substring(index1+1, index2);
            } else if (path[2].equals("service") && path[3].startsWith("s") && !sf) {
                id = path[3].substring(1);
            } else if (path[1].equals("service") && sf)
                id=path[2];
            else id = "unknown";

            this.setServiceid(id);
        }

    }

    public static boolean detectSf(String url) throws MalformedURLException {
        URL url1 = new URL(url);
        String[] path = url1.getPath().split("/");
        if (path[1].equals("service")) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     * <p/>
     * <p>The following schema fragment specifies the expected content contained within this class.
     * <p/>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "url"
    })
    public static class Urls {

        @XmlElement(required = true)
        protected List<String> url;

        /**
         * Gets the value of the url property.
         * <p/>
         * <p/>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the url property.
         * <p/>
         * <p/>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUrl().add(newItem);
         * </pre>
         * <p/>
         * <p/>
         * <p/>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         */
        public List<String> getUrl() {
            if (url == null) {
                url = new ArrayList<String>();
            }
            return this.url;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * <p/>
     * <p>The following schema fragment specifies the expected content contained within this class.
     * <p/>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="userSelectedRegion" type="{}RegionIerarchy" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "userSelectedRegion"
    })
    public static class UserSelectedRegions {

        @XmlElement(required = true)
        protected List<RegionIerarchy> userSelectedRegion;

        /**
         * Gets the value of the userSelectedRegion property.
         * <p/>
         * <p/>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the userSelectedRegion property.
         * <p/>
         * <p/>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUserSelectedRegion().add(newItem);
         * </pre>
         * <p/>
         * <p/>
         * <p/>
         * Objects of the following type(s) are allowed in the list
         * {@link RegionIerarchy }
         */
        public List<RegionIerarchy> getUserSelectedRegion() {
            if (userSelectedRegion == null) {
                userSelectedRegion = new ArrayList<RegionIerarchy>();
            }
            return this.userSelectedRegion;
        }

    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id: " + getServiceid());
        sb.append(" url: " + getUrl());
        sb.append("\nregion: " + getUserSelectedRegion());
        sb.append("\nvalues: " + getServiceValues());
        return sb.toString();
    }

    public String toXML() throws JAXBException {
        StringWriter sw = new StringWriter();
        writeXML(sw);
        return sw.toString();
    }

    public void writeXML(Writer writer) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext
                .newInstance("ru.atc.gosuslugi.minitest.jaxb.values");
//        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ObjectFactory objectFactory = new ObjectFactory();
        Marshaller marsh = jaxbContext.createMarshaller();
        marsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        Services services = new Services();
        services.getServices().add(this);
        marsh.marshal(objectFactory.createServices(services), writer);
    }

    public Services splitByURL() {
        Services services = new Services();
        if (this.getUrls() != null && this.getUrls().getUrl() != null && this.getUrls().getUrl().size() > 0) {
            for (int i = 0; i < this.getUrls().getUrl().size(); i++) {
                String url = this.getUrls().getUrl().get(i);
                try {
                    Service newService = copyOfWithoutURL();
                    newService.setUrl(url);
                    newService.setServiceid(newService.getServiceid() + '.' + i);
                    services.getServices().add(newService);
                } catch (IllegalAccessException e) {
                    log.error("impossible error. You can't see this message", e);
                }

            }
        } else services.getServices().add(this);
        return services;
    }

    public Services splitByUserSelectedRegion() {
        Services services = new Services();
        if (this.getUserSelectedRegions() != null && this.getUserSelectedRegions().getUserSelectedRegion() != null && this.getUserSelectedRegions().getUserSelectedRegion().size() > 0) {
            for (int i = 0; i < this.getUserSelectedRegions().getUserSelectedRegion().size(); i++) {
                RegionIerarchy usr = this.getUserSelectedRegions().getUserSelectedRegion().get(i);
                try {
                    Service newService = copyOfWithoutField("userSelectedRegions");
                    newService.setUserSelectedRegion(usr);
                    services.getServices().add(newService);
                } catch (IllegalAccessException e) {
                    log.error("impossible error. You can't see this message", e);
                }
            }
        } else services.getServices().add(this);
        return services;
    }

    public Service copyOfWithoutURL() throws IllegalAccessException {
        return copyOfWithoutField("url", "urls");
    }

    public Service copyOfWithoutField(String... fieldNames) throws IllegalAccessException {
        Service service = new Service();
        for (Field field : Service.class.getDeclaredFields()) {
            for (String fieldName : fieldNames)
                if (!field.getName().equals(fieldName))
                    field.set(service, field.get(this));
        }
        return service;
    }

    /**
     * @return null если все ОК, либо ошибку если не ок
     */
    public String checkCorrectness() {
        if (getUserSelectedRegions() != null && getUrls() != null)
            return "Надо выбирать либо несколько регионов, либо несколько урлов";
        if(getCustomTest()!=null) {
            try {

                if (
                        !( ICustomTest.class.isAssignableFrom(Class.forName(getCustomTest())) )
                        )
                    return "Класс " + getCustomTest() + " должен реализовывать интерфейс ICustomTest";
            } catch (Exception e) {
                return "Не могу найти класс " + getCustomTest();
            }
        }

        return null;
    }

    public String getCustomTest() {
        return customTest;
    }

    public void setCustomTest(String customTest) {
        this.customTest = customTest;
    }

    public String getCustomParamValue(String key) {
        for(Param param:customParam) {
            if(param.getName().equals(key)) {
                return param.getValue();
            }
        }
        return null;
    }
    public List<ru.atc.gosuslugi.minitest.jaxb.values.Param> getCustomParam() {
        return customParam;
    }

    public void setCustomParam(List<ru.atc.gosuslugi.minitest.jaxb.values.Param> customParam) {
        this.customParam = customParam;
    }
}

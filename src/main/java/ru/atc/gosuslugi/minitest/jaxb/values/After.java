package ru.atc.gosuslugi.minitest.jaxb.values;

/**
 * Date: 27.09.13
 * Author: astafev
 */
public enum After {
    /**Отменяет через интерфейс пользователя*/
    SIR_CANCEL,
    /**Отменяет через JS TODO*/
    SIR_CANCEL_JS,
    NOTHING,
    /**Проверяет наличие процесса TODO*/
    SIR_CHECK
}

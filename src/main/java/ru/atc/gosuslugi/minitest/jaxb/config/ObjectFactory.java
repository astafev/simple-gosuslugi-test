
package ru.atc.gosuslugi.minitest.jaxb.config;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * jaxb in the ru.atc.gosuslugi.minitest.jaxb.config package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Config_QNAME = new QName("", "config");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.atc.gosuslugi.minitest.jaxb.config
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Config }
     * 
     */
    public Config createConfig() {
        return new Config();
    }

    /**
     * Create an instance of {@link Environment }
     * 
     */
    public Environment createEnvironment() {
        return new Environment();
    }

    /**
     * Create an instance of {@link ValueFile }
     * 
     */
    public ValueFile createValueFile() {
        return new ValueFile();
    }

    /**
     * Create an instance of {@link ReportingConfig }
     * 
     */
    public ReportingConfig createReportingConfig() {
        return new ReportingConfig();
    }

    /**
     * Create an instance of {@link Database }
     * 
     */
    public Database createDatabase() {
        return new Database();
    }

    /**
     * Create an instance of {@link SirConfig }
     * 
     */
    public SirConfig createSirConfig() {
        return new SirConfig();
    }

    /**
     * Create an instance of {@link Config.Environments }
     * 
     */
    public Config.Environments createConfigEnvironments() {
        return new Config.Environments();
    }

    /**
     * Create an instance of {@link Config.Databases }
     * 
     */
    public Config.Databases createConfigDatabases() {
        return new Config.Databases();
    }

    /**
     * Create an instance of {@link Config.ValueFiles }
     * 
     */
    public Config.ValueFiles createConfigValueFiles() {
        return new Config.ValueFiles();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Config }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "config")
    public JAXBElement<Config> createConfig(Config value) {
        return new JAXBElement<Config>(_Config_QNAME, Config.class, null, value);
    }

}

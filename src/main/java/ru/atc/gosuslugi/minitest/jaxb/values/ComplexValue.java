
package ru.atc.gosuslugi.minitest.jaxb.values;

import org.w3c.dom.Element;

import javax.xml.bind.annotation.*;
import java.util.Arrays;
import java.util.List;

import static ru.atc.gosuslugi.minitest.util.CommonUtil.isEmpty;


/**
 * <p>Java class for ComplexValue complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="ComplexValue">
 *   &lt;complexContent>
 *     &lt;extension base="{}Value">
 *       &lt;sequence>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="district" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="house" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="building1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="building2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apartment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="post_index" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplexValue", propOrder = {
        "values",
        "addressString",
        "region",
        "district",
        "city",
        "cityArea",
        "town",
        "street",
        "additionalArea",
        "additionalStreet",
        "house",
        "building1",
        "building2",
        "apartment",
        "post_index",
        "any"
})
public class ComplexValue
        extends Value {
    public static final List<String> fiasOrder = Arrays.asList(
            "district",
            "city",
            "cityArea",
            "town",
            "street",
            "additionalArea",
            "additionalStreet",
            "house",
            "building1",
            "building2",
            "apartment",
            "post_index"
    );

    protected String addressString;

    @XmlElement(name = "value")
    protected List<String> values;
    protected String region;

    protected String district;
    protected String city;
    protected String town;
    protected String street;
    protected String house;
    protected String building1;
    protected String building2;
    protected String apartment;
    protected String additionalArea;
    protected String additionalStreet;
    protected String post_index;
    protected String cityArea;
    @XmlAnyElement
    public List<Element> any;


    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> value) {
        this.values = value;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getBuilding1() {
        return building1;
    }

    public void setBuilding1(String building1) {
        this.building1 = building1;
    }

    public String getBuilding2() {
        return building2;
    }

    public void setBuilding2(String building2) {
        this.building2 = building2;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getPost_index() {
        return post_index;
    }

    public void setPost_index(String post_index) {
        this.post_index = post_index;
    }

    public List<Element> getAny() {
        return any;
    }

    public void setAny(List<Element> any) {
        this.any = any;
    }

    public String getAddressString() {
        return addressString;
    }

    public void setAddressString(String addressString) {
        this.addressString = addressString;
    }

    public String getAdditionalArea() {
        return additionalArea;
    }

    public void setAdditionalArea(String additionalArea) {
        this.additionalArea = additionalArea;
    }

    public String getAdditionalStreet() {
        return additionalStreet;
    }

    public void setAdditionalStreet(String additionalStreet) {
        this.additionalStreet = additionalStreet;
    }

    public String getCityArea() {
        return cityArea;
    }

    public void setCityArea(String cityArea) {
        this.cityArea = cityArea;
    }

    @Override
    public String toString() {
        return "ComplexValue{" +
                "addressString='" + addressString + '\'' +
                ", values=" + values +
                ", region='" + region + '\'' +
                ", district='" + district + '\'' +
                ", city='" + city + '\'' +
                ", town='" + town + '\'' +
                ", street='" + street + '\'' +
                ", house='" + house + '\'' +
                ", building1='" + building1 + '\'' +
                ", building2='" + building2 + '\'' +
                ", apartment='" + apartment + '\'' +
                ", additionalArea='" + additionalArea + '\'' +
                ", additionalStreet='" + additionalStreet + '\'' +
                ", post_index='" + post_index + '\'' +
                ", cityArea='" + cityArea + '\'' +
                ", any=" + any +
                '}';
    }

    public void generateAddressString() {
        StringBuilder addressString = new StringBuilder();
        if (isEmpty(getRegion())) {
            throw new IllegalStateException("Impossible to generate address string. Region is empty!");
        } else {
            addressString.append(getRegion()).append(", ");
        }
        if (!isEmpty(getDistrict())) {
            addressString.append(getDistrict()).append(", ");
        }
        if (!isEmpty(getCity())) {
            addressString.append(getCity()).append(", ");
        }
        if (!isEmpty(getTown())) {
            addressString.append(getTown()).append(", ");
        }
        if (!isEmpty(getStreet())) {
            addressString.append(getStreet());
        }
        //С домом почему-то не ищет
        this.setAddressString(addressString.toString());
    }
}

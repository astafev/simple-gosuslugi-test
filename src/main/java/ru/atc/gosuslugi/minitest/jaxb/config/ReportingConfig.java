
package ru.atc.gosuslugi.minitest.jaxb.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reportingConfig complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reportingConfig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mail.smtp.host" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mail.user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mail.password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mail.to" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mail.from" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reportingConfig", propOrder = {
    "mailSmtpHost",
    "mailUser",
    "mailPassword",
    "mailTo",
    "mailFrom"
})
public class ReportingConfig {

    @XmlElement(name = "mail.smtp.host", required = true)
    protected String mailSmtpHost;
    @XmlElement(name = "mail.user")
    protected String mailUser;
    @XmlElement(name = "mail.password")
    protected String mailPassword;
    @XmlElement(name = "mail.to", required = true)
    protected String mailTo;
    @XmlElement(name = "mail.from", required = true)
    protected String mailFrom;

    /**
     * Gets the value of the mailSmtpHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailSmtpHost() {
        return mailSmtpHost;
    }

    /**
     * Sets the value of the mailSmtpHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailSmtpHost(String value) {
        this.mailSmtpHost = value;
    }

    /**
     * Gets the value of the mailUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailUser() {
        return mailUser;
    }

    /**
     * Sets the value of the mailUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailUser(String value) {
        this.mailUser = value;
    }

    /**
     * Gets the value of the mailPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailPassword() {
        return mailPassword;
    }

    /**
     * Sets the value of the mailPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailPassword(String value) {
        this.mailPassword = value;
    }

    /**
     * Gets the value of the mailTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailTo() {
        return mailTo;
    }

    /**
     * Sets the value of the mailTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailTo(String value) {
        this.mailTo = value;
    }

    /**
     * Gets the value of the mailFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailFrom() {
        return mailFrom;
    }

    /**
     * Sets the value of the mailFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailFrom(String value) {
        this.mailFrom = value;
    }

}

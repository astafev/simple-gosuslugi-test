
package ru.atc.gosuslugi.minitest.jaxb.values;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Services complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Services">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="service" type="{}Service" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Services", propOrder = {
    "services"
})
public class Services {

    @XmlElement(name = "service")
    protected List<Service> services;

    /**
     * Gets the value of the services property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the services property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServices().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Service }
     * 
     * 
     */
    public List<Service> getServices() {
        if (services == null) {
            services = new ArrayList<Service>();
        }
        return this.services;
    }

    @Override
    public String toString() {
        return getServices().toString();
    }

    public Services splitAllByUrl() {
        return splitAllByUrl(this);
    }
    public static Services splitAllByUrl(Services services) {
        Services result = new Services();
        for(Service service:services.getServices()) {
            if(service.getUrls()!=null && service.getUrls().getUrl().size()>0) {
                Services ss = service.splitByURL();
                for(Service s:ss.getServices()) {
                    result.getServices().add(s);
                }
            } else result.getServices().add(service);
        }
        return result;
    }
    public Services splitAllByUserSelectedRegion() {
        return Services.splitAllByUserSelectedRegion(this);
    }

    public static Services splitAllByUserSelectedRegion(Services services) {
        Services result = new Services();
        for(Service service:services.getServices()) {
            if(service.getUserSelectedRegions()!=null && service.getUserSelectedRegions().getUserSelectedRegion().size()>0) {
                Services ss = service.splitByUserSelectedRegion();
                for(Service s:ss.getServices()) {
                    result.getServices().add(s);
                }
            } else result.getServices().add(service);
        }
        return result;
    }
}

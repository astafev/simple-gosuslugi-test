
package ru.atc.gosuslugi.minitest.jaxb.values;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for Value complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Value">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="type" default="text">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="text"/>
 *             &lt;enumeration value="radio"/>
 *             &lt;enumeration value="dic"/>
 *             &lt;enumeration value="checkbox"/>
 *             &lt;enumeration value="file"/>
 *             &lt;enumeration value="button"/>
 *             &lt;enumeration value="checkboxGroup"/>
 *             &lt;enumeration value="checkboxLookup"/>
 *             &lt;enumeration value="pause"/>
 *             &lt;enumeration value="date"/>
 *             &lt;enumeration value="fieldDropDown"/>
 *             &lt;enumeration value="kladr"/>
 *             &lt;enumeration value="fias"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="dicVal" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="toUseMagic" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Value", propOrder = {
    "content"
})
@XmlSeeAlso({
    ComplexValue.class
})
public class Value {

    @XmlValue
    protected String content;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "dicVal")
    protected String dicVal;
    @XmlAttribute(name = "toUseMagic")
    protected Boolean toUseMagic;
    @XmlAttribute(name = "description")
    protected String description;

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        if (type == null) {
            return "text";
        } else {
            return type;
        }
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the dicVal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDicVal() {
        return dicVal;
    }

    /**
     * Sets the value of the dicVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDicVal(String value) {
        this.dicVal = value;
    }

    /**
     * Gets the value of the toUseMagic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isToUseMagic() {
        if (toUseMagic == null) {
            return false;
        } else {
            return toUseMagic;
        }
    }

    /**
     * Sets the value of the toUseMagic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setToUseMagic(Boolean value) {
        this.toUseMagic = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    public String getValue() {
        return content;
    }

    public void setValue(String value) {
        this.content = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: ").append(getName());
        sb.append(" value: ").append(getValue());
        sb.append(" type: ").append(getType());
        if(isToUseMagic())
            sb.append(" toUseMagic: true");
        return sb.toString();
    }
}

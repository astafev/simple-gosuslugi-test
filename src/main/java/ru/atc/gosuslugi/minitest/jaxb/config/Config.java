
package ru.atc.gosuslugi.minitest.jaxb.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for Config complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Config">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="environments" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="environment" type="{}Environment" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SIR" type="{}SirConfig" minOccurs="0"/>
 *         &lt;element name="numberOfThreads" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxExclusive value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="implicitWaitPeriod" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="reportFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="databases" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="database" type="{}Database" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="valueFiles" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="valueFile" type="{}ValueFile" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="allowedStatuses" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loadingFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="screenshotsPath" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mailReporting" type="{}reportingConfig" minOccurs="0"/>
 *         &lt;element name="webdriver" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="firefox"/>
 *               &lt;enumeration value="chrome"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="webdriver.chrome.web" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firefoxPath" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firefoxProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Config", propOrder = {
    "environments",
    "sir",
    "numberOfThreads",
    "implicitWaitPeriod",
    "reportFile",
    "beforeAfterActionTimeout",
    "databases",
    "valueFiles",
    "allowedStatuses",
    "sFinQueueStatusWaitPeriod",
    "loadingFile",
    "screenshotsPath",
    "mailReporting",
    "webdriver",
    "webdriverChromeDriver",
    "firefoxPath",
    "firefoxProfile"
})
public class Config {

    protected Environments environments;
    @XmlElement(name = "SIR")
    protected SirConfig sir;
    protected Integer numberOfThreads;
    @XmlElement(defaultValue = "5")
    protected Integer implicitWaitPeriod;
    protected String reportFile;
    protected Databases databases;
    protected ValueFiles valueFiles;
    @XmlElement(defaultValue = "1")
    protected String allowedStatuses;

    @XmlElement(name = "SF.inQueueStatus.waitPeriod", defaultValue = "10")
    protected Integer sFinQueueStatusWaitPeriod;

    protected String loadingFile;
    @XmlElement(defaultValue = "C:\\tmp")
    protected String screenshotsPath;
    protected ReportingConfig mailReporting;
    protected String webdriver;
    @XmlElement(name = "webdriver.chrome.driver")
    protected String webdriverChromeDriver;
    protected String firefoxPath;
    protected String firefoxProfile;

    @XmlElement(defaultValue = "0")
    private Integer beforeAfterActionTimeout;

    public Integer getBeforeAfterActionTimeout() {
        return beforeAfterActionTimeout;
    }

    public void setBeforeAfterActionTimeout(Integer beforeAfterActionTimeout) {
        this.beforeAfterActionTimeout = beforeAfterActionTimeout;
    }

    public Integer getsFinQueueStatusWaitPeriod() {
        return sFinQueueStatusWaitPeriod;
    }

    public void setsFinQueueStatusWaitPeriod(Integer sFinQueueStatusWaitPeriod) {
        this.sFinQueueStatusWaitPeriod = sFinQueueStatusWaitPeriod;
    }

    /**
     * Gets the value of the environments property.
     * 
     * @return
     *     possible object is
     *     {@link ru.atc.gosuslugi.minitest.jaxb.config.Config.Environments }
     *     
     */
    public Environments getEnvironments() {
        return environments;
    }

    /**
     * Sets the value of the environments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ru.atc.gosuslugi.minitest.jaxb.config.Config.Environments }
     *     
     */
    public void setEnvironments(Environments value) {
        this.environments = value;
    }

    /**
     * Gets the value of the sir property.
     * 
     * @return
     *     possible object is
     *     {@link SirConfig }
     *     
     */
    public SirConfig getSIR() {
        return sir;
    }

    /**
     * Sets the value of the sir property.
     * 
     * @param value
     *     allowed object is
     *     {@link SirConfig }
     *     
     */
    public void setSIR(SirConfig value) {
        this.sir = value;
    }

    /**
     * Gets the value of the numberOfThreads property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfThreads() {
        return numberOfThreads;
    }

    /**
     * Sets the value of the numberOfThreads property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfThreads(Integer value) {
        this.numberOfThreads = value;
    }

    /**
     * Gets the value of the implicitWaitPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getImplicitWaitPeriod() {
        return implicitWaitPeriod;
    }

    /**
     * Sets the value of the implicitWaitPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setImplicitWaitPeriod(Integer value) {
        this.implicitWaitPeriod = value;
    }

    /**
     * Gets the value of the reportFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportFile() {
        return reportFile;
    }

    /**
     * Sets the value of the reportFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportFile(String value) {
        this.reportFile = value;
    }

    /**
     * Gets the value of the databases property.
     * 
     * @return
     *     possible object is
     *     {@link ru.atc.gosuslugi.minitest.jaxb.config.Config.Databases }
     *     
     */
    public Databases getDatabases() {
        return databases;
    }

    /**
     * Sets the value of the databases property.
     * 
     * @param value
     *     allowed object is
     *     {@link ru.atc.gosuslugi.minitest.jaxb.config.Config.Databases }
     *     
     */
    public void setDatabases(Databases value) {
        this.databases = value;
    }

    /**
     * Gets the value of the valueFiles property.
     * 
     * @return
     *     possible object is
     *     {@link ru.atc.gosuslugi.minitest.jaxb.config.Config.ValueFiles }
     *     
     */
    public ValueFiles getValueFiles() {
        return valueFiles;
    }

    /**
     * Sets the value of the valueFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link ru.atc.gosuslugi.minitest.jaxb.config.Config.ValueFiles }
     *     
     */
    public void setValueFiles(ValueFiles value) {
        this.valueFiles = value;
    }

    /**
     * Gets the value of the allowedStatuses property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowedStatuses() {
        return allowedStatuses;
    }

    /**
     * Sets the value of the allowedStatuses property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowedStatuses(String value) {
        this.allowedStatuses = value;
    }

    /**
     * Gets the value of the loadingFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadingFile() {
        return loadingFile;
    }

    /**
     * Sets the value of the loadingFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadingFile(String value) {
        this.loadingFile = value;
    }

    /**
     * Gets the value of the screenshotsPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScreenshotsPath() {
        return screenshotsPath;
    }

    /**
     * Sets the value of the screenshotsPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScreenshotsPath(String value) {
        this.screenshotsPath = value;
    }

    /**
     * Gets the value of the mailReporting property.
     * 
     * @return
     *     possible object is
     *     {@link ReportingConfig }
     *     
     */
    public ReportingConfig getMailReporting() {
        return mailReporting;
    }

    /**
     * Sets the value of the mailReporting property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingConfig }
     *     
     */
    public void setMailReporting(ReportingConfig value) {
        this.mailReporting = value;
    }

    /**
     * Gets the value of the webdriver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebdriver() {
        return webdriver;
    }

    /**
     * Sets the value of the webdriver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebdriver(String value) {
        this.webdriver = value;
    }

    /**
     * Gets the value of the webdriverChromeDriver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebdriverChromeDriver() {
        return webdriverChromeDriver;
    }

    /**
     * Sets the value of the webdriverChromeDriver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebdriverChromeDriver(String value) {
        this.webdriverChromeDriver = value;
    }

    /**
     * Gets the value of the firefoxPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirefoxPath() {
        return firefoxPath;
    }

    /**
     * Sets the value of the firefoxPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirefoxPath(String value) {
        this.firefoxPath = value;
    }

    /**
     * Gets the value of the firefoxProfile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirefoxProfile() {
        return firefoxProfile;
    }

    /**
     * Sets the value of the firefoxProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirefoxProfile(String value) {
        this.firefoxProfile = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="database" type="{}Database" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "database"
    })
    public static class Databases {

        @XmlElement(required = true)
        protected List<Database> database;

        /**
         * Gets the value of the database property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the database property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDatabase().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Database }
         * 
         * 
         */
        public List<Database> getDatabase() {
            if (database == null) {
                database = new ArrayList<Database>();
            }
            return this.database;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="environment" type="{}Environment" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "environment"
    })
    public static class Environments {

        protected List<Environment> environment;

        /**
         * Gets the value of the environment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the environment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEnvironment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Environment }
         * 
         * 
         */
        public List<Environment> getEnvironment() {
            if (environment == null) {
                environment = new ArrayList<Environment>();
            }
            return this.environment;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="valueFile" type="{}ValueFile" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "valueFile"
    })
    public static class ValueFiles {

        @XmlElement(required = true)
        protected List<ValueFile> valueFile;

        /**
         * Gets the value of the valueFile property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the valueFile property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getValueFile().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ValueFile }
         * 
         * 
         */
        public List<ValueFile> getValueFile() {
            if (valueFile == null) {
                valueFile = new ArrayList<ValueFile>();
            }
            return this.valueFile;
        }

    }

}


package ru.atc.gosuslugi.minitest.jaxb.values;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * jaxb in the ru.atc.gosuslugi.minitest.jaxb.values package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Services_QNAME = new QName("", "services");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.atc.gosuslugi.minitest.jaxb.values
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Service }
     *
     */
    public Service createService() {
        return new Service();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Services }
     *
     */
    public Services createServices() {
        return new Services();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.SirConfig }
     *
     */
    public ru.atc.gosuslugi.minitest.jaxb.values.SirConfig createSirConfig() {
        return new SirConfig();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Filter }
     *
     */
    public Filter createFilter() {
        return new Filter();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Step }
     *
     */
    public Step createStep() {
        return new Step();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Value }
     *
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.ValuesType }
     *
     */
    public ValuesType createValuesType() {
        return new ValuesType();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Param }
     *
     */
    public Param createParam() {
        return new Param();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.DateFilter }
     *
     */
    public DateFilter createDateFilter() {
        return new DateFilter();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.ComplexValue }
     *
     */
    public ComplexValue createComplexValue() {
        return new ComplexValue();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.RegionIerarchy }
     *
     */
    public RegionIerarchy createRegionIerarchy() {
        return new RegionIerarchy();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Service.Urls }
     *
     */
    public Service.Urls createServiceUrls() {
        return new Service.Urls();
    }

    /**
     * Create an instance of {@link ru.atc.gosuslugi.minitest.jaxb.values.Service.UserSelectedRegions }
     *
     */
    public Service.UserSelectedRegions createServiceUserSelectedRegions() {
        return new Service.UserSelectedRegions();
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.atc.gosuslugi.minitest.jaxb.values.Services }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "services")
    public JAXBElement<Services> createServices(Services value) {
        return new JAXBElement<Services>(_Services_QNAME, Services.class, null, value);
    }

}

package ru.atc.gosuslugi.minitest.web.sir.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.web.sir.*;
import ru.atc.gosuslugi.minitest.web.sir.Row;

import java.util.List;

/**
 * Date: 14.10.13
 * Author: astafev
 */
public class ProcessCopiesPage extends SirPageObject {
    static {
        pageId = "view-process";
        panelId = "processPanel";
    }

    private ProcessCopiesPage(SirDriver driver1) {
        super(driver1);
    }

    public static ProcessCopiesPage goToProcessCopiesPage (SirDriver driver1) {
        return new ProcessCopiesPage(driver1);
    }

    @Override
    public Page getPage() {
        return Page.PROCESS_COPIES;
    }

    /**
     * <p>Вырубает процесс по номеру заявки с ПГУ. Если браузер на странице "Экзмепляра процессов"</p>
     * <p>На самом деле: ищет в реквизитах подстроку переданную</p>
     *
     * @param orderId номер заявки
     * @throws ru.atc.gosuslugi.minitest.web.PortalException если страница не "Экземпляры процессов" или если не нашел нужный процесс
     */
    public void stopProcess(String orderId) {
        if (getPage() != Page.PROCESS_COPIES) {
            throw new PortalException(driver, "Не на странице \"Экзмепляры процессов\", невозможно завершить процесс с orderId=" + orderId);
        }
        List<Row> rows = getDisplayedRows(0);
        for (Row row : rows) {
            if (row.getProcessMap().get("Реквизиты").contains(orderId)) {
                row.click();
                clickOnTollbarButton("Остановить процесс");
                return;
            }
        }
        throw new PortalException(driver, "Didn't found process with orderId = " + orderId + ".\n" +
                "Available: " + rows);
    }

    /**
     * Кликает на строку и гасит "остановить процесс"
     *
     * @throws PortalException если страница не "Экземпляры процессов" или если не нашел нужный процесс
     */
    public void stopProcess(Row row) {
        log.info("Stopping row " + row);
        if (getPage() != Page.PROCESS_COPIES) {
            throw new PortalException(driver, "Не на странице \"Экзмепляры процессов\", невозможно завершить процесс " + row);
        }
        row.click();
        findElement(By.xpath("//button/b[text()='Остановить процесс']")).click();
        log.info("Row " + row + " successfully stopped");
    }

    /**
     * Гасит 1-й процесс. Надо все фильтры предврительно выставить
     *
     * @throws PortalException если страница не "Экземпляры процессов" или если не нашел нужный процесс
     */
    public void stopFirstProcess() {
        long time = System.currentTimeMillis();
        log.debug("stopping first process");
        if (getPage() != Page.PROCESS_COPIES) {
            throw new PortalException(driver, "Не на странице \"Экзмепляры процессов\", невозможно завершить 1-й процесс ");
        }
//        List<Row> processList = getDisplayedRows(1);
        List<WebElement> processList = driver.getDisplayedList(this, By.cssSelector("div.x-grid3-row"), 1);
        if (processList.size() == 0) {
            log.error("Can't find any process within specified filters");
        } else {
            processList.get(0).click();
            findElement(By.xpath("//button/b[text()='Остановить процесс']")).click();
        }
        log.debug("stopping first process took " + (System.currentTimeMillis() - time));
    }
}

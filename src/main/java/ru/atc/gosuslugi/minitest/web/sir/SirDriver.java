package ru.atc.gosuslugi.minitest.web.sir;

import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.DateFilter;
import ru.atc.gosuslugi.minitest.jaxb.values.Filter;
import ru.atc.gosuslugi.minitest.web.sir.frames.AbstractSirFrame;
import ru.atc.gosuslugi.minitest.web.sir.frames.VariablesTable;
import ru.atc.gosuslugi.minitest.web.sir.pages.*;
import ru.atc.gosuslugi.minitest.util.ValueUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Date: 18.09.13
 * Author: astafev
 */
public class SirDriver extends Driver {
    public static final Logger log = LoggerFactory.getLogger(SirDriver.class);
    public static final SimpleDateFormat dd_MM_yy = new SimpleDateFormat("dd.MM.yy");
    public static final SimpleDateFormat SIR_DATE_FORMAT = new SimpleDateFormat("dd.MM.yy HH:mm");
    private SirPageObject sirPageObject;

    /**
     * На самом деле это не фрейм, но очень похож
     */
    private AbstractSirFrame openedFrame;

    public SirDriver(WebDriver driver) {
        super(driver);
//        this.web = web;
//        this.web = new EventFiringWebDriver(this.web);
//        ((EventFiringWebDriver) this.web).register(new WaitForEventListener());
    }

    public void start() throws InterruptedException {
        try {
            //вырубаем pop-up
            WebElement element = findElement(By.cssSelector("#popup a.close"));
            if (element.isDisplayed()) {
                element.click();
            }
        } catch (NoSuchElementException e) {
            //do nothing
            log.debug("Не увидел при входе херни со снилсом, возможно надо убрать костыль");
        }
        findElement(By.className("ui-startbutton")).click();
//        new WebDriverWait(this, 5).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ui-startmenu-item-content'][contains(text(), 'Регламенты')]")));
        findElement(By.xpath("//div[@class='ui-startmenu-item-content'][contains(text(), 'Регламенты')]")).click();
//        new WebDriverWait(this, 5).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//div[@class='ui-startmenu-item-content'][contains(text(), 'Регламенты')]")));
        Thread.sleep(2000);
        switchTo().defaultContent().switchTo().frame(0);
        AllTasksPage allTasksPage = new AllTasksPage(driver.findElement(By.id(AllTasksPage.panelId)), this);
        setCurrentPage(allTasksPage);
    }


    /**
     * Ищет в меню, всплюывающем при клике правой кнопкой мыши
     */
    private WebElement findInMenuItemText(String key) {
        List<WebElement> keysElements = getDisplayedList(By.className("x-menu-item-text"));
        Map<String, WebElement> map = new LinkedHashMap<String, WebElement>();
        for (WebElement element : keysElements) {
            map.put(element.getText(), element);
        }
        for (String s : map.keySet()) {
            if (s.equals(key)) {
                return map.get(s);
            }
        }
        throw new PortalException(this, "didn't found key '" + key + "' in filter. Available keys: " + map.keySet());
    }

    public void setDateFilter(DateFilter dateFilter) throws ParseException {
        Date startDate = null;
        Date endDate = null;
        if (dateFilter.getStart() != null)
            startDate = ValueUtils.stringToDate(dateFilter.getStart());
        if (dateFilter.getEnd() != null)
            startDate = ValueUtils.stringToDate(dateFilter.getEnd());
        setDateFilter(dateFilter.getKey(), startDate, endDate);
    }

    public void setDateFilter(String key, Date start, Date end) {
        rightClickOnLink(null);
        findInMenuItemText(key).click();

        WebElement block = new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(By.className("x-resizable-pinned")));
        List<WebElement> inputFields = block.findElements(By.cssSelector("input.x-form-field"));
        if (start != null) {
            inputFields.get(0).clear();
            inputFields.get(0).sendKeys(dd_MM_yy.format(start));
        }
        if (end != null) {
            inputFields.get(1).clear();
            inputFields.get(1).sendKeys(dd_MM_yy.format(end));
        }
        block.findElement(By.xpath("//button[text()='Применить']")).click();
    }

    private void rightClickOnLink(String partialLinkText) {
        rightClickOnLink(partialLinkText, getCurrentPage());
    }

    private void rightClickOnLink(String partialLinkText, SearchContext context) {
        if (partialLinkText == null || partialLinkText.isEmpty()) {
            partialLinkText = "Фильтр";
        }
        WebElement filterLabel = context.findElement(By.partialLinkText(partialLinkText));
        Actions builder = new Actions(driver);
        Action rightClick = builder.contextClick(filterLabel).build();
        rightClick.perform();
    }

    public Page getPage() {
        return getCurrentPage().getPage();
    }

    public SirPageObject getCurrentPage() {
        return sirPageObject;
    }

    public void setCurrentPage(SirPageObject sirPageObject) {
        this.sirPageObject = sirPageObject;
        this.getCurrentPage().setDriver(this);
    }




    public static SirDriver createAndRun(WebDriver driver) {
        int implicitWaitPeriod = Integer.valueOf(System.getProperty("webdriver.implicitWaitPeriod", "5"));
        SirDriver sirDriver = new SirDriver(driver);
        sirDriver.manage().timeouts().implicitlyWait(implicitWaitPeriod, TimeUnit.SECONDS);
        sirDriver.get(System.getProperty("ru.atc.SIR_URL"));
        sirDriver.setCurrentPage(new StartPage());
        return sirDriver;
    }

    public AllTasksPage goToAllTasks() {
//        new WebDriverWait(this, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("view-task"))).click();
//        WebElement page = new WebDriverWait(this, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("processPanel")));
        AllTasksPage allTasksPage = AllTasksPage.goToAllTasksPage(this);
        this.setCurrentPage(allTasksPage);
        return allTasksPage;
    }

    public ProcessCopiesPage goToProcessCopies() {
//        new WebDriverWait(this, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("view-process"))).click();
        WebElement element = new WebDriverWait(this, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("processPanel")));
        ProcessCopiesPage processCopiesPage = ProcessCopiesPage.goToProcessCopiesPage(this);
        this.setCurrentPage(processCopiesPage);
        return processCopiesPage;
    }

    public TemplatesPage goToTemplates() {
//        new WebDriverWait(this, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("view-process"))).click();
//        WebElement element = new WebDriverWait(this, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("processPanel")));
        TemplatesPage templatesPage = TemplatesPage.goToTemplatesPage(this);
        this.setCurrentPage(templatesPage);
        return templatesPage;
    }

    public void disableAllFilters() {
        getCurrentPage().disableAllFilters();
    }

    public void setFilter(Filter filter) throws InterruptedException {
        getCurrentPage().setFilter(filter);
    }

    public void setFilter(String key, String value) {
        getCurrentPage().setFilter(key, value);
    }

    /**
     * Возвращает ключ шаблона по имени.
     * Для этого переходит на страницу "Шаблоны процессов", ставит там фильтр и смотрит все оставшиеся
     */
    public String getTemplateKey(String templateName) {
        TemplatesPage templatesPage;
        if (getPage() != Page.PROCESS_TEMPLATES) {
            templatesPage = goToTemplates();
        } else {
            templatesPage = (TemplatesPage) getCurrentPage();
        }
        templatesPage.setFilter("Шаблон", templateName);
        List<Row> rowList = templatesPage.getDisplayedRows(0);
        switch (rowList.size()) {
            case 0:
                throw new PortalException(this, "Didn't found template with templateName=" + templateName);
            case 1:
                return rowList.get(0).getProcessMap().get("Ключ");
            default: {
                String key = rowList.get(0).getProcessMap().get("Ключ");
                ;
                for (int i = 1; i < rowList.size(); i++) {
                    String key2 = rowList.get(i).getProcessMap().get("Ключ");
                    if (!key.equals(key2)) {
                        log.warn("Found different keys on specified templateName=" + templateName + ":\n" +
                                key + "; " + key2);
                        throw new PortalException(this, "Found different keys on specified templateName=" + templateName + ":\n" +
                                key + "; " + key2);
                    }
                }
                return key;
            }
        }
    }

    public AbstractSirFrame getOpenedFrame() {
        return openedFrame;
    }

    public void setOpenedFrame(AbstractSirFrame openedFrame) {
        this.openedFrame = openedFrame;
    }

    public VariablesTable openProcessVariables(Row row) {
        if (getPage() != Page.ALL_TASKS) {
            throw new IllegalStateException("Wrong page! " + getPage());
        }
        row.click();
        getCurrentPage().clickOnTollbarButton("Переменные");
        return new VariablesTable(this);
    }



}
package ru.atc.gosuslugi.minitest.web.sir.cancelmodule;

/**
 * Date: 11.10.13
 * Author: astafev
 */
enum Status {
    SUCCESS,
    ERROR,
    WRONG_INPUT,
    SYSTEM_ERROR
}

enum RunType {
    BY_KEYS("key"),
    BY_PROCESS("genocide");

    String key;
    RunType(String key) {
        this.key = key;
    }


}

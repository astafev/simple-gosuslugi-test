package ru.atc.gosuslugi.minitest.web;

import org.openqa.selenium.remote.UnreachableBrowserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortalException  extends RuntimeException{
    public String url;
    public String screenshotFile;
    public static final Logger logger = LoggerFactory.getLogger(PortalException.class);
    public String error;
    public boolean brokenBrowser = false;


    public PortalException(IDriver driver, String string) {
        this(driver, string, null);
    }

    public PortalException(IDriver driver, String string, Throwable cause) {
        super(string, cause);
        try{
            url = driver.getCurrentUrl();
            String screenshot = String.valueOf(System.currentTimeMillis()) + ".png";
            driver.takeScreenshot(screenshot);
            screenshotFile = System.getProperty("screenshotsPath") + screenshot;
        } catch (UnreachableBrowserException e) {
            logger.warn("Unable to take screenshot! URL: " + url, e);
            error = e.getMessage();
            brokenBrowser = true;
            screenshotFile = null;
        } catch (Exception e) {
            logger.warn("Unable to take screenshot! URL: " + url, e);
            error = e.getMessage();
            screenshotFile = null;
        }
    }


}

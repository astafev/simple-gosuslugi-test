package ru.atc.gosuslugi.minitest.web.sir;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Перед тем как найти что-нибудь проверяем что блок "Пожалуйста подождите" не отображается
 */
public class WaitForEventListener extends AbstractWebDriverEventListener {
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.not(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[contains(text(), 'Пожалуйста подождите')]"))));
    }
}

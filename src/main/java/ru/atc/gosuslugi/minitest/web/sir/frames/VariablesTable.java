package ru.atc.gosuslugi.minitest.web.sir.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.portal.OneOfElementPresent;
import ru.atc.gosuslugi.minitest.web.sir.Row;
import ru.atc.gosuslugi.minitest.web.sir.SirDriver;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Date: 14.10.13
 * Author: astafev
 */
public class VariablesTable extends AbstractSirFrame {
    Map<String, Row> processes;
    public static final String VARIABLE_NAME = "Название";
    public static final String VARIABLE_TYPE = "Тип";
    public static final String VARIABLE_VALUE = "Значение";
    private static final String[] keys = new String[] {
            VARIABLE_NAME,
            VARIABLE_TYPE,
            VARIABLE_VALUE,
            ""
    };

    public static Logger log = LoggerFactory.getLogger(VariablesTable.class);
    private String caseNumber;

    public VariablesTable(SirDriver driver1) {
        super(driver1.getDisplayed(By.cssSelector(".x-window.x-resizable-pinned")));
        this.setDriver(driver1);
        new WebDriverWait(driver1, 10).until(new OneOfElementPresent(By.className("x-grid3-row-table"), this));
    }

    /**
     * @throws NullPointerException если нет такого*/
    public String getValue(String key) {
        if(processes == null) {
            init();
        }
        return processes.get(key).getProcessMap().get("Значение");
    }

    public void init() {
        long millis = System.currentTimeMillis();
        processes = new HashMap<String, Row>();
        List<WebElement> list = driver.getDisplayedList(this, By.className("x-grid3-row"));
        for(int i=0; i<list.size(); i++) {
            List<WebElement> tds = list.get(i).findElements(By.tagName("td"));
            Map<String, String> processMap = new LinkedHashMap<String, String>();
            for(int j=0; j<Math.min(tds.size(), keys.length); j++) {
                processMap.put(keys[j], tds.get(j).getText());
            }
            Row row = new Row(list.get(i), processMap);
            this.processes.put(processMap.get("Название"), row);
            if(processMap.get(VARIABLE_NAME).equalsIgnoreCase("CaseNumber")) {
                setCaseNumber(processMap.get(VARIABLE_VALUE));
            }
        }
        log.debug(System.currentTimeMillis() - millis + " took to init variables table");
    }

    public String getCaseNumber() {
        if(caseNumber == null) {
            init();
        }
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

}

package ru.atc.gosuslugi.minitest.web.sir.cancelmodule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.result.ResultUtils;
import ru.atc.gosuslugi.minitest.run.AbstractRunner;
import ru.atc.gosuslugi.minitest.run.Starter;
import ru.atc.gosuslugi.minitest.web.sir.*;
import ru.atc.gosuslugi.minitest.web.sir.js.*;
import ru.atc.gosuslugi.minitest.util.ConfigUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import ru.atc.gosuslugi.minitest.web.sir.js.Process;

/**
 * Date: 09.10.13
 * Author: astafev
 */
public class SirCancelStarter {
    public static Logger log = LoggerFactory.getLogger(SirCancelStarter.class);
    public static final LinkedHashMap<String, Result> keyStatusMap = new LinkedHashMap<String, Result>();
    public static SirDriver sirDriver;
    public static SirDriverJS sirDriverJs;
    public static String reportFile = "sir_cancel_report %date%.html";

    public static final String USAGE = "Usage: <file>\n" +
            "-t <type>\n" +
            "   types: \"" + RunType.BY_KEYS.key + "\",\"" + RunType.BY_PROCESS.key + "\"";
    public static RunType type = RunType.BY_KEYS;


    public static void main(String[] args) {
        try {
            Date startDate = new Date();


            log.info("-----------------------------------------\n                 Starting ... Date: " + startDate);
            String fileName = System.getProperty("configFile", "config.xml");
            ConfigUtils.readConfig(fileName);

            List<String> results = verifyInput(args);


            if (log.isDebugEnabled())
                log.debug("System properties: " + System.getProperties().toString());

            sirDriver = SirDriver.createAndRun(Driver.createNewWebDriver().getDriver());
            sirDriverJs = SirDriverJS.getOne(sirDriver);
            //может не надо?
            sirDriver.start();
            switch (type) {
                case BY_KEYS:
                    stopByKeys(results);
                    break;
                case BY_PROCESS:
                    stopByTemplateAndDate(results);
                    break;
                default:
                    throw new IllegalArgumentException("something wrong...");
            }
            File reports = new File("reports");
            if (!reports.exists()) {
                if (!reports.mkdir()) {
                    log.error("unable to create directory \"reports\"");
                    reports = null;
                }
            }
            File report = new File(reports, reportFile.replaceAll("%date%", Starter.DATE_FORMAT.format(new Date())));
            Writer writer = new BufferedWriter(new FileWriter(report));
            try {
                Map<String, Object> objectMap = new HashMap<String, Object>();
                objectMap.put("results", keyStatusMap);
                objectMap.put("endDate", new Date());
                objectMap.put("startDate", startDate);
                ResultUtils.writeFtlReport(writer, objectMap, System.getProperty("sircancel.reportFtl", "sirCancel.ftl"));

            } catch (Exception e) {
                log.error("Ошибка при создании ftl-отчета", e);
            } finally {
                sirDriver.quit();
                writer.close();
            }
            log.info(keyStatusMap.toString());
        } catch (Exception e) {
            log.error("Error! " + e.getLocalizedMessage(), e);
        }
    }

    private static List<String> verifyInput(String[] args) throws IOException {
        List<String> result = null;
        if (args.length == 0) {
            throw new IllegalArgumentException("Empty list of arguments!");
        } else {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-t")) {
                    type = RunType.valueOf(args[++i]);
                } else {
                    File file = new File(args[i]);
                    if (!file.exists()) {
                        throw new IllegalArgumentException("File '" + file.getAbsolutePath() + "' doesn't exists!");
                    }
                    result = Files.readAllLines(file.toPath(), Charset.defaultCharset());
                }
            }
        }
        Map<Integer, String> errors = null;
        switch (type) {
            case BY_KEYS:
                errors = verifyInputFileByKeys(result);
                break;
            case BY_PROCESS:
                errors = verifyInputFileByProcess(result);
                break;
            default:
                throw new IllegalArgumentException("something wrong...");
        }
        if (errors != null && errors.size() > 0) {
            StringWriter sw = new StringWriter();
            sw.append("ERROR IN FILE:\n");
            for (Integer integer : errors.keySet()) {
                sw.append(integer.toString()).append(" : ");
                sw.append(errors.get(integer)).append("\n");
            }
            throw new IllegalArgumentException(sw.toString());
        }
        return result;
    }

    public static final String STOP_BY_PROCESS_FILE_FORMAT = "<ключ шаблона>\n<имя процесса>\n<Дата с которой начать убивать>\n[<Дата после которой не надо убивать>]\n";
    public static DateFormat sirDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    public static DateFormat sirDateFormatMin = new SimpleDateFormat("dd.MM.yyyy");


    public static Date getDateFromString(String s) throws ParseException {
        try {
            return sirDateTimeFormat.parse(s);
        } catch (ParseException e) {
            return sirDateFormatMin.parse(s);
        }
    }

    /**
     * Формат:
     * id процесса
     * Имя процесса
     * Дата с которой надо начать убивать все
     */
    private static Map<Integer, String> verifyInputFileByProcess(List<String> strings) {
        Map<Integer, String> errorMap = null;
        if (strings.size() != 3 && strings.size() != 4) {
            errorMap = new HashMap<Integer, String>(1);
            errorMap.put(0, "Неверный формат входного файла. Правильный:\n" + STOP_BY_PROCESS_FILE_FORMAT);
            return errorMap;
        }
        try {
            getDateFromString(strings.get(2));
        } catch (ParseException e) {

            errorMap = new HashMap<Integer, String>(1);
            errorMap.put(3, "Неправильный формат даты! " + e.getMessage());
        }
        try {
            if (strings.size() > 3)
                getDateFromString(strings.get(2));
        } catch (ParseException e) {
            if (errorMap != null)
                errorMap = new HashMap<Integer, String>(1);
            errorMap.put(4, "Неправильный формат даты! " + e.getMessage());
        }
        return errorMap;
    }

    private static Map<Integer, String> verifyInputFileByKeys(List<String> strings) {
        Map<Integer, String> errorMap = null;
        for (int i = 0; i < strings.size(); i++) {
            String[] arr = strings.get(i).split(" ");
            if (arr.length > 2 || arr.length == 0 || arr[0].isEmpty()) {
                if (errorMap == null) {
                    errorMap = new LinkedHashMap<Integer, String>();
                }
                errorMap.put(new Integer(i), "Should be 1 or 2 words! " + Arrays.toString(arr));
            }
        }
        if (errorMap == null || errorMap.size() == 0) {
            return null;
        } else return errorMap;
    }

    /**
     * Удаляет все по ключу. В сир должно быть уже зайдено
     */
    public static void stopByKeys(List<String> inputs) {
        for (int i = 0; i < inputs.size(); i++) {
            Result result = new Result();
            keyStatusMap.put(inputs.get(i), result);
            String id = null;
            String[] arr = inputs.get(i).split(" ");
            if (arr.length == 2) {
                id = arr[0] + "." + arr[1];
            } else if (arr.length == 1) {
                id = arr[0];
            } else {
                result.setStatus(Status.WRONG_INPUT);
                result.setException(new IllegalArgumentException("can't create id from input " + inputs.get(i)));

                continue;
            }
            try {
                sirDriverJs.stopProcess(id);
                result.setStatus(Status.SUCCESS);
            } catch (PortalException e) {
                result.setException(e);
                result.setStatus(Status.ERROR);
            }
        }
    }

    public static void stopByTemplateAndDate(List<String> inputs) {
        try {
            String key = inputs.get(0);
            String processName = inputs.get(1);
            Date startDate = null;
            if (!inputs.get(2).isEmpty())
                startDate = getDateFromString(inputs.get(2));
            Date endDate = null;
            if (inputs.size() > 3)
                endDate = getDateFromString(inputs.get(2));
            StringBuilder script = new StringBuilder("start=0&limit=1000&showprocess=All&createDate_from=");
            if (startDate != null)
                script.append(sirDateFormatMin.format(startDate));
            script.append("&createDate_to=");
            if (endDate != null)
                script.append(sirDateFormatMin.format(endDate));
            script.append("&createDate_rb=&createDate_order=0&serviceName_from=").append(processName);
            script.append("&serviceName_to=&serviceName_rb=&serviceName_order=1");
            try {
                List<Process> processes = sirDriverJs.getObjects(script.toString(), Process.class);
                for (Process process : processes) {
                    Date createDate = sirDateTimeFormat.parse(process.getCreateDate());
                    long createDateTime = createDate.getTime();
                    if (startDate != null && createDateTime > startDate.getTime()) {
                        if (endDate != null && createDateTime < endDate.getTime()) {
                            Result result = new Result();
                            result.setProcess(process);
                            try {
                                sirDriverJs.stopProcess(process);
                                result.setStatus(Status.SUCCESS);
                            } catch (Exception e) {
                                result.setStatus(Status.ERROR);
                                result.setException(e);
                            }
                            keyStatusMap.put(process.getProcessInstanceId(), result);
                        }
                    }
                }
            } catch (Exception e) {
                throw new SirException(sirDriver, e.getMessage(), e);
            }
        } catch (ParseException e) {
            Result result = new Result();
            result.setException(e);
            result.setStatus(Status.WRONG_INPUT);
            keyStatusMap.put("", result);
        }
    }
}
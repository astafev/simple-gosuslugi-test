package ru.atc.gosuslugi.minitest.web.sir.js;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Date: 16.10.13
 * Author: astafev
 */
public class AbstractBean {

    protected AbstractBean() {

    }

    public AbstractBean(Map<String, ? extends Object> map) throws NoSuchFieldException {
        init(map);
    }

    public void init(Map<String, ? extends Object> map) throws NoSuchFieldException {
        for(String key:map.keySet()) {
            try {
                this.getClass().getDeclaredField(key).set(this, map.get(key));
            } catch (IllegalAccessException e) {
                SirDriverJS.log.error("Impossible error!", e);
            }
        }
    }

    public void init(JSONObject object)  {
        Field[] fields = this.getClass().getDeclaredFields();
        for(Field field:fields) {
            try {
                Object obj = object.get(field.getName());
                if(obj==null) {
                    //do nothing
                    continue;
                }
                if(field.getType().isAssignableFrom(obj.getClass())) {
                    field.set(this, obj);
                } else {
                    if(field.getType() == String.class) {
                        field.set(this, obj.toString());
                    }
                }

            } catch (IllegalAccessException e) {
                SirDriverJS.log.error(field.getName() + " Impossible error!", e);
            } catch (JSONException e) {
                SirDriverJS.log.warn(field.getName() + " doesn't exist in object.\n" + object.toString());
            }
        }

    }

}

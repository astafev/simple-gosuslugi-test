package ru.atc.gosuslugi.minitest.web.portal;

/**
 * Date: 28.10.13
 * Author: astafev
 */
public enum Page {
    APPLICATION_INFO,
    SERVICE_INFO,
    DRAFTS_OF_SERVICE,

    STEP,

    FORM_PREVIEW, //на сервере форм
    ;

    public int pageNum = -1;


//TODO подумать о реализции класса Page getPage() в классе Driver  ...
}
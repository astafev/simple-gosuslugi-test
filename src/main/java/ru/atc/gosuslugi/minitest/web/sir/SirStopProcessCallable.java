package ru.atc.gosuslugi.minitest.web.sir;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.DateFilter;
import ru.atc.gosuslugi.minitest.jaxb.values.Filter;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.ResultUtils;
import ru.atc.gosuslugi.minitest.result.Status;
import ru.atc.gosuslugi.minitest.web.sir.frames.VariablesTable;
import ru.atc.gosuslugi.minitest.web.sir.js.SirDriverJS;
import ru.atc.gosuslugi.minitest.web.sir.pages.AllTasksPage;
import ru.atc.gosuslugi.minitest.web.sir.pages.ProcessCopiesPage;

import java.text.ParseException;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Date: 25.09.13
 * Author: astafev
 */
public class SirStopProcessCallable implements Callable<Result> {
    final Result result;
    final SirDriver sir;
    public static final Logger log = LoggerFactory.getLogger(SirStopProcessCallable.class);

    public SirStopProcessCallable(Result result, SirDriver sir) {
        this.result = result;
        this.sir = sir;
    }

    @Override
    public Result call() {
        if (sir.getPage() == Page.JUST_IN) {
            try {
                sir.start();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                setResult(e, Status.UNKNOWN_ERROR);
                return result;
            }
        }
        Service service = result.getService();
        String sir_type = System.getProperty("ru.atc.SIR_TYPE", "PROD");
        if(sir_type.equalsIgnoreCase("PROD")) {
            return shutWithJS();
        }
        else if (sir_type.equalsIgnoreCase("TEST")) {
            return stopWithProcessCopiesPage();
        } else {
            setResult(new IllegalArgumentException("Unknown type of SIR: " + service.getType()), Status.UNKNOWN_ERROR);
            return result;
        }
    }

    private Result stopWithProcessCopiesPage() {
        ProcessCopiesPage page;
        if (sir.getPage() != Page.PROCESS_COPIES) {
            try {
                log.debug("Going to process copies page");
                page = sir.goToProcessCopies();
            } catch (Exception e) {
                log.error("", e);
                setResult(e, Status.UNKNOWN_ERROR);
                return result;
            }
        } else {
            page = (ProcessCopiesPage)sir.getCurrentPage();
        }

        try {
            Service service = result.getService();
            String orderId = result.getOrderId();
            page.disableAllFilters();
            if(service.getSirConfig()!=null) {
                List<Filter> filters = service.getSirConfig().getFilters();
                if(filters!=null && filters.size()>0) {
                    for(Filter filter:filters)
                        sir.setFilter(filter);

                }
                List<DateFilter> dateFilters = service.getSirConfig().getDateFilters();
                if(dateFilters!=null && dateFilters.size()>0) {
                    for(DateFilter filter:dateFilters)
                        sir.setDateFilter(filter);

                }
            }
            page.setFilter("Реквизиты", orderId);
            page.stopFirstProcess();
            setResult(null, Status.CANCELED_IN_SIR);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            setResult(e, Status.ERROR_CANCELING_IN_SIR);
        }
        return result;
    }

    /**
     * <p>учитывая что нам больше не надо ходить на страницу экземпляров процессов:</p>
     * <ul>
     *

     *     <li>Если нет фильтров - ставим фильтр на реквизиты - caseNumber ?и дату - со вчера?</li>
     *     <ol>
     *         <li>Ставим фильтр Реквизиты=orderId</li>
     *         <li>Если остался один - берем processId<br/>
     *             Если нет - смотрим у всех caseNumber...</li>
     *         <li>Если остался один - убиваем</li>
     *     </ol>
     *     <li>Если есть - ставим указанные фильтры</li>
     *     <ol>
     *         <li>Ставим фильтр Реквизиты=orderId</li>
     *         <li>Если остался один - берем processId<br/>
     *             Если нет - смотрим у всех caseNumber...</li>
     *         <li>Если остался один - убиваем</li>
     *     </ol>
     * </ul>
     * После того как взяли processId, если ключ не введен в параметрах, берем наимение процесса из параметров, идем в щаблоны. Там стави фильтр и берем ключ
     * */
    private Result shutWithJS() {

        if (sir.getPage() != Page.ALL_TASKS) {
            try {
                log.debug("Going to process copies page");
                sir.goToAllTasks();
            } catch (Exception e) {
                log.error("", e);
                setResult(e, Status.UNKNOWN_ERROR);
                return result;
            }

        }
        try {
            Service service = result.getService();
            String orderId = result.getOrderId();
            takeCareOfFilters(result);
            List<Row> rows = sir.getCurrentPage().getDisplayedRows(0);
            Row proc2Stop = null;
            switch (rows.size()) {
                case 0:
                    SirException sirException = new SirException(sir, "Не нашел процессов после применения фильтров!");
                    throw sirException;
                case 1:
                    proc2Stop = rows.get(0);
                    break;
                default:
                    for(int i=0; i< rows.size(); i++) {
                        VariablesTable vt = sir.openProcessVariables(rows.get(i));
                        if(vt.getCaseNumber().equals(orderId)) {
                            proc2Stop = rows.get(i);
                            vt.close();
                            break;
                        }
                        vt.close();
                    }
                    if(proc2Stop == null)
                        throw new SirException(sir, "Didn't found process with caseNumber = " + orderId + "\nrows: " + rows);
            }
            String processId = proc2Stop.getProcessMap().get(AllTasksPage.PROCESS_ID);
            String key;
            if(service.getSirConfig()!=null && service.getSirConfig().getKey()!=null && !service.getSirConfig().getKey().isEmpty()) {
                key = service.getSirConfig().getKey();
            } else
                key = sir.getTemplateKey(proc2Stop.getProcessMap().get(AllTasksPage.PROCESS_NAME));
            SirDriverJS jsDriver = SirDriverJS.getOne(sir);
            jsDriver.stopProcess(key, processId);
            setResult(null, Status.CANCELED_IN_SIR);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            setResult(e, Status.ERROR_CANCELING_IN_SIR);
        }
        return result;
    }

    /**Вырубаем старые, ставим новые*/
    private void takeCareOfFilters(Result result) throws InterruptedException, ParseException {
        Service service = result.getService();
        String orderId = result.getOrderId();
        sir.disableAllFilters();
        if(service.getSirConfig()==null ||
                (
                        (service.getSirConfig().getDateFilters()==null || service.getSirConfig().getDateFilters().isEmpty()) &&
                                (service.getSirConfig().getFilters()==null || service.getSirConfig().getFilters().isEmpty())
                )
                ) {
            sir.setFilter(AllTasksPage.REQUISITES, orderId);
        }
        else {
            List<Filter> filters = service.getSirConfig().getFilters();
            if(filters!=null && filters.size()>0) {
                for(Filter filter:filters) {
                    if(filter.getValue().equalsIgnoreCase("${caseNumber}"))
                        filter.setValue(orderId);
                    sir.setFilter(filter);
                }
            }
            List<DateFilter> dateFilters = service.getSirConfig().getDateFilters();
            if(dateFilters!=null && dateFilters.size()>0) {
                for(DateFilter filter:dateFilters)
                    sir.setDateFilter(filter);
            }
        }
    }


    public void setResult(Exception e, Status status) {
        ResultUtils.setResult(e, status, result, sir);
        try {
            SirRunner.sirDriversQueue.put(sir);
        } catch (InterruptedException e1) {
            log.error("Impossible error! You can't see this message.", e1);
            ResultUtils.setResult(e, Status.UNKNOWN_ERROR, result, sir);
            result.setErrorMessage(e1.getMessage());
        }
//        RunnerV2.getInstance().freeDrivers.add(portal);
    }
}

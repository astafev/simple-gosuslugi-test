package ru.atc.gosuslugi.minitest.web.portal;

import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.config.Environment;
import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.util.ConfigUtils;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;

//перенесем сюда побольше методов из Utils
public class PortalDriver extends Driver implements IPortalDriver {
    public static final Logger log = LoggerFactory.getLogger(PortalDriver.class);
    public static final HashSet<String> allowedStatuses = new HashSet<String>(5);

    static {
        allowedStatuses.add("Исполнено");
        allowedStatuses.add("Отправлено в ведомство");
        allowedStatuses.add("Принято от заявителя");
        allowedStatuses.add("Принято ведомством");
    }

    public PortalDriver(WebDriver driver1) {
        super(driver1);
    }


    private List<WebElement> getDisplayedDicList(SearchContext searchContext, By by) {
        List<WebElement> webElements = searchContext.findElements(by);
        Iterator<WebElement> iterator = webElements.iterator();
        while (iterator.hasNext()) {
            WebElement element = iterator.next();
            if (element.getCssValue("display").equals("none")) {
                iterator.remove();
            }
        }
        return webElements;
    }

    private void pickFromDic(String value) {
        (new WebDriverWait(driver, 20)).until(new OneOfElementPresent(By.className("lineLookupCont")));

        List<WebElement> elements = getDisplayedDicList(
                getDisplayed(By.cssSelector("td.popupForm")).findElement(By.cssSelector("div.rubberWhiteBoxIn")), By.cssSelector("a.lineLookup td"));

        if (value == null || value.isEmpty()) {
//            new WebDriverWait(web, 20).until(new WaitIfClockIsDisplayed());
            elements.get(0).click();
            /*WebElement element = (new WebDriverWait(web, 20)).until(new OneOfElementPresent(By.className("popupBg")));
            (new WebDriverWait(web, 20)).until(new OneOfElementPresent(By.className("lineLookupCont")));
            element.findElement(By.className("scrollWrap")).findElement(By.className("lineLookupCont")).click();*/
        } else {
            List<String> strings = new LinkedList<String>();
            for (WebElement element : elements) {
                try {
                    strings.add(element.getText());
                } catch (NoSuchElementException e) {
                    log.error("Unable to find 'td' child of element '" + element.getTagName() + "' innerHTML=" + element.getAttribute("innerHTML"), e);
                }
            }

            try {
                if (value.startsWith("\"") && value.endsWith("\""))
                    for (int i = 0; i < strings.size(); i++) {
                        if (strings.get(i).equals(value)) {
                            elements.get(i).click();
                            return;
                        }
                    }
                else {
                    for (int i = 0; i < strings.size(); i++) {
                        if (strings.get(i).trim().equalsIgnoreCase(value.trim())) {
                            elements.get(i).click();
                            return;
                        }
                    }
                    throw new NoSuchElementException("No " + value);
                }
            } catch (NoSuchElementException e) {
                try {
                    searchInDicWindow(getDisplayed(By.className("popupBg")), value);
                    elements = getDisplayedDicList(getDisplayed(By.cssSelector("td.popupForm")).findElement(By.cssSelector("div.rubberWhiteBoxIn")), By.className("lineLookup"));
                    strings = new ArrayList<String>(elements.size());
                    for (WebElement element : elements) {
                        strings.add(element.findElement(By.tagName("td")).getText());
                    }
                    if (value.startsWith("\"") && value.endsWith("\""))
                        for (int i = 0; i < strings.size(); i++) {
                            if (strings.get(i).equals(value)) {
                                elements.get(i).click();
                                return;
                            }
                        }
                    else {
                        for (int i = 0; i < strings.size(); i++) {
                            if (strings.get(i).trim().equalsIgnoreCase(value.trim())) {
                                elements.get(i).click();
                                return;
                            }
                        }
                    }
                } catch (NoSuchElementException e1) {
                    throw new RuntimeException("Unable to pick value '" + value + "' from dictionary. Available: " + strings);
                }
            }
            throw new RuntimeException("Unable to pick value '" + value + "' from dictionary. Available: " + strings);
        }
    }

    /**
     * @return null если нет ошибки либо текст ошибки
     */
    private String checkDicError() {
        try {
            //проверем, может там уже написано что ниче не найдено
            List<WebElement> elem = driver.findElements(By.cssSelector("div.linesLookup span.no_data_msg"));
            for (WebElement e : elem) {
                if (e.isDisplayed())
                    return e.getText();
            }
            return null;
        } catch (NoSuchElementException e1) {
            return null;
        }
    }

    void searchInDicWindow(WebElement element, String textToSearch) {
        element.findElement(By.className("search-text-input")).sendKeys(textToSearch);
        element.findElement(By.className("searchFieldBut")).click();
    }

    /**
     * Находит все отображаемые ошибки валидации на странице
     */
    public Map<String, String> getErrors() {
        Map<String, String> mapWithErrors = new LinkedHashMap<String, String>();
        List<WebElement> errorElements = driver.findElements(By.cssSelector("label.error"));
        for (WebElement label : errorElements) {
            if (label.isDisplayed()) {
                mapWithErrors.put(label.getAttribute("for"), label.getText());
            }
        }
        return mapWithErrors;
    }

    public void goToNextStep(int initStep, int nextStep) throws ValidationException {
        if (nextStep == 0) {
            nextStep = initStep + 1;
        }
        driver.findElement(By.id("submitBtn")).click();
        int step = -1;
        try {
            step = getStepNum();
        } catch (NoSuchElementException e) {
            try {
                sendApplication();
            } catch (UnsendException e1) {
                throw new RuntimeException(e1);
            }
        }
        if (nextStep != step || nextStep == initStep) {
            Map<String, String> mapWithErrors = getErrors();
            if (nextStep != step || mapWithErrors.size() != 0) {
                ValidationException exception = new ValidationException(this, "Appeared on step " + step + " instead of "
                        + nextStep + "\n" + mapWithErrors.toString());
                exception.errorLabels = mapWithErrors;
                throw exception;
            } else {
                //everything fine
                return;
            }
        }
    }

    public void goToNextStep() throws ValidationException {
        goToNextStep(getStepNum(), 0);
    }

    public int getStepNum() {
        try {
            return Integer.valueOf(driver.findElement(By.id("stepNum")).getAttribute("value"));
        } catch (NoSuchElementException e) {
            throw new UnknownStepException(e.getMessage(), e);
        }
    }

    public String getStepDescription() {
        try{
            WebElement stepTitle = findElement(By.cssSelector("table.stepMenu.stepMenuWidth")).findElement(By.xpath("//tr[2]/td[" + getStepNum() + "]"));
            return stepTitle.getText();
        } catch (NoSuchElementException e) {
            throw new UnknownStepException("Can't find navigation panel", e);
        }

    }


    public void fillPage(Step step) {
        fillPage(step.getStepValues());
    }

    public void fillPage(List<Value> values) {
        if (values == null) {
            return;
        }
        if (values.size() > 0)
            if (!values.get(0).getType().equals("button")) {
//                (new WebDriverWait(web, 15)).until(ExpectedConditions.presenceOfElementLocated(By.id(values.get(0).getName())));
            }
        for (Value value : values) {
            try {
                putValue(value);
            } catch (Exception e) {
                log.warn("Error occured during trying to put value " + value, e);
                throw new PortalException(this, "Error occured during trying to put value " + value + "\n" + e.getMessage(), e);
            }
        }
    }

    /**
     * <p>Заполняет значение радиогруппы. value.getValue - часть текстового содержания элемента. Если в кавычках - то точное совпадение.</p>
     * <p>Если используем магию, dicValue - значение, которое ставим в input, если там пусто, берем из value</p>
     */
    public void putValueInRadio(Value value) {
        if (!value.isToUseMagic()) {
            WebElement row = findElement(By.xpath("//input[@id='" + value.getName() + "']/../.."));
            List<WebElement> elements;
            if (value.getValue().startsWith("\"") && value.getValue().endsWith("\""))
                elements = row.findElements(By.xpath("//span[text()='" + value.getValue().substring(1, value.getValue().length() - 1) + "']"));
            else
                elements = row.findElements(By.xpath("//span[contains(text(),'" + value.getValue() + "')]"));
            if (elements.size() > 1) {
                StringBuilder message = new StringBuilder("Found more than one variants in radiogroup ");
                message.append(value);
                for (WebElement element : elements) {
                    message.append("\n");
                    message.append(element.getText());
                }
                log.warn(message.toString());
            }
            if (elements.size() == 0) {
                log.error("Didn't found any variants in radiogroup" + value);
            } else
                elements.get(0).click();
        } else {
            String magicValue = value.getDicVal();
            if (magicValue == null || value.getDicVal().isEmpty()) {
                magicValue = value.getValue();
            }
            StringBuilder sb = new StringBuilder();
            sb.append("$('#" + value.getName() + "').val('" + magicValue + "');");
            sb.append("$('#" + value.getName() + "').trigger();");
            executeScript(sb.toString());
        }
    }

    public void putValueInText(Value value) {
        if (value.isToUseMagic()) {
            ((JavascriptExecutor) driver).executeScript("$('#" + value.getName() + "').val('" + value.getValue() + "');"
                    + "$('#" + value.getName() + "').change()");
        } else {
            WebElement input = driver.findElement(By.id(value.getName()));
            String inputType = input.getAttribute("type");
            if (inputType != null && inputType.equals("checkbox")) {
                putValueInCheckbox(value);
            } else {
                input.clear();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                input.sendKeys(value.getValue().trim());
            }
        }
    }

    public void putValue(Value value) {
        log.debug("putting value " + value.toString());
        try {
            switch (InputType.valueOf(value.getType())) {
                case dic:
                case staticDic:
                case hierarchyDic:
                    putValueInDic(value);
                    break;
                case pause:
                    try {
                        try {
                            Thread.sleep(Long.parseLong(value.getValue()));
                        } catch (NumberFormatException e) {
                            log.error("wrong format of pause value: " + value.getValue(), e);
                            Thread.sleep(1000);
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case file:
                    try {
                        putValueInFile(value);
                    } catch (IOException e) {
                        log.error("Error while putting value: " + value, e);
                    }
                    break;
                case date:
                    if (value.getType().equals("date")) {
                        try {
                            value.setValue(dateValue(value));
                        } catch (ParseException e) {
                            throw new PortalException(this, "Error while parsing value\n" + value, e);
                        }
                    }
                case text:
                    putValueInText(value);
                    break;
                case checkbox:
                    putValueInCheckbox(value);
                    break;
                case checkboxGroup:
                    putValueInCheckboxGroup(value);
                case checkboxLookup:
                    putValueInCheckboxLookup(value);
                    break;
                case button:
                    clickButton(value);
                    break;
                case radio:
                    putValueInRadio(value);
                    break;
                default:
                    log.error("Unknown input type: " + value);
            }
        } catch (IllegalArgumentException e) {
            log.warn("Unknown input type: " + value);
            putValueInText(value);
        }
    }

    public void clickButton(Value value) {
        if (value.getValue() == null || value.getValue().isEmpty()) {
            findElement(By.id(value.getName())).click();
        } else {
            List<WebElement> buttons = findElements(By.cssSelector(value.getValue()));
            boolean clickNthButton = false;
            int n = 0;
            try {
                n = Integer.valueOf(value.getDicVal());
                clickNthButton = true;
            } catch (Exception e) {
                //do nothing
            }
            if (clickNthButton == false) {
                for (WebElement elem : buttons) {
                    if (elem.isDisplayed()) {
                        elem.click();
                        break;
                    }
                }
            } else {
                List<WebElement> displayedButtons = new LinkedList<WebElement>();
                for (WebElement elem : buttons) {
                    if (elem.isDisplayed()) {
                        displayedButtons.add(elem);
                    }
                }
                displayedButtons.get(n).click();
            }
        }
    }

    public void putValueInCheckboxLookup(Value value) {
        if (value.isToUseMagic()) {
            StringBuilder sb = new StringBuilder();

            sb.append("document.getElementById('").append(value.getName()).append("').value='");
            String[] values = null;
            if (!(value.getDicVal() == null) && !value.getDicVal().isEmpty()) {
                sb.append(value.getDicVal());
                values = value.getValue().split("\\|");
            } else {
                sb.append(value.getValue());
            }
            sb.append("';");

            if (values != null) {
                sb.append("document.getElementById(\"" + value.getName() + "-lookup-form-text\").value=\"[Изменить]\";");
                sb.append("$('#" + value.getName() + "-lookup-form-clear-button').show();");
                sb.append("var element = document.getElementById(\"" + value.getName() +
                        "\").parentNode.getElementsByClassName('checkboxLookup-value')[0];");
                for (String val : values) {
                    sb.append("var new_li=document.createElement('li');")
                            .append("new_li.innerHTML=\"" + val + "\";");
                    sb.append("element.appendChild(new_li);");
                }
            }
            sb.append("$('#" + value.getName() + "').change();");
            executeScript(sb.toString());
        } else {
            try {
                findElement(By.id(value.getName() + "-lookup-form-text")).click();
                //типа ждем появления
                (new WebDriverWait(this, 10)).until(ExpectedConditions.presenceOfElementLocated(By.className("popupFormBg")));
//                web.findElement(By.className("popupFormBg"));
                WebElement popup = getDisplayed(By.cssSelector("td.popupForm"));
                List<WebElement> elements = getDisplayedDicList(popup.findElement(By.cssSelector("div.rubberWhiteBoxIn")), By.cssSelector("a.lineLookup"));

                if (value.getValue() == null || value.getValue().isEmpty() && value.getDicVal() == null || value.getDicVal().isEmpty()) {
                    elements.get(0).findElement(By.tagName("span")).click();
                } else {
                    if (value.getValue() != null && !value.getValue().isEmpty()) {
                        Map<String, WebElement> elementMap = new HashMap<String, WebElement>();
                        for (WebElement element : elements) {
                            elementMap.put(element.getText(), element);
                        }

                        String[] valuesArray = value.getValue().split("\\|");
                        for (int i = 0; i < valuesArray.length; i++) {
                            boolean found = false;
                            for (String val : elementMap.keySet()) {
                                if (val.contains(valuesArray[i].trim())) {
                                    elementMap.get(val).findElement(By.tagName("span")).click();
                                    break;
                                }
                            }
                            if (!found)
                                throw new PortalException(this, "Не могу найти значение справочника " + valuesArray[i] + ".\n Доступные значения: " + elementMap.keySet());
                        }
                    }
                    if (value.getDicVal() != null && !value.getDicVal().isEmpty()) {
                        Map<String, WebElement> elementMap2 = new HashMap<String, WebElement>();
                        for (WebElement element : elements) {
                            elementMap2.put(element.findElement(By.tagName("li")).getAttribute("rel"), element);
                        }
                        String[] valuesArray = value.getDicVal().split("\\|");
                        for (int i = 0; i < valuesArray.length; i++) {
                            boolean found = false;
                            for (String val : elementMap2.keySet()) {
                                if (val.equals(valuesArray[i].trim())) {
                                    elementMap2.get(val).findElement(By.tagName("span")).click();
                                    break;
                                }
                            }
                            if (!found)
                                throw new PortalException(this, "Не могу найти значение справочника (dicVal) " + valuesArray[i] + ".\n Доступные значения: " + elementMap2.keySet());
                        }
                    }

                }
                popup.findElement(By.className("popupSelectBut")).click();
            } catch (RuntimeException e) {
                throw new PortalException(this, "Произошла ошибка при попытке заполнить справочник\nvalue: " + value.toString(), e);
            }
        }

    }

    /**
     * либо в значении через | тектовые значения, которые надо true сделать, либо
     * to_test
     */
    public void putValueInCheckboxGroup(Value value) {
        if (value.isToUseMagic()) {
            StringBuilder sb = new StringBuilder();
            sb.append("document.getElementById('").append(value.getName()).append("').value='");
            if (value.getDicVal() == null || value.getDicVal().isEmpty()) {
                sb.append(value.getDicVal());
            } else {
                sb.append(value.getValue());
            }
            sb.append("';$('#" + value.getName() + " ').change();");
            executeScript(sb.toString());
        } else {
            List<WebElement> lis = findElement(By.cssSelector("#" + value.getName() + "-checkboxGroup")).findElements(By.tagName("li"));
            List<String> textValues = new LinkedList<String>();
            if (value.getValue() != null && !value.getValue().isEmpty()) {
                String[] values = value.getValue().split("\\|");
                textValues.addAll(Arrays.asList(values));
            }
            List<String> dicValues = new LinkedList<String>();
            if (value.getDicVal() != null && !value.getDicVal().isEmpty()) {
                String[] values = value.getDicVal().split("\\|");
                for (String val : values) {
                    dicValues.add(val.trim());
                }

            }
            for (WebElement li : lis) {
                boolean toSelect = dicValues.contains(li.getAttribute("rel"));
                if (!toSelect) {
                    String text = li.findElement(By.tagName("span")).getText();
                    for (String val : textValues) {
                        if (text.contains(val.trim())) {
                            toSelect = true;
                            break;
                        }
                    }
                }
                boolean selected = li.getAttribute("class").contains("selected");
                if (selected != toSelect) {
                    li.click();
                }
            }
        }
    }

    public void putValueInCheckbox(Value value) {
        if (!value.isToUseMagic()) {
            WebElement input = driver.findElement(By.id(value.getName()));
            String inputType = input.getAttribute("type");
            if (!inputType.equals("checkbox")) {
                throw new IllegalArgumentException("Wrong type of input: " + inputType + ".\nValue: " + value.toString());
            }

            if (value.getValue() == null || value.getValue().length() == 0 ||
                    value.getValue().equals("false") || value.getValue().equals("0")
                    || value.getValue().equals("off")) {
                if (input.getAttribute("value").equals("on"))
                    driver.findElement(By.id(value.getName())).findElement(By.xpath("../a[2]")).click();
            } else if (value.getValue().equals("true") || value.getValue().equals("1")
                    || value.getValue().equals("on")) {
                if (input.getAttribute("value").equals("off"))
                    driver.findElement(By.id(value.getName())).findElement(By.xpath("../a")).click();
            } else
                throw new IllegalArgumentException("Wrong value for type \"checkbox\".\nValue: " + value.toString());
        } else {
            String val = value.getDicVal();
            if (val == null || val.isEmpty()) {
                val = value.getValue();
            }
            executeScript("document.getValueById('" + value.getName() + "').value = " + val + ";$('#" + value.getName() + "').change();");
        }
    }


    public void logIn(String username, String password, boolean trySecondTime) {
        log.info("Logging in on page: " + driver.getCurrentUrl() + "\nwith login: " + username);
        if (isLoggedIn()) {
            log.warn("Already logged in.");
            return;
        }
        try {
            driver.findElement(By.id("username"));
        } catch (NoSuchElementException e) {
            driver.findElement(By.id("sign-in-register")).findElement(By.cssSelector("a.sign-in")).click();
            (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        }
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
//      Почему то эта ебаная хрень не работает
//        web.findElement(By.xpath("(//button[@value='Войти'])[4]")).click();
        List<WebElement> buttons = driver.findElements(By.tagName("button"));
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).isDisplayed()) {
                buttons.get(i).click();
                break;
            }
        }
        try {
            Thread.sleep(1000);
            WebElement errorLabel = driver.findElement(By.id("errorLabel"));
            if (errorLabel.isDisplayed()) {
                if (errorLabel.getText().trim().equals("Введено неверное имя пользователя или пароль") && trySecondTime) {
                    //пытаемся 2-й раз. А вдруг?!
                    logIn(username, password, false);
                }
                throw new RuntimeException(errorLabel.getText());
            }
        } catch (NoSuchElementException e) {
            //do nothing. everything fine
        } catch (StaleElementReferenceException e) {
            log.warn(e.getMessage(), e);
        } catch (InterruptedException e) {
            log.error("somebody interrupted", e);
            throw new RuntimeException(e);
        }
        log.info("successfully logged in");
    }

    public void logIn() {
        for (Environment env : ConfigUtils.ENVIRONMENTS) {
            if (env.isThisEnv(driver.getCurrentUrl())) {
                logIn(env.getLogin(), env.getPassword(), true);
                return;
            }
        }
        throw new RuntimeException("Unknown environment. " + driver.getCurrentUrl());
    }

    public void selectRightRegion(String... region) {
        if (
                (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("region-selected"))).getText()
                        .equals(region[region.length - 1])
                ) {
            log.info("Right region already selected. " + Arrays.toString(region));
        } else {
            log.info("Selecting region. " + Arrays.toString(region));

            driver.findElement(By.id("region-selected")).click();
//            (new WebDriverWait(web, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.search > input[type=\"text\"]")).click();
            //ждем?
            if (region.length == 1) {
                try {
                    driver.findElement(By.xpath("//td[text()='" + region[0] + "']")).click();
                } catch (NoSuchElementException e) {
                    driver.findElement(By.cssSelector("div.search > input[type=\"text\"]")).sendKeys(region[0]);
                    driver.findElement(By.cssSelector("div.search > input[type=\"text\"]")).submit();
                    (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='" + region[0] + "']"))).click();
                }
            } else {
                for (int i = 0; i < region.length; i++) {
                    (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='" + region[i] + "']"))).click();
                }
            }
            try {
                Thread.sleep(1500);  //я не ебу как еще обхитрить
                (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.select"))).click();
//            web.findElement(By.cssSelector("button.select")).click();
            } catch (NoSuchElementException e) {
                log.error("Unable to find button 'css=button.select'\n" + Arrays.toString(region));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }//*/
        }
    }

    public boolean isLoggedIn() {
        if (driver.getCurrentUrl().contains("://sia") || driver.getCurrentUrl().contains("://esia")) {
            return false;
        }
        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
            if (isExitBoxOnPage())
                return true;
            else {
                try {
//                    new WebDriverWait(web, 0).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.sign-in")))
                    findElement(By.cssSelector("a.sign-in")).click();
//                    Thread.sleep(1500);
                    return isExitBoxOnPage();
                } catch (NoSuchElementException e1) {
                    String url = driver.getCurrentUrl();
                    driver.get(url.substring(0, url.indexOf('/', 9)) + "/pgu/personcab");
//                    Thread.sleep(1000);
                    return isExitBoxOnPage();
                }
            }
        } finally {
            int implicitWaitPeriod = Integer.valueOf(System.getProperty("webdriver.implicitWaitPeriod", "3"));
            driver.manage().timeouts().implicitlyWait(implicitWaitPeriod, TimeUnit.SECONDS);
        }
    }

    private boolean isExitBoxOnPage() {
        try {
            if(getDisplayed(By.cssSelector("*.exit-box"))!=null)
                return true;
            else return false;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void putValueInFile(Value value) throws IOException {

        WebElement div = driver.findElement(By.id(value.getName() + ".fileID-upl"));
        WebElement fileLoad = div.findElement(By.name("file"));
        fileLoad.sendKeys(getFilePath(value));
        try {
            (new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOfElementLocated(By.className("qq-upload-spinner")));
            if (div.findElement(By.className("qq-upload-failed-text")).isDisplayed()) {
                throw new PortalException(this, "Произошла ошибка при загрузке файла");
            }
        } catch (Exception e) {
            if (e instanceof PortalException)
                throw (PortalException) e;
            throw new PortalException(this, "", e);
        }
    }

    public void logOut() {
        try {
            driver.findElement(By.cssSelector("*.exit-box")).click();
        } catch (NoSuchElementException e) {
            log.warn("Possibly, already logged out. ", e);
            return;
        }
    }

    public void startNewDeclaration() throws BrokenServiceException {
        new WebDriverWait(this, 10).until(new WaitIfClockIsDisplayed());
        try {
            int stepNum = getStepNum();
            log.info("Service started. Step " + stepNum);
            return;
        } catch (NoSuchElementException e) {
            //do nothing
        }
        String url = driver.getCurrentUrl();
        if (driver.getTitle().contains("Информация об услуге")) {
            try {
                try {
                    (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Получить услугу"))).click();
                } catch (TimeoutException e1) {
                    driver.navigate().refresh();
                    (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Получить услугу"))).click();
                }
                try {
                    try {
                        getStepDescription();
                        return;
                    } catch (NoSuchElementException e) {
                        Thread.sleep(1000);
                        getStepDescription();
                        return;
                    }
                } catch (NoSuchElementException e) {
                    new WebDriverWait(driver, 20).until(ExpectedConditions.titleContains("Выбор черновика заявления"));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            } catch (NoSuchElementException e) {
                log.error("Unable to find button 'Получить услугу'. " + driver.getCurrentUrl());
                throw new BrokenServiceException(e);
            } catch (TimeoutException e) {
                log.error("Unable to find button 'Получить услугу'. " + driver.getCurrentUrl());
                throw new BrokenServiceException(e);
            }
        }
        if (driver.getTitle().contains("Выбор черновика заявления")) {
            (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.className("inner")))
                    .findElement(By.className("defaultValue"))
                    .findElement(By.className("radio")).click();
            driver.findElement(By.name("sendDraft")).click();
        } else throw new BrokenServiceException("Wrong title! '" + driver.getTitle() + "' on: " + url);
    }

    //TODO если нет значения, воспринимать dicVal как номер эл-та который надо выбрать
    void putValueInDic(Value value) {
        if (value.getValue() != null && !value.isToUseMagic()) {
            try {
                driver.findElement(By.id(value.getName() + "-lookup-form-text")).click();
                if (value.getValue().isEmpty()) {
                    pickFromDic(null);
                } else {
                    String[] valuesArray = value.getValue().split("/");
                    for (String val : valuesArray) {
                        pickFromDic(val);
                    }
                    Thread.sleep(500);
                    WebElement popupBg = OneOfElementPresent.oneOfElementPresent(driver, By.className("popupBg"));
                    if (popupBg != null) {
                        popupBg.findElement(By.className("popupSelectButText")).click();
                    }
                }
            } catch (RuntimeException e) {
                throw new PortalException(this, "Произошла ошибка при попытке заполнить справочник\nvalue: " + value.toString(), e);
            } catch (InterruptedException e) {
                log.error("Impossible error. You can't see this message", e);
                throw new RuntimeException(e);
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("document.getElementById('" + value.getName() + "').value = '" + value.getDicVal() + "';");
            if (value.getDicVal() != null)
                sb.append("document.getElementById('" + value.getName() + "-lookup-form-text').value = '" + value.getValue() + "';");
            sb.append("$('#" + value.getName() + "-lookup-form-clear-button').show();");
            sb.append("$('#" + value.getName() + "').change();");
            ((JavascriptExecutor) driver).executeScript(sb.toString());
        }
    }

    /**
     * <p>Посылает заявление.</p>
     * <p>Если после нажатия кнопки есть панель навигации - проблемы.<br>
     * Если находит ошибки валидации, кидает ValidationException, иначе просто PortalException</p>
     * <p>Если не возникает ошибки отправки/формирования, переходит на страницу заявления, если там статус -
     * "принято от заявителя" или "отправлено в ведомство", то все хорошо</p>
     */
    public Map<String, String> sendApplication() throws UnsendException {
        try {
            driver.findElement(By.id("submitBtn")).click();
        } catch (NoSuchElementException e) {
            log.info("Application probably already send");
        }
        String title = driver.getTitle();
        if (title.contains("Ошибка")) {
            String message = driver.findElement(By.cssSelector("h1 + p")).getText();
            throw new UnsendException(this, message);
        } else {
            try {
                log.info(driver.findElement(By.className("lastStepText")).getText());
            } catch (Exception e) {
                log.warn(e.getMessage(), e);
            }
        }

        try {
            driver.findElement(By.cssSelector("div.white-wrapper")).findElement(By.xpath("//span[contains(text(),'Информация по данному заявлению')]")).click();
        } catch (NoSuchElementException e1) {
            try {
                int pageNum = getStepNum();
                log.error("Error occured. Wanted to send application, instead process appeared on step " + pageNum);
                Map<String, String> mapWithErrors = getErrors();
                if (mapWithErrors.size() > 0) {
                    throw new ValidationException(this, "Couldn't send application. Errors:\n" + mapWithErrors.toString());
                }
                throw new PortalException(this, "Couldn't send application");
            } catch (Exception e) {

            }
        } catch (org.openqa.selenium.TimeoutException e) {
            if (Boolean.valueOf(System.getProperty("fuckingStable", "true"))) {
                log.warn("Probable skipping error. Page didn't load.", e);
                driver.get(driver.getCurrentUrl());
            } else throw e;
        }

        try {
            //Иногда не удается нажать c первого раза почему-то
            driver.findElement(By.cssSelector("div.white-wrapper")).findElement(By.xpath("//span[contains(text(),'Информация по данному заявлению')]")).click();
        } catch (NoSuchElementException e) {
            //Ничего не делаем, сюда будет попадать, если не удалось нажать на "Информация по данному заявлению"
        }

        Map<String, String> applicationInfo = new LinkedHashMap<String, String>();
        addApplicationInfo(applicationInfo);
        log.debug("application info", applicationInfo);
        String status = applicationInfo.get("статус");
        if (!allowedStatuses.contains(status)) {
            throw new UnsendException(this, "Status: " + status);
        }

        String successMessage = driver.findElement(By.cssSelector("div.form-text.form-text-common")).getText();
        log.info("Application successfully sent.\n" + successMessage);
        return applicationInfo;
    }

    public void addApplicationInfo(Map<String, String> appInfo) {
        if (getTitle().contains("Информация об услуге")) {
            WebElement organ = new WebDriverWait(this, 30).until(new OneOfElementsPresentsByChain(this, true, By.cssSelector("div.center"), By.cssSelector("h1 a.sub-title")));
            WebElement service = findElement(By.cssSelector("h1 div.title"));

//            WebElement header = getDisplayed(By.tagName("h1"));
            appInfo.put("ведомство", organ.getText());
            appInfo.put("услуга", service.getText());
//            System.out.println(appInfo);
        } else if (getTitle().contains("Выбор черновика заявления")) {
            WebElement description = findElement(By.className("serviceDescription"));
            for(WebElement row:description.findElements(By.tagName("dl"))) {
                appInfo.put(
                        row.findElement(By.tagName("dt")).getText().toLowerCase(),
                        row.findElement(By.tagName("dd")).getText()
                );
            }
        } else if (getTitle().contains("Информация о заявке")) {
            WebElement form = findElement(By.className("form-text-common"));

            for (WebElement element : form.findElements(By.tagName("dl"))) {
                List<WebElement> dt = element.findElements(By.tagName("dt"));
                if (dt.size() > 0) {
                    String key = dt.get(0).getText().trim().toLowerCase();
                    String value = element.findElement(By.tagName("dd")).getText();
                    appInfo.put(key, value);
                }
            }
        }


    }

    public String getOrderId() {
        try{
            return findElement(By.id("orderId")).getAttribute("value");
        } catch (Exception e) {
            return null;
        }
    }

}
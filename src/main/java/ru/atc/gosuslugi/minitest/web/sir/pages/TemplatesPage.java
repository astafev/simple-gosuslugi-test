package ru.atc.gosuslugi.minitest.web.sir.pages;

import ru.atc.gosuslugi.minitest.web.sir.Page;
import ru.atc.gosuslugi.minitest.web.sir.SirDriver;

/**
 * Date: 14.10.13
 * Author: astafev
 */
public class TemplatesPage extends SirPageObject {
    static {
        pageId = "view-defenition";
        panelId = "definitionPanel";
    }

    private TemplatesPage(SirDriver driver) {
        super(driver);
    }

    public static TemplatesPage goToTemplatesPage (SirDriver driver1) {
        return new TemplatesPage(driver1);
    }


    @Override
    public Page getPage() {
        return Page.PROCESS_TEMPLATES;
    }
}

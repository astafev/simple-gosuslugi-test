package ru.atc.gosuslugi.minitest.web.sir;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.run.AfterActionRunner;
import ru.atc.gosuslugi.minitest.jaxb.values.*;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.Results;
import ru.atc.gosuslugi.minitest.result.Status;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Date: 18.09.13
 * Author: astafev
 */
public class SirRunner extends AfterActionRunner {

    public static final Logger log = LoggerFactory.getLogger(SirRunner.class);
    private Results results;
    ExecutorService executor;
    List<Future<Result>> futures = new LinkedList<Future<Result>>();
    List<? extends WebDriver> drivers;
    static BlockingQueue<SirDriver> sirDriversQueue = new LinkedBlockingQueue<SirDriver>();
    static BlockingQueue<PortalDriver> freeDrivers;

    private Thread queueBrokerThread;
    public final int numberOfThreads;

    public SirRunner(BlockingQueue<PortalDriver> freeDrivers, List<PortalDriver> drivers, Results results) {
        SirRunner.freeDrivers = freeDrivers;
        this.results = results;
        this.drivers = drivers;

        this.numberOfThreads = Integer.parseInt(System.getProperty("numberOfThreads", "1"));
        this.executor  = Executors.newFixedThreadPool(numberOfThreads);
    }



    @Override
    public void run() throws InterruptedException {
        //Первым делом запустим штуку, которая будет объекты из очереди тырить
        DriversQueueBroker queueBroker = new DriversQueueBroker(freeDrivers, sirDriversQueue, true);
        queueBrokerThread = new Thread(queueBroker);
        queueBrokerThread.setDaemon(true);
        queueBrokerThread.start();

        for (int i = 0; i<results.getResults().size(); i++) {
            Result result = results.getResults().get(i);
            if(Thread.currentThread().isInterrupted()) {
                this.quit();
                return;
            }
            if(result.getStatus() == Status.SENT) {
                switch (After.valueOf(result.getAfterAction())) {
                    case SIR_CANCEL_JS:
                        log.warn("SIR_CANCEL_JS probably not implemented yet. Going to use usual!");
                    case SIR_CANCEL:
                        //Ждем тут
                        SirStopProcessCallable sirStopProcessCallable = new SirStopProcessCallable(result, sirDriversQueue.take());
                        Future<Result> future1 = executor.submit(sirStopProcessCallable);
                        futures.add(future1);
                        break;
                    case SIR_CHECK:
                        log.debug("going to check " + result.getApplication() + "");
                        SirCheckCallable sirCheckCallable = new SirCheckCallable(result, sirDriversQueue.take());
                        Future<Result> future2 = executor.submit(sirCheckCallable);
                        futures.add(future2);
                        break;
                }
            }
        }
    }

    @Override
    public void quit() {
        executor.shutdown();
        queueBrokerThread.interrupt();
        for (WebDriver driver : drivers) {
            try {
                driver.quit();
            } catch (Exception e) {
                log.error("Error while trying to close web", e);
            }
        }
        try {
            results.updateDB();
        } catch (Exception e) {
            log.error("error while updating db", e);
            log.info(results.getResults().toString());
        }
    }

    @Override
    public void waitFor() throws InterruptedException {
        while (futures.size() > 0) {
            Iterator<Future<Result>> iterator = futures.iterator();
            while (iterator.hasNext()) {
                Future<Result> future = iterator.next();
                if (future.isDone()) {
                    iterator.remove();
                    try {
                        future.get();
                    } catch (ExecutionException e) {
                        log.error("impossible error. It shouldn't happen", e);
                    }
                    results.updateDB();
                }
            }
            Thread.sleep(5000);
        }
        queueBrokerThread.interrupt();
        queueBrokerThread.join();
        DriversQueueBroker queueBroker = new DriversQueueBroker(freeDrivers, sirDriversQueue, false);
        queueBrokerThread = new Thread(queueBroker);
        queueBrokerThread.setDaemon(true);
        queueBrokerThread.start();
        executor.shutdown();
    }
}
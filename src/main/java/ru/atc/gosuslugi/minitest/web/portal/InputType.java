package ru.atc.gosuslugi.minitest.web.portal;

/**
 * Date: 28.10.13
 * Author: astafev
 */
public enum InputType {
    text,
    radio,
    dic,
    staticDic,
    hierarchyDic,
    checkbox,
    file,
    button,
    checkboxGroup,
    checkboxLookup,
    pause,
    date,

    fieldDropDown,
    kladr,
    fias,
}

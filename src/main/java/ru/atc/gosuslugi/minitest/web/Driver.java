package ru.atc.gosuslugi.minitest.web;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.internal.Killable;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;
import ru.atc.gosuslugi.minitest.util.ValueUtils;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Date: 18.09.13
 * Author: astafev
 */
public abstract class Driver implements IDriver {
    protected WebDriver driver;
    public static final Logger log = LoggerFactory.getLogger(Driver.class);

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    public static final Pattern P_DATE_FORMAT = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}");
    public boolean brokenDriver = false;


    public void reinit() {
        try {
            driver.close();
        } catch (Exception e) {
            if (driver instanceof Killable) {
                ((Killable) driver).kill();
            }
        }
        driver = createNewWebDriver().getDriver();
    }

    public Driver(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public void get(String s) {
        driver.get(s);
    }

    @Override
    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return driver.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    @Override
    public String getPageSource() {
        return driver.getPageSource();
    }

    @Override
    public void close() {
        driver.close();
    }

    @Override
    public void quit() {
        driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return driver.navigate();
    }

    @Override
    public Options manage() {
        return driver.manage();
    }

    @Override
    public Object executeScript(String s, Object... objects) {
        return ((JavascriptExecutor) driver).executeScript(s, objects);
    }

    @Override
    public Object executeAsyncScript(String s, Object... objects) {
        return ((JavascriptExecutor) driver).executeAsyncScript(s, objects);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> xOutputType) throws WebDriverException {
        return ((TakesScreenshot) driver).getScreenshotAs(xOutputType);
    }

    /**
     * @return имя файла, где будет скриншот в папке для скриншотов (java.io.tmpdir)
     */
    public String takeScreenshot() {
        String screenshot = String.valueOf(System.currentTimeMillis()) + ".png";
        this.takeScreenshot(screenshot);
        return screenshot;
    }

    public void takeScreenshot(String fileName) {
        if (System.getProperty("screenshotsPath") == null) {
            System.setProperty("screenshotsPath", System.getProperty("java.io.tmpdir") + File.separator);
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        log.debug("taking screenshot to file \"" + screenshot + "\"");
        try {
            String screenshotName = System.getProperty("screenshotsPath") + File.separator + fileName;
            log.debug("screenshot file: " + screenshotName);
            FileUtils.copyFile(screenshot, new File(screenshotName));
        } catch (IOException e) {
            log.error("", e);
        }
    }

    public void takeScreenshot(File file) {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, file);
        } catch (IOException e) {
            log.error("", e);
        }
    }

    public static Date stringToDate(String val) throws ParseException {
        if (val == null || val.isEmpty()) {
            return new Date();
        }
        Matcher matcher = P_DATE_FORMAT.matcher(val);
        if (matcher.matches())
            return DATE_FORMAT.parse(val);
        List<String> vals = Arrays.asList(val.split("\\s*"));
        Calendar calendar = new GregorianCalendar();
        Iterator<String> iter = vals.iterator();
        while (iter.hasNext()) {
            String part = iter.next();
            if (P_DATE_FORMAT.matcher(part).matches()) {
                calendar.setTime(DATE_FORMAT.parse(part));
                iter.remove();
            }
        }

        iter = vals.iterator();
        while (iter.hasNext()) {
            String part = iter.next();
            int addVal = Integer.valueOf(part.substring(0, part.length() - 2), 10);
            String s = part.substring(part.length() - 2);
            if (s.equals("s")) {
                calendar.add(Calendar.SECOND, addVal);
            } else if (s.equals("m")) {
                calendar.add(Calendar.MINUTE, addVal);
            } else if (s.equals("h")) {
                calendar.add(Calendar.HOUR, addVal);
            } else if (s.equals("d")) {
                calendar.add(Calendar.DAY_OF_MONTH, addVal);
            } else if (s.equals("M")) {
                calendar.add(Calendar.MONTH, addVal);
            } else if (s.equals("y")) {
                calendar.add(Calendar.YEAR, addVal);
            } else if (s.equals("w")) {
                calendar.add(Calendar.WEEK_OF_YEAR, addVal);
            } else throw new ParseException("Unknown type " + s, part.length() - 2);
        }
        return calendar.getTime();
    }

    protected String dateValue(Value value) throws ParseException {
        return DATE_FORMAT.format(ValueUtils.stringToDate(value.getValue()));
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getDisplayedList(SearchContext searchContext, By by) {
        return getDisplayedList(searchContext, by, 0);
    }

    public List<WebElement> getDisplayedList(SearchContext searchContext, By by, int listSize) {
        List<WebElement> webElements = searchContext.findElements(by);
        List<WebElement> displayedElements = new LinkedList<WebElement>();
        for (WebElement element : webElements) {
            if (element.isDisplayed()) {
                displayedElements.add(element);
                if (listSize > 0 && displayedElements.size() >= listSize) {
                    break;
                }
            }
        }
        return displayedElements;
    }

    public List<WebElement> getDisplayedList(By by) {
        return getDisplayedList(this, by);
    }

    /**
     * @returns null либо 1-й отображаемый элемент
     */
    public WebElement getDisplayed(SearchContext searchContext, By by) {
        List<WebElement> list = getDisplayedList(searchContext, by, 2);
        if (list.size() > 1)
            log.warn("more than one displayed. By: " + by + " on " + getCurrentUrl());
        if (list.size() == 0)
            return null;
        return list.get(0);
    }

    public WebElement getDisplayed(By by) {
        return getDisplayed(this, by);
    }

    public Keyboard getKeyboard() {
        return ((HasInputDevices) driver).getKeyboard();
    }

    public Mouse getMouse() {
        return ((HasInputDevices) driver).getMouse();
    }

    public void setDefaultImplicitWait() {
        try {
            int waitPeriod = Integer.parseInt(System.getProperty("webdriver.implicitWaitPeriod", "5"));
            driver.manage().timeouts().implicitlyWait(waitPeriod, TimeUnit.SECONDS);
        } catch (NumberFormatException e) {
            log.error("Wrong format of 'webdriver.implicitWaitPeriod':\n" + e.getMessage(), e);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }
    }

    protected String getFilePath(Value value) throws IOException {
        String fileName;
        if (!(value.getValue() == null || value.getValue().length() == 0)) {
            File file = new File(value.getValue());
            if (file.exists()) {
                fileName = value.getValue();
            } else {
                URL url = Driver.class.getResource(value.getValue());

                if (url == null) {
                    log.warn("Unable to find file " + value.getValue());
                    fileName = value.getValue();
                } else {
                    try {
                        file = new File(url.toURI());
                        if (file.exists())
                            fileName = file.getAbsolutePath();
                        else {
                            log.warn("Unable to find file " + value.getValue());
                            fileName = value.getValue();
                        }
                    } catch (URISyntaxException e) {
                        log.error("Error in syntax of " + value.getName() + "\n" + e.getMessage(), e);
                        throw new PortalException(this, "Error in syntax of " + value.getName() + "\n" + e.getMessage(), e);
                    }
                }
            }
        } else {
            if ((fileName = System.getProperty("loadingFile")) == null) {
                fileName = System.getProperty("java.io.tmpdir") + File.separator + "testFile.jpg";
                File file = new File(fileName);
                if (!file.exists()) {
                    if (!file.createNewFile()) {
                        throw new RuntimeException("Unable to create file in tmpdir");
                    }
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                    os.write(new byte[500]); //пишем 500 нулей)
                    os.close();
                }
                System.setProperty("loadingFile", fileName);
            }
        }
        return fileName;
    }

    public static PortalDriver createNewWebDriver() {
        DesiredCapabilities capabilities;
        if ("chrome".equals(System.getProperty("webdriver"))) {
            capabilities = DesiredCapabilities.chrome();
        } else if ("phantom".equals(System.getProperty("webdriver")) || "ghost".equals(System.getProperty("webdriver"))) {
            capabilities = DesiredCapabilities.phantomjs();
        } else {
            capabilities = DesiredCapabilities.firefox();
        }
        if (Boolean.valueOf(System.getProperty("webdriver.proxy"))) {
            String host = System.getProperty("webdriver.proxy.host", "localhost");
            String port = System.getProperty("webdriver.proxy.port", "8888");
            String PROXY = String.format("%s:%s", host, port);
            log.debug(String.format("creating proxy " + PROXY));
            Proxy proxy = new Proxy().setSslProxy(PROXY).setHttpProxy(PROXY);
            capabilities.setCapability(CapabilityType.PROXY, proxy);
        }

        PortalDriver driver;
        if (System.getProperty("webdriver") != null) {
            if (System.getProperty("webdriver").equals("chrome")) {
                driver = createNewChromeWebDriver(capabilities);
            } else if (System.getProperty("webdriver").equals("phantom") || System.getProperty("webdriver").equals("ghost")) {
                driver = createNewGhostDriver(capabilities);
            } else {
                throw new IllegalArgumentException("wrong property webdriver!" );
            }
        } else {
            final FirefoxBinary firefoxBinary;
            if (System.getProperty("firefoxPath") != null && System.getProperty("firefoxPath").length() > 0)
                firefoxBinary = new FirefoxBinary(new File(System.getProperty("firefoxPath")));
            else
                firefoxBinary = new FirefoxBinary();
            driver = createNewFirefoxWebDriver(firefoxBinary, capabilities);
        }
        try {
            int waitPeriod = Integer.parseInt(System.getProperty("webdriver.implicitWaitPeriod", "5"));
            driver.manage().timeouts().implicitlyWait(waitPeriod, TimeUnit.SECONDS);
            log.debug("implicit wait period " + waitPeriod);
        } catch (NumberFormatException e) {
            log.error("Wrong format of 'webdriver.implicitWaitPeriod':\n" + e.getMessage(), e);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }

        return driver;
    }

    public static PortalDriver createNewChromeWebDriver(Capabilities capabilities) {
        ChromeDriver chromeDriver = new ChromeDriver(capabilities);
        PortalDriver portalDriver = new PortalDriver(chromeDriver);
        return portalDriver;
    }

    public static PortalDriver createNewFirefoxWebDriver(FirefoxBinary firefoxBinary, DesiredCapabilities capabilities) {

        if (firefoxBinary == null) {
            firefoxBinary = new FirefoxBinary();
        }

        FirefoxProfile profile;
        if (System.getProperty("firefoxProfile") == null)
            profile = new FirefoxProfile();
        else {
            File file = new File(System.getProperty("firefoxProfile"));
            if (!file.exists() && !file.isDirectory()) {
                log.error("Can't find firefox profile in path " + System.getProperty("firefoxProfile"));
                profile = new FirefoxProfile();
            } else profile = new FirefoxProfile(file);
        }
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(false);
        if (Boolean.valueOf(System.getProperty("enableNativeEvents")))
            profile.setEnableNativeEvents(true);
        else
            profile.setEnableNativeEvents(false);
//        profile.set
//        profile.setPreference("webdriver.log.file", "firefoxJS.log");
        profile.setPreference("webdriver.firefox.logfile", "firefox.log");
        profile.setPreference("webdriver.log.driver", "WARNING");

        LoggingPreferences loggingPreferences = new LoggingPreferences();
//        loggingPreferences.enable(LogType.CLIENT, Level.OFF);
//        loggingPreferences.enable(LogType.BROWSER, Level.OFF);
        capabilities.setCapability("loggingPrefs", loggingPreferences);

        log.info("starting browser");

        FirefoxDriver driver = new FirefoxDriver(firefoxBinary, profile, capabilities);
//        driver.setLogLevel(Level.SEVERE);

        return new PortalDriver(driver);
    }

    public static PortalDriver createNewGhostDriver(DesiredCapabilities capabilities) {
        capabilities.setCapability("phantomjs.binary.path","/home/astafev/Downloads/phantomjs-1.9.2-linux-x86_64/bin/phantomjs" );
        WebDriver driver = new PhantomJSDriver(capabilities);
        return new PortalDriver(driver);
    }
}

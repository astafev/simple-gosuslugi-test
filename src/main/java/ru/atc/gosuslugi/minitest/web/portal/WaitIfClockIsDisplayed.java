package ru.atc.gosuslugi.minitest.web.portal;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * Date: 08.10.13
 * Author: astafev
 */
public class WaitIfClockIsDisplayed implements ExpectedCondition<Boolean> {

    @Override
    public Boolean apply(WebDriver driver) {
        try {
            //to_test
            By clockBy;
            if (driver instanceof PortalDriverSF)
                clockBy = By.id("shadowglass-clock");
            else clockBy = By.cssSelector("div.ui-widget-overlay > img");

            if (driver.findElement(clockBy).isDisplayed()) {
                if (driver instanceof PortalDriverSF) {
                    try {
                        //Панелька с черновиками
                        return driver.findElement(By.cssSelector(".draft-wrapper")).isDisplayed();
                    } catch (NoSuchElementException e) {
                        return false;
                    }
                } else return false;
            } else
                return true;
        } catch (NoSuchElementException e) {
            return true;
        } catch (StaleElementReferenceException e) {
            return apply(driver);
        }
    }
}

package ru.atc.gosuslugi.minitest.web.sir;

import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.PortalException;

/**
 * Ничем не отличается, просто для красоты кода
 * Date: 14.10.13
 * Author: astafev
 */
public class SirException extends PortalException {
    public SirException(Driver driver, String string) {
        super(driver, string);
    }

    public SirException(Driver driver, String string, Throwable cause) {
        super(driver, string, cause);
    }
}

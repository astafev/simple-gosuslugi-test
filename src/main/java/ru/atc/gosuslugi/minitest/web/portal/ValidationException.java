package ru.atc.gosuslugi.minitest.web.portal;
import ru.atc.gosuslugi.minitest.web.PortalException;

import java.util.Map;

/**
 * На шаге поля заполнены неверно
 */
public class ValidationException extends PortalException {
    public Map<String, String> errorLabels;


    public ValidationException(PortalDriver driver, String message) {
        super(driver, message);
    }

    public ValidationException(PortalDriver driver, String message, Throwable cause) {
        super(driver, message, cause);
    }
}

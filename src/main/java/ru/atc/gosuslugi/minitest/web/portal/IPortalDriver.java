package ru.atc.gosuslugi.minitest.web.portal;

import ru.atc.gosuslugi.minitest.web.IDriver;
import ru.atc.gosuslugi.minitest.jaxb.values.Step;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;

import java.util.Map;

/**
 * Date: 30.10.13
 * Author: astafev
 */
public interface IPortalDriver extends IDriver {

    void putValue(Value value);

    void goToNextStep(int initStep, int nextStep) throws ValidationException;

    void startNewDeclaration() throws BrokenServiceException;

    int getStepNum() throws UnknownStepException;

    Map<String, String> getErrors();

    Map<String, String> sendApplication() throws UnsendException;

    String getStepDescription();

    void fillPage(Step step);
    /**
     * должна быть открыта форма авторизации
     */
    void logIn(String username, String password, boolean trySecondTime);

    void logIn();

    boolean isLoggedIn();

    void selectRightRegion(String... region);

    String getOrderId();

    void addApplicationInfo(Map<String, String> appInfo);


}

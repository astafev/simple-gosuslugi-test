package ru.atc.gosuslugi.minitest.web.sir.js;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.web.sir.*;
import ru.atc.gosuslugi.minitest.util.ReflectUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 15.10.13
 * Author: astafev
 */
public class SirDriverJS {
    private SirDriver driver;
    public static Logger log = LoggerFactory.getLogger(SirDriverJS.class);

    private SirDriverJS(SirDriver driver) {
        this.driver = driver;
    }

    public static SirDriverJS getOne(SirDriver driver) {
        return new SirDriverJS(driver);
    }

    public static String script(String url, String request) {
        return String.format("var xmlhttp = new XMLHttpRequest();\n" +
                "xmlhttp.open('POST', '%s', false);\n" +
                "xmlhttp.setRequestHeader(\"Content-type\",\"application/x-www-form-urlencoded; charset=UTF-8\");\n" +
                "xmlhttp.send('%s');\n" +
                "return xmlhttp.responseText;", url, request);
    }

    /**
     * @param id - {ключ}.{pageId процесса}
     * @throws ru.atc.gosuslugi.minitest.web.PortalException
     *          если возникла ошибка
     */
    public void stopProcess(String id) {
        Object response = driver.executeScript("var xmlhttp = new XMLHttpRequest();\n" +
                "xmlhttp.open('POST', '/jbpm-console-v2.0/console/stopprocess', false);\n" +
                "xmlhttp.setRequestHeader(\"Content-type\",\"application/x-www-form-urlencoded; charset=UTF-8\");\n" +
                "xmlhttp.send('templateWork=All&id=" + id + "');\n" +
                "return xmlhttp.responseText;");
        if (response instanceof String) {
            String s_response = (String) response;
            if (s_response.contains("{success:true}")) {
                log.info("process " + id + " successfully stopped");
            } else
                throw new SirException(driver, "Error while trying to stop process " + id + "\n" + s_response.substring(0, 200), new NullPointerException(s_response));
        } else {
            log.warn("unknown type of response: " + response.getClass() + "\n" + response.toString());
        }
    }

    /**
     * {"total":"0","data":[]}
     * {"total":"2","data":[{"id":"660991-1","name":"660991 Выдача разрешения на ввод объекта в эксплуатацию","deploymentId":"42639263","date":"23.08.2013 05:44","key":"660991","version":"1"},{"id":"660991-2","name":"660991 Выдача разрешения на ввод объекта в эксплуатацию","deploymentId":"43596209","date":"10.09.2013 04:37","key":"660991","version":"2"}]}
     */
    public String getTemplateKey(String templateName) {
        String script = "var xmlhttp = new XMLHttpRequest();\n" +
                "xmlhttp.open('POST', '/jbpm-console-v2.0/console/definitions', false);\n" +
                "xmlhttp.setRequestHeader(\"Content-type\",\"application/x-www-form-urlencoded; charset=UTF-8\");\n" +
                "xmlhttp.send('start=0&limit=1000&showtemplate=All&name_from=" + templateName + "&name_to=&name_rb=&name_order=0');\n" +
                "return xmlhttp.responseText;";
        return getFilteresObject(script, Template.class, "key");
    }


    public <T extends AbstractBean> String getFilteresObject(String script, Class<T> clazz, String property) {
        try{
            Object response = driver.executeScript(script);

        List<T> list = listObjectsFromJSONArray((String) response, clazz);

        Method getter = ReflectUtils.getterMethod(property, clazz);
        String key = null;
        for (T t : list) {
            String result = (String)getter.invoke(t);
            if (key == null) {
                key = result;
            } else if (!key.equals(result)) {
                throw new SirException(this.driver, "Found different keys: '" + key + "' and '" + result + "'\nfor class: '" + clazz.getName() + "'\nlist:" + list);
            }
        }
        return key;
        } catch (Exception e) {
            throw new SirException(driver, "Error while trying to get '" + property + "' for '" + clazz.getName() + "'", e);
        }
    }

    public <T extends AbstractBean> List<T> getObjects(String script, Class<T> clazz) throws JSONException, IllegalAccessException, InstantiationException {
        return listObjectsFromJSONArray((String)driver.executeScript(script), clazz);
    }


    public <T extends AbstractBean> List<T> listObjectsFromJSONArray(String json, Class<T> clazz) throws JSONException, IllegalAccessException, InstantiationException {
        JSONObject jsonObject = new JSONObject(json);
        int total = jsonObject.getInt("total");
        List<T> result = new ArrayList<T>(total);
        JSONArray jsonArray = jsonObject.getJSONArray("data");
        if (jsonArray.length() != total) {
            log.warn("Wrong input number 'total' doesn't equal real array length.\n" + json);
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            T t = clazz.newInstance();
            t.init(jsonArray.getJSONObject(i));
            result.add(t);
        }
        return result;
    }

    /**
     * @throws PortalException если возникла ошибка
     */
    public void stopProcess(String key, String processId) {
        stopProcess(key + "." + processId);
    }

    public void stopProcess(Process process) {
        stopProcess(process.getProcessInstanceId());
    }
}

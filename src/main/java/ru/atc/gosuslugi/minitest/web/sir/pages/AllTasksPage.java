package ru.atc.gosuslugi.minitest.web.sir.pages;

import org.json.JSONException;
import org.openqa.selenium.WebElement;
import ru.atc.gosuslugi.minitest.jaxb.values.DateFilter;
import ru.atc.gosuslugi.minitest.jaxb.values.Filter;
import ru.atc.gosuslugi.minitest.web.sir.Page;
import ru.atc.gosuslugi.minitest.web.sir.SirDriver;
import ru.atc.gosuslugi.minitest.web.sir.js.Process;
import ru.atc.gosuslugi.minitest.web.sir.js.SirDriverJS;
import ru.atc.gosuslugi.minitest.util.ValueUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Страница "Все задачи"
 * Date: 14.10.13
 * Author: astafev
 */
public class AllTasksPage extends SirPageObject {
    public static final String PROCESS_ID = "Идeнтификатор процесса";
    public static final String TASK = "Задача";
    public static final String PROCESS_NAME = "Процесс";
    public static final String START_DATE = "Дата начала";
    public static final String REQUISITES = "Реквизиты";

    public static int limit = 100;

    static {
        pageId = "view-task";
        panelId = "alltaskpanel";
        url = "/jbpm-console-v2.0/console/task";
    }

    private AllTasksPage(SirDriver driver) {
        super(driver);
    }

    public AllTasksPage() {
    }
    public AllTasksPage(WebElement element, SirDriver sirDriver) {
        super(element);
        this.setDriver(sirDriver);
    }

    public static AllTasksPage goToAllTasksPage(SirDriver driver1) {
        return new AllTasksPage(driver1);
    }


    @Override
    public Page getPage() {
        return Page.ALL_TASKS;
    }

    @Override
    public String requestWithFilters(List filters) throws ParseException {
        StringBuilder req = new StringBuilder("start=0&limit=" + limit + "&showprocess=All");
        for (Object aFilter : filters) {
            if (aFilter instanceof Filter) {
                Filter filter = (Filter) aFilter;
                if (filter.getKey().equals(REQUISITES)) {
                    req.append("&info_from=").append(filter.getValue()).append("&info_to=&info_rb=&info_order=0");
                } else if (filter.getKey().equals(PROCESS_NAME)) {
                    req.append("&serviceName_from=").append(filter.getValue()).append("&serviceName_to=&serviceName_rb=&serviceName_order=0");
                } else if (filter.getKey().equals(TASK)) {
                    req.append("&name_from=").append(filter.getValue()).append("&name_to=&name_rb=&name_order=0");
                } else if (filter.getKey().equals(TASK)) {
                    req.append("&name_from=").append(filter.getValue()).append("&name_to=&name_rb=&name_order=0");
                } else if (filter.getKey().equals(PROCESS_ID)) {
                    req.append("&processInstanceId_from=").append(filter.getValue()).append("&processInstanceId_to=&processInstanceId_rb=&processInstanceId_order=0");
                } else throw new UnsupportedOperationException("Unknown filter key: " + ((Filter) aFilter).getKey());
            } else if (aFilter instanceof DateFilter) {

                DateFilter dateFilter = (DateFilter) aFilter;
                if (dateFilter.getKey().equals(START_DATE)) {
                    if (dateFilter.getStart() != null) {
                        Date start = ValueUtils.stringToDate(dateFilter.getStart());
                        req.append("&createDate_from=").append(ValueUtils.DATE_FORMAT.format(start));
                    }
                    if (dateFilter.getEnd() != null) {
                        Date start = ValueUtils.stringToDate(dateFilter.getStart());
                        req.append("&createDate_to=20.12.13").append(ValueUtils.DATE_FORMAT.format(start));
                    }
                    req.append("&createDate_rb=&createDate_order=1");
                } else throw new UnsupportedOperationException("Unknown filter key: " + ((Filter) aFilter).getKey());
            }
        }
        return req.toString();
    }


    public List<Process> listProcesses(List filters, SirDriverJS driverJS) throws IllegalAccessException, JSONException, InstantiationException, ParseException {
        return driverJS.getObjects(SirDriverJS.script(url, requestWithFilters(filters)), Process.class);
    }

}

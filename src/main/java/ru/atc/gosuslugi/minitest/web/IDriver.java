package ru.atc.gosuslugi.minitest.web;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;

import java.io.File;
import java.util.List;

/**
 * Date: 30.10.13
 * Author: astafev
 */
public interface IDriver extends WebDriver, TakesScreenshot, JavascriptExecutor, HasInputDevices {

    public String takeScreenshot();
    public void takeScreenshot(String fileName);
    public void takeScreenshot(File file);
    public WebDriver getDriver() ;

    public void setDriver(WebDriver driver);

    public List<WebElement> getDisplayedList(SearchContext searchContext, By by);

    public List<WebElement> getDisplayedList(SearchContext searchContext, By by, int listSize);

    public List<WebElement> getDisplayedList(By by);

    /**
     * @returns null либо 1-й отображаемый элемент
     * */
    public WebElement getDisplayed(SearchContext searchContext, By by);

    public WebElement getDisplayed(By by);

    public Keyboard getKeyboard();

    public Mouse getMouse();

    public void setDefaultImplicitWait();
}

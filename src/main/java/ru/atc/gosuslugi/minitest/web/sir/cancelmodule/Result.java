package ru.atc.gosuslugi.minitest.web.sir.cancelmodule;

import ru.atc.gosuslugi.minitest.web.sir.js.Process;

/**
 * Date: 11.10.13
 * Author: astafev
 */
public class Result {
    Process process;
    Exception exception;
    Status status;

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isSuccess() {
        if(status == Status.SUCCESS)
            return true;
        else return false;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    @Override
    public String toString() {
        if(exception == null)
            return status.toString();
        else return "Result{" +
                "exception=" + exception.getMessage() +
                ", status=" + status +
                '}';
    }
}

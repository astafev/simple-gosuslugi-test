package ru.atc.gosuslugi.minitest.web.sir.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.atc.gosuslugi.minitest.web.portal.OneOfElementPresent;
import ru.atc.gosuslugi.minitest.web.sir.SirDriver;

import java.util.Date;
import java.util.List;

/**
 * Date: 14.10.13
 * Author: astafev
 */
public class FilterFrame extends AbstractSirFrame {
    public FilterFrame(SirDriver driver1) {
        this(driver1, 10);
    }

    public FilterFrame(SirDriver driver1, int waitPeriod) {
        super(new WebDriverWait(driver1, waitPeriod).until(new OneOfElementPresent(By.cssSelector(".x-window.x-window-noborder.x-window-plain.x-resizable-pinned"))));
        this.setDriver(driver1);
    }

    public void setFilter(String text) {
        WebElement inputField = findElement(By.tagName("input"));
        inputField.clear();
        inputField.sendKeys(text);
        driver.getDisplayed(this, By.xpath("//button[text()='Применить']")).click();
    }
    public void setDateFilter(Date start, Date end) {
        List<WebElement> inputFields = findElements(By.cssSelector("input.x-form-field"));
        if (start != null) {
            inputFields.get(0).clear();
            inputFields.get(0).sendKeys(SirDriver.dd_MM_yy.format(start));
        }
        if (end != null) {
            inputFields.get(1).clear();
            inputFields.get(1).sendKeys(SirDriver.dd_MM_yy.format(end));
        }
        findElement(By.xpath("//button[text()='Применить']")).click();
    }
}

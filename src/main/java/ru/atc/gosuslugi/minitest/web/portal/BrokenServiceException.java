package ru.atc.gosuslugi.minitest.web.portal;

import ru.atc.gosuslugi.minitest.jaxb.values.Service;

/**
 * Created with IntelliJ IDEA.
 * User: astaf_000
 * Date: 20.06.13
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
public class BrokenServiceException extends Exception{
    public Service service;

    public BrokenServiceException() {
        super();
    }

    public BrokenServiceException(String message) {
        super(message);
    }

    public BrokenServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public BrokenServiceException(Throwable cause) {
        super(cause);
    }

}

package ru.atc.gosuslugi.minitest.web.sir;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.jaxb.values.DateFilter;
import ru.atc.gosuslugi.minitest.jaxb.values.Filter;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;
import ru.atc.gosuslugi.minitest.result.Result;
import ru.atc.gosuslugi.minitest.result.ResultUtils;
import ru.atc.gosuslugi.minitest.result.Status;
import ru.atc.gosuslugi.minitest.web.sir.js.*;
import ru.atc.gosuslugi.minitest.web.sir.js.Process;
import ru.atc.gosuslugi.minitest.web.sir.pages.AllTasksPage;
import ru.atc.gosuslugi.minitest.web.sir.pages.SirPageObject;
import ru.atc.gosuslugi.minitest.util.ValueUtils;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * Date: 13.12.13
 * Author: astafev
 */
public class SirCheckCallable implements Callable<Result> {
    final Result result;
    final SirDriver sir;
    public static final Logger log = LoggerFactory.getLogger(SirCheckCallable.class);

    public SirCheckCallable(Result result, SirDriver sir) {
        this.result = result;
        this.sir = sir;
    }

    @Override
    public Result call() {
        /*if (sir.getPage() == Page.JUST_IN) {
            try {
                sir.start();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                setResult(e, Status.UNKNOWN_ERROR);
                return result;
            }
        }*/
//        Service service = result.getService();
//        String sir_type = System.getProperty("ru.atc.SIR_TYPE", "PROD");
        return checkWithJS();

    }


    private Result checkWithJS() {
        SirPageObject page = new AllTasksPage();

        try {
            Service service = result.getService();
            String orderId = result.getOrderId();

            List filters = new LinkedList();
            if(service.getSirConfig()!=null) {
                filters.addAll(service.getSirConfig().getFilters());
                filters.addAll(service.getSirConfig().getDateFilters());
            }
            //to_Think предусмотреть возможность тестить без orderId в реквизитах
            filters.add(new Filter(AllTasksPage.REQUISITES, orderId));
            if(filters.size()==1) {
                DateFilter dateFilter = new DateFilter();
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(new Date());
                calendar.add(Calendar.DAY_OF_MONTH, -1);
                dateFilter.setKey(AllTasksPage.START_DATE);
                dateFilter.setStart(ValueUtils.dateToString(calendar.getTime()));
                filters.add(dateFilter);
            }

            List<Process> processes = ((AllTasksPage)page).listProcesses(filters, SirDriverJS.getOne(sir));
            if(processes.size()>1 || processes.size() == 0)
            {
                StringBuilder sb = new StringBuilder();
                for(Process proc:processes) {
                    sb.append(proc.getProcessInstanceId()).append('\n');
                }
                result.setAdditionalInfo(sb.toString());
                log.warn("Error! For application " + orderId + " found " + processes.size() + " processes in sir. \n" + processes);
                setResult(new IllegalStateException("Должен быть один процесс, а найдено " + processes.size() + "\n" + processes.toString()), Status.ERROR_CHECKING_IN_SIR);
            } else {
                result.setAdditionalInfo(processes.get(0).getProcessInstanceId());
                log.info("Application " + orderId + " in sir: " + processes.get(0).getProcessInstanceId());
                setResult(null, Status.SENT);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            setResult(e, Status.ERROR_CANCELING_IN_SIR);
        }
        return result;
    }

    /**
     * <p>учитывая что нам больше не надо ходить на страницу экземпляров процессов:</p>
     * <ul>
     * <p/>
     * <p/>
     * <li>Если нет фильтров - ставим фильтр на реквизиты - caseNumber ?и дату - со вчера?</li>
     * <ol>
     * <li>Ставим фильтр Реквизиты=orderId</li>
     * <li>Если остался один - берем processId<br/>
     * Если нет - смотрим у всех caseNumber...</li>
     * <li>Если остался один - убиваем</li>
     * </ol>
     * <li>Если есть - ставим указанные фильтры</li>
     * <ol>
     * <li>Ставим фильтр Реквизиты=orderId</li>
     * <li>Если остался один - берем processId<br/>
     * Если нет - смотрим у всех caseNumber...</li>
     * <li>Если остался один - убиваем</li>
     * </ol>
     * </ul>
     * После того как взяли processId, если ключ не введен в параметрах, берем наимение процесса из параметров, идем в щаблоны. Там стави фильтр и берем ключ
     */
    private Result checkOnMainPage() {
        SirPageObject page;
        if (sir.getPage() != Page.ALL_TASKS) {
            try {
                log.debug("Going to main page");
                page = sir.goToAllTasks();
            } catch (Exception e) {
                log.error("", e);
                setResult(e, Status.UNKNOWN_ERROR);
                return result;
            }
        } else {
            page = (AllTasksPage) sir.getCurrentPage();
        }

        try {
            Service service = result.getService();
            String orderId = result.getOrderId();
            page.disableAllFilters();
            if(service.getSirConfig()!=null) {
                List<Filter> filters = service.getSirConfig().getFilters();
                if(filters!=null && filters.size()>0) {
                    for(Filter filter:filters)
                        sir.setFilter(filter);

                }
                List<DateFilter> dateFilters = service.getSirConfig().getDateFilters();
                if(dateFilters!=null && dateFilters.size()>0) {
                    for(DateFilter filter:dateFilters)
                        sir.setDateFilter(filter);

                }
            }
            /*List filters = new LinkedList();
            if(service.getSirConfig()!=null) {
                filters.addAll(service.getSirConfig().getFilters());
                filters.addAll(service.getSirConfig().getDateFilters());
            }
            //to_Think предусмотреть возможность тестить без orderId в реквизитах
            filters.add(new Filter(AllTasksPage.REQUISITES, orderId));
*/
            page.setFilter("Реквизиты", orderId);
/*
            List<Process> processes = ((AllTasksPage)page).listProcesses(filters, SirDriverJS.getOne(sir));
            if(processes.size()>1 || processes.size() == 0)
            {
                StringBuilder sb = new StringBuilder();
                for(Process proc:processes) {
                    sb.append(proc.getProcessInstanceId()).append('\n');
                }
                result.setAdditionalInfo(sb.toString());
                log.warn("Error! For application " + orderId + " found " + processes.size() + " processes in sir. \n" + processes);
                setResult(new IllegalStateException("Должен быть один процесс, а найдено " + processes.size() + "\n" + processes.toString()), Status.ERROR_CHECKING_IN_SIR);
            } else {
                result.setAdditionalInfo(processes.get(0).getProcessInstanceId());
                log.info("Application " + orderId + " in sir: " + processes.get(0).getProcessInstanceId());
                setResult(null, Status.SENT);
            }*/
            List<Row> rows = page.getDisplayedRows(10);
            if(rows.size()>1 || rows.size() == 0)
            {
                StringBuilder sb = new StringBuilder();
                for(Row row:rows) {
                    sb.append(row.getValue(AllTasksPage.PROCESS_ID)).append('\n');
                }
                result.setAdditionalInfo(sb.toString());
                setResult(new IllegalStateException("Должен быть один процесс, а найдено " + rows.size() + "\n" + rows.toString()), Status.ERROR_CHECKING_IN_SIR);
            } else {
                result.setAdditionalInfo(rows.get(0).getValue(AllTasksPage.PROCESS_ID));
                setResult(null, Status.SENT);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            setResult(e, Status.ERROR_CANCELING_IN_SIR);
        }
        return result;
    }

    /**
     * Вырубаем старые, ставим новые
     */
    private void takeCareOfFilters(Result result) throws InterruptedException, ParseException {
        Service service = result.getService();
        String orderId = result.getOrderId();
        sir.disableAllFilters();
        if (service.getSirConfig() == null ||
                (
                        (service.getSirConfig().getDateFilters() == null || service.getSirConfig().getDateFilters().isEmpty()) &&
                                (service.getSirConfig().getFilters() == null || service.getSirConfig().getFilters().isEmpty())
                )
                ) {
            sir.setFilter(AllTasksPage.REQUISITES, orderId);
        } else {
            List<Filter> filters = service.getSirConfig().getFilters();
            if (filters != null && filters.size() > 0) {
                for (Filter filter : filters) {
                    if (filter.getValue().equalsIgnoreCase("${caseNumber}"))
                        filter.setValue(orderId);
                    sir.setFilter(filter);
                }
            }
            List<DateFilter> dateFilters = service.getSirConfig().getDateFilters();
            if (dateFilters != null && dateFilters.size() > 0) {
                for (DateFilter filter : dateFilters)
                    sir.setDateFilter(filter);
            }
        }
    }


    public void setResult(Exception e, Status status) {
        ResultUtils.setResult(e, status, result, sir);
        try {
            SirRunner.sirDriversQueue.put(sir);
        } catch (InterruptedException e1) {
            log.error("Impossible error! You can't see this message.", e1);
            ResultUtils.setResult(e, Status.UNKNOWN_ERROR, result, sir);
            result.setErrorMessage(e1.getMessage());
        }
//        RunnerV2.getInstance().freeDrivers.add(portal);
    }
}

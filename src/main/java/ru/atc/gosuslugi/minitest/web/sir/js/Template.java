package ru.atc.gosuslugi.minitest.web.sir.js;

import java.util.Map;

/**
 * Date: 15.10.13
 * Author: astafev
 */
public class Template extends AbstractBean {
    private String id;
    private String name;
    private String deploymentId;
    private String date;
    private String key;
    private String version;

    public Template() {

    }
    public Template(Map<String, String> map) throws NoSuchFieldException {
        super(map);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}

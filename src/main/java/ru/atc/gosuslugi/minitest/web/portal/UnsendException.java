package ru.atc.gosuslugi.minitest.web.portal;

import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.Service;

import java.util.Map;

public class UnsendException extends PortalException {
//    public String serviceid;
    public Map<String, String> values;
    public Service service;


    public UnsendException(PortalDriver driver, String string) {
        super(driver, string);
    }
    public UnsendException(PortalDriver driver, String string, Throwable cause) {
        super(driver, string, cause);
    }

}

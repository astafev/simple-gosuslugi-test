package ru.atc.gosuslugi.minitest.web.sir.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.Driver;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.DateFilter;
import ru.atc.gosuslugi.minitest.jaxb.values.Filter;
import ru.atc.gosuslugi.minitest.web.sir.*;
import ru.atc.gosuslugi.minitest.web.sir.Row;
import ru.atc.gosuslugi.minitest.web.sir.frames.FilterFrame;

import java.text.ParseException;
import java.util.*;

/**
 * Date: 14.10.13
 * Author: astafev
 */
public abstract class SirPageObject implements WebElement{
    protected WebElement panel;

    protected SirDriver driver;

    public static String panelId = "content-panel";
    public static String pageId = null;
    public static String url = null;
    public static Logger log = LoggerFactory.getLogger(SirPageObject.class);

    protected SirPageObject(SirDriver driver) {
        this(driver.findElement(By.id(panelId)));
        this.driver = driver;
        driver.findElement(By.id(pageId)).click();
        new WebDriverWait(driver, 20).until(ExpectedConditions.not(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[contains(text(), 'Пожалуйста подождите')]"))));
    }

    public SirPageObject(WebElement panel) {
        setPanel(panel);
    }

    protected SirPageObject() {
    }

    public WebElement getPanel() {
        return panel;
    }

    public void setPanel(WebElement panel) {
        this.panel = panel;
    }

    public abstract Page getPage();

    @Override
    public void click() {
        panel.click();
    }

    @Override
    public void submit() {
        panel.submit();
    }

    @Override
    public void sendKeys(CharSequence... keysToSend) {
        panel.sendKeys(keysToSend);
    }

    @Override
    public void clear() {
        panel.clear();
    }

    @Override
    public String getTagName() {
        return panel.getTagName();
    }

    @Override
    public String getAttribute(String name) {
        return panel.getAttribute(name);
    }

    @Override
    public boolean isSelected() {
        return panel.isSelected();
    }

    @Override
    public boolean isEnabled() {
        return panel.isEnabled();
    }

    @Override
    public String getText() {
        return panel.getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return panel.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return panel.findElement(by);
    }

    @Override
    public boolean isDisplayed() {
        return panel.isDisplayed();
    }

    @Override
    public Point getLocation() {
        return panel.getLocation();
    }

    @Override
    public Dimension getSize() {
        return panel.getSize();
    }

    @Override
    public String getCssValue(String propertyName) {
        return panel.getCssValue(propertyName);
    }

    public SirDriver getDriver() {
        return driver;
    }

    public void setDriver(SirDriver driver) {
        this.driver = driver;
    }

    public void setFilter(String key, String searchString) {
        log.debug(String.format("setting filter %s:%s", key, searchString));
        long time = System.currentTimeMillis();
        if (System.getProperty("webdriver") != null && System.getProperty("webdriver").equals("chrome")) {
            ((JavascriptExecutor) driver).executeScript("var frame = document.getElementsByClassName('ui-window-content');\n" +
                    "if(frame.length == 0) {\n" +
                    "    var element = document.getElementById('processPanel').getElementsByClassName('x-tree-node-anchor')[0];\n" +
                    "} else var element = frame[0].contentWindow.document.getElementById('processPanel').getElementsByClassName('x-tree-node-anchor')[0];\n" +
                    "if (document.createEvent) {\n" +
                    "    var ev = document.createEvent('HTMLEvents');\n" +
                    "    ev.initEvent('contextmenu', true, false);\n" +
                    "    element.dispatchEvent(ev);\n" +
                    "} else {\n" +
                    "    element.fireEvent('oncontextmenu');\n" +
                    "}");
        } else
            rightClickOnLink("Фильтр");

//        Thread.sleep(1000);
        findInMenuItemText(key).click();

        FilterFrame block = new FilterFrame(driver);
        block.setFilter(searchString);
        log.debug(System.currentTimeMillis() - time + " took to set filter");
        new WebDriverWait(driver, 15).until(ExpectedConditions.not(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[contains(text(), 'Пожалуйста подождите')]"))));
    }

    public void setFilter(Filter filter)  {
        setFilter(filter.getKey(), filter.getValue());
    }

    public void disableFilter(String key) {
        WebElement li = findElement(By.partialLinkText(key + " "));
        /*try {
            li = getCurrentPage().findElement(By.partialLinkText(key + " содержит"));
        } catch (NoSuchElementException e) {
            //Даты с
            li = getCurrentPage().findElement(By.partialLinkText(key + " "));
        }
        */
        Actions builder = new Actions(driver);
        Action rightClick = builder.contextClick(li).build();
        rightClick.perform();
        findInMenuItemText("Выключить").click();
    }

    public void disableAllFilters() {
        log.debug("disabling filters");
        long time = System.currentTimeMillis();
        try {
            WebElement parent = findElement(By.linkText("Фильтр")).findElement(By.xpath("../.."));

            if (!parent.getAttribute("innerHTML").contains("<li"))
                return;
            List<WebElement> elements = parent.findElements(By.tagName("li"));
            for (WebElement element : elements) {
                Actions builder = new Actions(driver);
                Action rightClick = builder.contextClick(element.findElement(By.tagName("a"))).build();
                rightClick.perform();
                findInMenuItemText("Выключить").click();
            }
        } finally {
            log.debug("disabling filters took " + (System.currentTimeMillis() - time));
        }

    }

    /**
     * Ищет в меню, всплюывающем при клике правой кнопкой мыши
     */
    private WebElement findInMenuItemText(String key) {
        List<WebElement> keysElements = driver.getDisplayedList(By.className("x-menu-item-text"));
        Map<String, WebElement> map = new LinkedHashMap<String, WebElement>();
        for (WebElement element : keysElements) {
            map.put(element.getText(), element);
        }
        for (String s : map.keySet()) {
            if (s.equals(key)) {
                return map.get(s);
            }
        }
        throw new PortalException(driver, "didn't found key '" + key + "' in filter. Available keys: " + map.keySet());
    }

    public void setDateFilter(DateFilter dateFilter) throws ParseException {
        Date startDate = null;
        Date endDate = null;
        if (dateFilter.getStart() != null)
            startDate = Driver.stringToDate(dateFilter.getStart());
        if (dateFilter.getEnd() != null)
            startDate = Driver.stringToDate(dateFilter.getEnd());
        setDateFilter(dateFilter.getKey(), startDate, endDate);
    }

    public void setDateFilter(String key, Date start, Date end) {
        rightClickOnLink(null);
        findInMenuItemText(key).click();

        FilterFrame block = new FilterFrame(driver);
        block.setDateFilter(start, end);
    }

    private void rightClickOnLink(String partialLinkText) {
        if (partialLinkText == null || partialLinkText.isEmpty()) {
            partialLinkText = "Фильтр";
        }
        WebElement filterLabel = panel.findElement(By.partialLinkText(partialLinkText));
        Actions builder = new Actions(driver);
        Action rightClick = builder.contextClick(filterLabel).build();
        rightClick.perform();
    }

    /**
     * Возвращает процессы видимые.
     *
     * @return ключ карты - карта с описанием процесса (типа Наименование:...; Реквизиты:...), значение - строчка процесса
     */
    public List<Row> getDisplayedRows(int size) {
        long time = System.currentTimeMillis();
        WebElement thead = driver.getDisplayed(By.cssSelector("tr.x-grid3-hd-row"));
        List<WebElement> ths = thead.findElements(By.cssSelector("td.x-grid3-hd"));
        String[] keys = new String[ths.size()];
        for (int i = 0; i < ths.size(); i++) {
            if(ths.get(i).isDisplayed())
                keys[i] = ths.get(i).getText();
            else {
                String innerHTML = ths.get(i).getAttribute("innerHTML");
                keys[i]=innerHTML.substring(innerHTML.indexOf("</a>") + 4, innerHTML.indexOf("<img")).trim();
            }
        }
        log.debug(System.currentTimeMillis() - time + "ms took to make a map for process (from thead)");

        List<WebElement> processes = driver.getDisplayedList(this, By.cssSelector("div.x-grid3-row"), size);
        log.debug(System.currentTimeMillis() - time + "ms took to find all processes. Found " + processes.size());
        List<Row> result = new LinkedList<Row>();
//        Map<Map<String, String>, WebElement> result = new LinkedHashMap<Map<String, String>, WebElement>(processes.size());
        for (int i = 0; i < processes.size(); i++) {
            List<WebElement> tds = processes.get(i).findElements(By.cssSelector("td.x-grid3-col"));
            Map<String, String> processMap = new LinkedHashMap<String, String>();
            for (int j = 0; j < tds.size(); j++) {
                if(tds.get(j).isDisplayed())
                    processMap.put(keys[j], tds.get(j).getText());
                else {
                    String value = tds.get(j).findElement(By.tagName("div")).getAttribute("innerHTML");
                    processMap.put(keys[j], value);
                }
            }
            Row row = new Row(processes.get(i), processMap);
            result.add(row);
        }
        log.debug(System.currentTimeMillis() - time + "ms took to collect info about all processes.");
        log.debug("Found processes:\n" + result);
        return result;
    }

    public void clickOnTollbarButton(String name) {
        //to_test может можно найти более изящный способ
        findElement(By.xpath("//button/b[text()='" +name+ "']")).click();
    }

    public String requestWithFilters(List<Filter> filters) throws ParseException {
        throw new UnsupportedOperationException("Should be overrided!");
    }

}

package ru.atc.gosuslugi.minitest.web.sir;

import org.openqa.selenium.WebElement;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Date: 27.09.13
 * Author: astafev
 */
public class Row {
    Map<String, String> processMap = new LinkedHashMap<String, String>();
//    String id;
//    String key;
    WebElement row;

    public Row(WebElement row, Map<String, String> processMap) {
        this.processMap = processMap;
        this.row = row;
    }

    public Map<String, String> getProcessMap() {
        return processMap;
    }

    public void setProcessMap(Map<String, String> processMap) {
        this.processMap = processMap;
    }

    public WebElement getRow() {
        return row;
    }

    public void setRow(WebElement row) {
        this.row = row;
    }

    public void click() {
        row.click();
    }

    @Override
    public String toString() {
        return "Row{" +
                processMap +
                '}';
    }

    public String getValue(String key) {
        return processMap.get(key);
    }

}

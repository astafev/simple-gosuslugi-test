var frame = document.getElementsByClassName('ui-window-content');
if(frame.length == 0) {
    var element = document.getElementById('processPanel').getElementsByClassName('x-tree-node-anchor')[0];
} else var element = frame[0].contentWindow.document.getElementById('processPanel').getElementsByClassName('x-tree-node-anchor')[0];
if (document.createEvent) {
    var ev = document.createEvent('HTMLEvents');
    ev.initEvent('contextmenu', true, false);
    element.dispatchEvent(ev);
} else {
    element.fireEvent('oncontextmenu');
}
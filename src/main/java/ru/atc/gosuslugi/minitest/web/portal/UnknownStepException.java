package ru.atc.gosuslugi.minitest.web.portal;

import org.openqa.selenium.NoSuchElementException;

/**
 * Date: 28.10.13
 * Author: astafev
 */
public class UnknownStepException extends NoSuchElementException {
    public UnknownStepException(String string) {
//        super(web, string);
        super(string);
    }

    public UnknownStepException(String string, Throwable cause) {
//        super(web, string, cause);
        super(string, cause);
    }
}

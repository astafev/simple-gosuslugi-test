package ru.atc.gosuslugi.minitest.web.sir.pages;

import org.openqa.selenium.WebElement;
import ru.atc.gosuslugi.minitest.web.sir.Page;

/**
 * Date: 14.10.13
 * Author: astafev
 */
public class StartPage extends SirPageObject {
    public StartPage() {
        super((WebElement)null);
    }

    @Override
    public Page getPage() {
        return Page.JUST_IN;
    }
}

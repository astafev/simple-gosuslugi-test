package ru.atc.gosuslugi.minitest.web.portal;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.List;

/**
 * Date: 18.09.13
 * Author: astafev
 */
class ElementIsInvisible implements ExpectedCondition<Boolean> {
    By by;
    SearchContext searchContext;

    public ElementIsInvisible(String id, WebDriver driver) {
        this(driver.findElement(By.id(id)));
    }

    public ElementIsInvisible(SearchContext searchContext) {
        this(By.className("PGU-LoadingStatus"), searchContext);
    }

    public ElementIsInvisible(By by, SearchContext searchContext) {
        this.searchContext = searchContext;
        this.by = by;
    }

    @Override
    public Boolean apply(WebDriver driver) {
        try {
            if (searchContext.findElement(by).isDisplayed())
                return false;
            else
                return true;
        } catch (NoSuchElementException e) {
            return true;
        }
    }
}

class OneOfElementsPresentInDic extends OneOfElementPresent {

    public OneOfElementsPresentInDic(By by) {
        super(by);
    }

    public OneOfElementsPresentInDic(By by, SearchContext context) {
        super(by, context);
    }

    public String checkDicError() {
        try {
            //проверем, может там уже написано что ниче не найдено
            List<WebElement> elem = context.findElements(By.cssSelector("div.linesLookup span.no_data_msg"));
            for (WebElement e : elem) {
                if (e.isDisplayed())
                    return e.getText();
            }
            return null;
        } catch (NoSuchElementException e1) {
            return null;
        }
    }

    @Override
    public WebElement apply(WebDriver driver) {
        if (context == null) {
            context = driver;
        }
        List<WebElement> elements = context.findElements(by);
        WebElement element = null;
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).isDisplayed()) {
                Point p = elements.get(i).getLocation();
                if (p.getX() > 0 && p.getY() > 0)
                    element = elements.get(i);
            }
        }
        if (element == null) {
            String dicError = checkDicError();
            if (dicError != null)
                throw new RuntimeException(dicError);
        }
        return element;
    }
}

class OneOfElementsPresentsByChain implements ExpectedCondition<WebElement> {
    SearchContext context = null;
    By[] byChain;
    boolean checkVisibility = false;

    public OneOfElementsPresentsByChain(By... bys) {
        this(null, false, bys);
    }

    public OneOfElementsPresentsByChain(SearchContext context, By... bys) {
        this(context, false, bys);
    }
    public OneOfElementsPresentsByChain(SearchContext context, boolean checkvisibility, By... bys) {
        this.byChain = bys;
        this.context = context;
        this.checkVisibility = checkvisibility;
    }

    public static WebElement oneOfElementPresent(SearchContext searchContext, boolean checkVisibility, By... bys) {
//        SearchContext searchContext1 = searchContext;
        for(By by: bys) {
            if(checkVisibility) {
                searchContext = getDisplayed(searchContext.findElements(by));
                if(searchContext == null)
                    return null;
            } else searchContext = searchContext.findElement(by);
        }
        return (WebElement)searchContext;
    }

    public static WebElement getDisplayed(List<WebElement> elements) {
        for(WebElement element:elements) {
            if(element.isDisplayed())
                return element;
        }
        return null;
    }

    @Override
    public WebElement apply(WebDriver driver) {
        if (context == null) {
            context = driver;
        }
        try{
            return OneOfElementsPresentsByChain.oneOfElementPresent(context, checkVisibility, byChain);
        } catch (NoSuchElementException e) {
            return null;
        } catch (StaleElementReferenceException e) {
            return null;
        }
    }
}

class OneOfElementsVisible implements ExpectedCondition<WebElement> {
    By by;
    SearchContext searchContext;

    public OneOfElementsVisible(SearchContext searchContext, By by) {
        this.by = by;
        this.searchContext = searchContext;
    }

    public OneOfElementsVisible(By by) {
        this(null, by);
    }

    @Override
    public WebElement apply(WebDriver driver) {
        if(searchContext == null) {
            searchContext = driver;
        }
        try {
            return searchContext.findElement(by);
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
package ru.atc.gosuslugi.minitest.web.portal;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.List;

/**
 * Виден ли один из элементов*/
public class OneOfElementPresent implements ExpectedCondition<WebElement> {
    By by;
    SearchContext context = null;

    By[] byChain;

    public OneOfElementPresent(By by) {
        this.by = by;
    }

    public OneOfElementPresent(By... bys) {
        this.byChain = bys;
    }

    public OneOfElementPresent(By by, SearchContext context) {
        this(by);
        this.context = context;
    }

    public static WebElement oneOfElementPresent(SearchContext searchContext, By by) {
        List<WebElement> elements = searchContext.findElements(by);
        WebElement element = null;
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).isDisplayed()) {
                Point p = elements.get(i).getLocation();
                if (p.getX() > 0 && p.getY() > 0)
                    element = elements.get(i);
            }
        }
        return element;
    }

    @Override
    public WebElement apply(WebDriver driver) {
        if (context == null) {
            context = driver;
        }
        return OneOfElementPresent.oneOfElementPresent(context, by);
    }
}

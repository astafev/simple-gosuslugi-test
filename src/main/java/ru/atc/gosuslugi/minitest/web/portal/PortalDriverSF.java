package ru.atc.gosuslugi.minitest.web.portal;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.atc.gosuslugi.minitest.web.PortalException;
import ru.atc.gosuslugi.minitest.jaxb.values.ComplexValue;
import ru.atc.gosuslugi.minitest.jaxb.values.Value;

import static ru.atc.gosuslugi.minitest.util.CommonUtil.isEmpty;

import ru.atc.gosuslugi.minitest.util.ValueUtils;

import java.io.*;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Date: 13.09.13
 * Author: astafev
 */
public class PortalDriverSF extends PortalDriver {
    PortalDriver portalDriver;

    public PortalDriverSF(PortalDriver portalDriver) {
        super(portalDriver.getDriver());
        this.portalDriver = portalDriver;
    }

    public PortalDriverSF(WebDriver driver) {
        super(driver);
    }

    @Override
    public void putValueInDic(Value value) {
        WebElement div = findElement(By.id(value.getName()));
        div.findElement(By.className("PGU-ComboBoxFace")).click();

        new WebDriverWait(this, 30).until(new ElementIsInvisible(div));
        List<WebElement> elementsValues = findElement(By.id(value.getName())).findElements(By.className("PGU-ComboBoxOption"));
        if (value.getValue() != null && !value.getValue().isEmpty()) {
            for (WebElement element : elementsValues) {
                if (element.getText().trim().equalsIgnoreCase(value.getValue().trim())) {
                    element.click();
                    return;
                }
            }
            StringBuilder error = new StringBuilder();
            error.append("Не нашел значение '").append(value.getValue()).append("'\nДоступные значения:");
            for (WebElement element : elementsValues) {
                error.append(element.getText()).append("\n");
            }
            throw new PortalException(this, error.toString());
        } else if (elementsValues.size() >= 2)
            elementsValues.get(2).click();
        else throw new PortalException(this, "Нет вариантов");
    }

    @Override
    public void putValue(Value value) {
        log.debug("putting SF value " + value);
        final String type = value.getType() == null ? "" : value.getType();
        try {
            switch (InputType.valueOf(type)) {
                case fieldDropDown:
                case dic:
                case hierarchyDic:
                    putValueInDic(value);
                    break;
                case date:
                    List<WebElement> elements = findElements(By.id(value.getName() + "_picker"));
                    for (WebElement elem : elements) {
                        if (elem.getTagName().equals("input")) {
                            elem.clear();
                            try {
                                elem.sendKeys(dateValue(value));
                            } catch (ParseException e) {
                                throw new PortalException(this, "Error while parsing value\n" + value, e);
                            }
                            return;
                        }
                    }
                    throw new PortalException(this, "Unable to find input with id = '" + value.getName() + "'.");
                case kladr:
                    putValueInKLADR(value);
                    break;
                case fias:
                    putValueInFIAS(value);
                    break;
                default:
                    super.putValue(value);
            }
        } catch (IllegalArgumentException e) {
            putValueInText(value);
        }


    }

    @Override
    public void putValueInText(Value value) {
        List<WebElement> elements = findElements(By.id(value.getName()));
        for (WebElement elem : elements) {
            String tagName = elem.getTagName();
            if (tagName.equals("input") || tagName.equals("textarea")) {
//                    elem.click();
                elem.clear();
                elem.sendKeys(value.getValue());
                return;
            }
        }
        throw new PortalException(this, "Unable to find input with id = '" + value.getName() + "'.");
    }

    @Override
    public void startNewDeclaration() throws BrokenServiceException {
        String url = driver.getCurrentUrl();
        if (driver.getTitle().contains("Информация об услуге")) {
            try {
                try {
                    (new WebDriverWait(driver, 20))
                            .until(ExpectedConditions.presenceOfElementLocated(By.linkText("Получить услугу"))).click();
                } catch (TimeoutException e1) {
                    //Это делает более стабильными тесты, зато проканываем ошибку, возможно стоит сделать опции запуска
                    driver.navigate().refresh();
                    (new WebDriverWait(driver, 20))
                            .until(ExpectedConditions.presenceOfElementLocated(By.linkText("Получить услугу"))).click();
                }
                try {
                    (new WebDriverWait(driver, 20))
                            .until(new WaitIfClockIsDisplayed());
                    getStepNum();
                    return;
                } catch (NoSuchElementException e) {
                    new WebDriverWait(driver, 20).
                            until(ExpectedConditions.visibilityOfElementLocated(By.className("draft-new-button"))).click();
                    (new WebDriverWait(driver, 20)).until(new WaitIfClockIsDisplayed());
                    return;
                }
            } catch (TimeoutException e) {
//                log.error(e.getMessage() + "\n" + web.getCurrentUrl(), e);
                throw new PortalException(this, e.getMessage() + "\n" + driver.getCurrentUrl(), e);
            }
        }
        try {
            new WebDriverWait(driver, 10).
                    until(ExpectedConditions.presenceOfElementLocated(By.className("draft-new-button"))).click();
            (new WebDriverWait(driver, 20)).until(new WaitIfClockIsDisplayed());
            return;
        } catch (NoSuchElementException e) {
            //do nothing
        }
        throw new BrokenServiceException("Wrong title! '" + driver.getTitle() + "' on: " + url);
    }

    @Override
    public void goToNextStep(int initStep, int nextStep) throws ValidationException {
        if (nextStep == 0) {
            nextStep = initStep + 1;
        }
        driver.findElement(By.id("__nextStep")).click();
        try {
            Thread.sleep(1000);
            new WebDriverWait(this, 10).until(ExpectedConditions.invisibilityOfElementLocated(By.id("shadowglass-clock")));
        } catch (InterruptedException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
        int step = -1;
        try {
            step = getStepNum();
        } catch (NoSuchElementException e) {
            try {
                sendApplication();
            } catch (UnsendException e1) {
                throw new RuntimeException(e1);
            }
        }
        if (nextStep != step || nextStep == initStep) {
            Map<String, String> mapWithErrors = getErrors();
            if (nextStep != step || mapWithErrors.size() != 0) {
                ValidationException exception = new ValidationException(this, "came instead of step " + nextStep + " to step " + step +
                        "\n" + mapWithErrors.toString());
                exception.errorLabels = mapWithErrors;
                throw exception;
            } else {
                //everything fine
                return;
            }
        }
    }

    @Override
    public int getStepNum() {
        try {
            List<WebElement> steps = driver.findElement(By.cssSelector("table.PGU-FormStepMenu")).findElements(By.tagName("td"));
            for (int i = 0; i < steps.size(); i++) {
                if (steps.get(i).getAttribute("class").contains("PGU-FormStepMenuItem-current"))
                    return i + 1;
            }
            log.warn("Couldn't get page number. Unable to find element css=\"table.stepMenu.stepMenuWidth td.active\"");
        } catch (NoSuchElementException e) {
            throw new UnknownStepException(e.getMessage(), e);
        }
        throw new UnknownStepException("Unable to find element css=\"table.stepMenu.stepMenuWidth td.active\"");
    }

    @Override
    public Map<String, String> getErrors() {
        Map<String, String> mapWithErrors = new LinkedHashMap<String, String>();
        List<WebElement> errorElements = driver.findElements(By.cssSelector("div.PGU-FieldError"));
        for (WebElement error : errorElements) {
            if (error.isDisplayed() && error.getText() != null && !error.getText().isEmpty()) {
                WebElement parent = error.findElement(By.xpath("../.."));
                String id = parent.getAttribute("id");
                String label = "";
                for (WebElement element : parent.findElements(By.tagName("label"))) {
                    label = element.getText();
                    break;
                }
                mapWithErrors.put(label + "(" + id + ")", error.getText());
            }
        }
        return mapWithErrors;
    }

    public void putValueInKLADR(Value value) {
        ComplexValue complexValue;
        if (value.getClass() == Value.class ||
                ((ComplexValue) value).getValue() == null) {
            complexValue = ValueUtils.parseKladr(value);
        } else {
            complexValue = (ComplexValue) value;
        }
        if (isEmpty(complexValue.getRegion()) || isEmpty(complexValue.getPost_index())) {
            throw new IllegalArgumentException("Region can't be empty!");
        }

        WebElement row = findElement(By.id(complexValue.getName()));
        //очищаем
        List<WebElement> list1 = row.findElements(By.className("clear-address"));
        if (list1.size() > 1)
            list1.get(0).click();
        else {
            //адрес не заполнен, do nothing
        }
        row.findElement(By.className("change-address")).click();
        //Очень странная система: 1-й div - для заполнения из справочника, 2-й для заполнения вручную... Как я понял...
//        List<WebElement> forms = findElements(By.cssSelector("div.kladrCont"));
        WebElement formDiv = getDisplayed(By.cssSelector("div.kladrCont"));
//        try {
//            new WebDriverWait(this, 2).until(ExpectedConditions.visibilityOf(forms.get(0)));
//        } catch (TimeoutException e) {
//            throw new PortalException(this, "Probably, error in algorithm. First form is not displayed", e);
//        }
        formDiv.findElement(By.cssSelector("a.kladrButtonFormHand")).click();
        formDiv = getDisplayed(By.cssSelector("div.kladrCont"));
        WebElement form = formDiv.findElement(By.cssSelector("div.kladrForm.kladrFormHand"));

        fillKladrValue(complexValue.getRegion(), "REGION", form, true);
        fillKladrValue(complexValue.getDistrict(), "DISTRICT", form, true);
        fillKladrValue(complexValue.getCity(), "CITY", form, true);
        fillKladrValue(complexValue.getTown(), "TOWN", form, false);
        fillKladrValue(complexValue.getStreet(), "STREET", form, true);
        fillKladrValue(complexValue.getHouse(), "HOUSE", form, true);
        fillKladrValue(complexValue.getBuilding1(), "BUILDING1", form, false);
        fillKladrValue(complexValue.getBuilding2(), "BUILDING2", form, false);
        fillKladrValue(complexValue.getApartment(), "APARTMENT", form, true);
        fillKladrValue(complexValue.getPost_index(), "POST_INDEX", form, true);

        formDiv.findElement(By.cssSelector("div.kladrButtons.kladrButtonsStep")).findElement(By.cssSelector("a.kladrButtonSave")).click();
        try {
            new WebDriverWait(this, 2).until(ExpectedConditions.not(ExpectedConditions.visibilityOf(formDiv)));
            //готово
        } catch (TimeoutException e) {
            List<WebElement> errorElements = getDisplayedList(formDiv, By.cssSelector("div.valueError"));
            Map<String, String> errorsMap = new LinkedHashMap<String, String>();
            for (WebElement elem : errorElements) {
                String name = elem.findElement(By.xpath("..")).findElement(By.cssSelector("div.inputClassic input")).getAttribute("name");
                errorsMap.put(name, elem.getText());
            }
            throw new ValidationException(this, errorsMap.toString(), e);
        }
    }

    private void fillKladrValue(String val, String name, WebElement form, boolean required) {
        WebElement input = form.findElement(By.name(name));
        if (isEmpty(val)) {
            if (required) {
                input.findElement(By.xpath("../../..")) //строка
                        .findElement(By.cssSelector("ul.checkListNew")).findElement(By.cssSelector("a.checkSelectNew")).click(); //чекбокс
            } else {
                //do nothing
            }
        } else {
            input.sendKeys(val);
        }

    }

    public void putValueInFIAS(Value value) {
        ComplexValue complexValue;
        if (value.getClass() == Value.class) {
            log.warn("Fias value as simple value! This may cause some troubles.");
            complexValue = ValueUtils.parseFIAS(value);
        } else {
            complexValue = (ComplexValue) value;
        }
        if (isEmpty(complexValue.getAddressString())) {
            complexValue.generateAddressString();
        }

        WebElement row = findElement(By.id(complexValue.getName()));
        //очищаем
        List<WebElement> list1 = row.findElements(By.className("clear-address"));
        if (list1.size() > 1)
            list1.get(0).click();
        else {
            //адрес не заполнен, do nothing
        }
        row.findElement(By.className("changeAddress")).click();
        WebElement formDiv = getDisplayed(By.cssSelector("div.kladrCont"));

        WebElement form = formDiv.findElement(By.cssSelector("form.fias"));
        WebElement input = form.findElement(By.cssSelector("div.row.complex_input"));

        WebElement addressString = getDisplayed(input, By.tagName("input"));
        addressString.clear();
        addressString.sendKeys(complexValue.getAddressString());
        try {
            //Ждем пока подгрузятся справочники
            Thread.sleep(2000);
            findElement(By.cssSelector("div.select-list li.active > a")).click();
        } catch (InterruptedException e) {
            throw new PortalException(this, "Interrupted!", e);
        } catch (TimeoutException e) {
            throw new PortalException(this, "Can't find any variants in FIAS widget for search string '" + complexValue.getAddressString() + "'", e);
        }
//        Actions bycicle = new Actions(this);
//        Action act = bycicle.moveToElement(addressString).moveByOffset(20,0).click().build();
//        act.perform();
        new WebDriverWait(this, 5).until(ExpectedConditions.visibilityOf(form.findElement(By.name("city"))));

        fillFiasValue(complexValue.getDistrict(), "area", form, false);
        fillFiasValue(complexValue.getCity(), "city", form, true);
        fillFiasValue(complexValue.getCityArea(), "cityArea", form, false);
        fillFiasValue(complexValue.getTown(), "place", form, false);
        fillFiasValue(complexValue.getStreet(), "street", form, false);
        fillFiasValue(complexValue.getAdditionalArea(), "additionalArea", form, false);
        fillFiasValue(complexValue.getAdditionalStreet(), "additionalStreet", form, false);
        fillFiasValue(complexValue.getHouse(), "house", form, true);
        fillFiasValue(complexValue.getBuilding1(), "building1", form, false);
        fillFiasValue(complexValue.getBuilding2(), "building2", form, false);
        fillFiasValue(complexValue.getApartment(), "apartment", form, true);
        if (isEmpty(complexValue.getPost_index()))
            fillFiasValue(complexValue.getPost_index(), "post_index", form, false);

        formDiv.findElement(By.name("save")).click();
        try {
            new WebDriverWait(this, 2).until(ExpectedConditions.not(ExpectedConditions.visibilityOf(formDiv)));
            //готово
        } catch (TimeoutException e) {
            List<WebElement> errorElements = getDisplayedList(formDiv, By.cssSelector("div.error-text"));
            Map<String, String> errorsMap = new LinkedHashMap<String, String>();
            for (WebElement elem : errorElements) {
                String name = elem.getAttribute("for");
                errorsMap.put(name, elem.getText());
            }
            throw new ValidationException(this, errorsMap.toString(), e);
        }
    }

    private void fillFiasValue(String val, String name, WebElement form, boolean required) {

        WebElement input = form.findElement(By.name(name));
        if (!isEmpty(input.getAttribute("readonly")) || !input.isDisplayed()) {
            log.warn("can't fill value " + name + ":" + val);
            return;
        }
        if (isEmpty(val)) {
            if (required) {
                input.findElement(By.xpath("../../..")) //строка
                        .findElement(By.cssSelector("a.checkbox")).click(); //чекбокс
            } else {
                //do nothing
            }
        } else {
            input.sendKeys(val);
        }

    }

    @Override
    public void putValueInRadio(Value value) {
        WebElement row = null;
        for (WebElement element : findElements(By.id(value.getName()))) {
            if (element.getTagName().equals("div")) {
                row = element;
                break;
            }
        }
        if (row == null)
            throw new PortalException(this, "Unable to find div with id " + value.getName());
        List<WebElement> variants = row.findElements(By.className("PGU-FieldRadioItem"));

        if (value.getValue() != null && !value.getValue().isEmpty()) {
            for (WebElement element : variants) {
                if (element.findElement(By.tagName("label")).getText().trim().equalsIgnoreCase(value.getValue().trim())) {
                    element.click();
                    return;
                }
            }
        } else if (value.getDicVal() != null && !value.getDicVal().isEmpty()) {
            for (WebElement element : variants) {
                if (element.findElement(By.tagName("input")).getAttribute("value").trim().equalsIgnoreCase(value.getDicVal().trim())) {
                    element.click();
                    return;
                }
            }
        } else
            throw new UnsupportedOperationException("value must be set! " + value.toString());
    }

    @Override
    public void putValueInCheckbox(Value value) {
        WebElement div = getElementWithId(value.getName(), "div");
        WebElement input = getElementWithId(value.getName(), "input");
        if (value.getValue() != null && (value.getValue().equalsIgnoreCase("true")
                || value.getValue().equalsIgnoreCase("да")
                || value.getValue().equalsIgnoreCase("yes")
        )) {
            if (input.getAttribute("value").equals("false"))
                div.click();
        } else if (value.getValue() != null && (value.getValue().equalsIgnoreCase("false")
                || value.getValue().equalsIgnoreCase("нет")
                || value.getValue().equalsIgnoreCase("no")
        )) {
            if (input.getAttribute("value").equals("true"))
                div.click();
        }
    }

    @Override
    public Map<String, String> sendApplication() throws UnsendException {
        try {
            findElement(By.id("__nextStep")).click();
            Map<String, String> mapWithErrors = getErrors();
            if (mapWithErrors.size() > 0) {
                throw new ValidationException(this, "Couldn't send application. Errors:\n" + mapWithErrors.toString());
            }
            new WebDriverWait(this, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='__nextStep']/span[contains(text(), 'Подать заявление')]"))).click();
            new WebDriverWait(this, 25).until(new WaitIfClockIsDisplayed());
        } catch (NoSuchElementException e) {
            log.info("Application probably already send");
        } catch (TimeoutException e) {
            log.info("Application probably already send");
        }
        try {
            new WebDriverWait(this, 5).until(ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(By.cssSelector("table.PGU-FormStepMenu"))));
        } catch (TimeoutException e) {
            try {
                try {
                    driver.findElement(By.id("__lkRedirect")).click();
                } catch (NoSuchElementException exc) {
                    int pageNum = getStepNum();

                    log.error("Error occured. Wanted to send application, instead process appeared on step " + pageNum);
                    Map<String, String> mapWithErrors = getErrors();
                    if (mapWithErrors.size() > 0) {
                        throw new ValidationException(this, "Couldn't send application. Errors:\n" + mapWithErrors.toString());
                    }
                    throw new PortalException(this, "Couldn't send application");
                }

            } catch (NoSuchElementException e1) {
                //everything fine. No errors found.
            }
        }

        String title = driver.getTitle();
        if (title.contains("Ошибка")) {
            String message = driver.findElement(By.cssSelector("h1 + p")).getText();
            throw new UnsendException(this, message);
        } else {
//                log.info(web.findElement(By.className("lastStepText")).getText());
        }
        Map<String, String> applicationInfo = new LinkedHashMap<String, String>();
        addApplicationInfo(applicationInfo);
        return applicationInfo;
    }


    private WebElement getElementWithId(String id, String tagName) {
        List<WebElement> elements = findElements(By.id(id));
        for (WebElement element : elements) {
            if (element.getTagName().equals(tagName)) {
                return element;
            }
        }
        return null;
    }

    @Override
    public void putValueInFile(Value value) throws IOException {
        String fileName = getFilePath(value);
        WebElement fileLoad =
                new WebDriverWait(this, 10).until(new OneOfElementsPresentsByChain(By.id(value.getName()), By.tagName("input")));
        fileLoad.sendKeys(fileName);
        (new WebDriverWait(driver, 30)).until(
                new ElementIsInvisible
                        (By.className("PGU-FieldUploadProgress"), findElement(By.id(value.getName()))));
            /*if (div.findElement(By.className("qq-upload-failed-text")).isDisplayed()) {
                throw new PortalException(this, "Произошла ошибка при загрузке файла");
            }*/
    }

    @Override
    public String getOrderId() {
        try {
            Object result = executeScript("return FormPlayer.env.orderId");
            if (result == null) {
                Thread.sleep(1000);
                result = executeScript("return FormPlayer.env.orderId");
            }
            if (result == null) {
                Thread.sleep(1000);
                result = executeScript("return FormPlayer.env.orderId");
            }
            return result.toString();
        } catch (Exception e) {
            log.warn("Can't get orderId in SF. Probably shouldn't use this function", e);
            return null;
        }
    }

    public String refreshPageAndGetStatus() {
        navigate().refresh();
        HashMap<String, String> map = new LinkedHashMap<String, String>();
        addApplicationInfo(map);
        return map.get("статус");
    }

    public String getStepDescription() {
        WebElement stepTitle = findElement(By.cssSelector("table.PGU-FormStepMenu"))
                .findElement(By.cssSelector("td.PGU-FormStepMenuItem.PGU-FormStepMenuItem-current"));
        return stepTitle.getText();
    }
}


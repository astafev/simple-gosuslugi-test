package ru.atc.gosuslugi.minitest.web.sir;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.atc.gosuslugi.minitest.web.portal.PortalDriver;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Date: 27.09.13
 * Author: astafev
 */
public class DriversQueueBroker implements Runnable{
    public static final Logger log = LoggerFactory.getLogger(DriversQueueBroker.class);
    BlockingQueue<PortalDriver> portalDrivers;
    BlockingQueue<SirDriver> sirDrivers;
    boolean direction;

    public DriversQueueBroker(BlockingQueue<PortalDriver> portalDrivers, BlockingQueue<SirDriver> sirDrivers, boolean direction) {
        this.portalDrivers = portalDrivers;
        this.sirDrivers = sirDrivers;
        this.direction = direction;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (direction) {
                    PortalDriver portalDriver = portalDrivers.take();
                    SirDriver sirDriver = SirDriver.createAndRun(portalDriver.getDriver());
                    SirDriver.log.debug("opening " + System.getProperty("ru.atc.SIR_URL"));

                    sirDrivers.put(sirDriver);
                } else {
                    SirDriver sir = sirDrivers.take();
                    portalDrivers.offer(new PortalDriver(sir.getDriver()), 10, TimeUnit.SECONDS);
                }
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                log.error("\nError occured!!!", e);
            }
        }
    }
}

package ru.atc.gosuslugi.minitest.web.sir.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import ru.atc.gosuslugi.minitest.web.sir.SirDriver;

import java.util.List;

/**
 * Date: 14.10.13
 * Author: astafev
 */
public abstract class AbstractSirFrame implements SearchContext{
    protected SirDriver driver;
    protected WebElement element;

    public AbstractSirFrame(WebElement element) {
        this.element = element;
    }

    public WebElement getElement() {
        return element;
    }

    public void setElement(WebElement element) {
        this.element = element;
    }

    public SirDriver getDriver() {
        return driver;
    }

    public void setDriver(SirDriver driver) {
        this.driver = driver;
    }

    @Override
    public List<WebElement> findElements(By by) {
        return element.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return element.findElement(by);
    }

    public void close() {
        driver.getDisplayed(this, By.className("x-tool-close")).click();
    }
}

<!DOCTYPE html>
<#-- @ftlvariable name="results" type="ru.atc.gosuslugi.minitest.result.Results" -->

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>${results.startDate?string("HH:mm dd.MM.yyyy")}</title>
</head>

<body>
<h1>тесты</h1>
<#if results.id?has_content>
    id: ${results.id}
</#if>
<br/>
${results.summaryTable}
<br/><br/>


<table rules="all" border="1">
    <thead>
    <tr>
        <th>Услуга</th>
        <th>Ведосмтво</th>
        <th>Информация о услуге</th>
        <th>Скриншот</th>
        <th>Завершающее действие</th>
        <th>Статус</th>
        <th>order ID</th>
        <th>Сообщение об ошибке</th>
        <th>stack trace</th>
        <th>additional info</th>
    </tr>
    </thead>
    <tbody>
    <#list results.results as resultInst>
        <#--<#if resultInst.service??>-->



    <tr>
        <#assign  service = resultInst.service!emptyService>

        <#if !resultInst.success>
            <#assign bckgrColor = "red"/>
        <#else>
            <#assign bckgrColor = "green"/>
        </#if>
        <td style="background-color:${bckgrColor};">${resultInst.serviceId!''}</td>
        <td>
            <#if resultInst.application?? && resultInst.application["ведомство"]??>${resultInst.application["ведомство"]}</#if>
        </td>
        <td>

            <#--<b>Услуга:</b> -->
            <a href="${resultInst.url!''}">
            <#if resultInst.application?? && resultInst.application["услуга"]??>
                ${resultInst.application["услуга"]}
            <#else>
                ${resultInst.url!''}
            </#if></a>
        </td>

        <#if !resultInst.success>
            <#assign noScreenshotMessage = "Не удалось снять скриншот. Возможно, проблема с браузером."/>
        <#else>
            <#assign noScreenshotMessage = "Услуга успешно отправлена!"/>
        </#if>
        <td>
            <#if resultInst.screenshotPath??>
                <a href="file:///${resultInst.screenshotPath}">${resultInst.url!'null'}</a>
            <#else>
            ${noScreenshotMessage}
            </#if>
        </td>
        <td>${resultInst.afterAction!'NOTHING'}</td>
        <td>${resultInst.status}</td>
        <td>${resultInst.orderId!''}</td>
        <td><#if resultInst.exception??>${resultInst.exception.message}<#else>Все ОК!</#if></td>
        <td><#if resultInst.exception??>
            <textarea rows="10" cols="70" readonly="true"><@printStackTrace resultInst.exception /></textarea></#if></td>
        <td>${resultInst.additionalInfo!''}</td>
    </tr>
       <#-- <#else>

        <tr>
            <td>${resultInst.serviceId!''}</td>
            <td colspan="9">Couldn't retrieve result</td>
            </tr>
        </#if>-->
    </#list>
    </tbody>
</table>

</body>
</html>

<#--Не готово. Слшком сложно, я обломался дописыва-->
<#macro summaryTable
results>
    <#assign successCounter = 0/>
    <#assign size = results?size/>
    <#list results as resultInst>
        <#if resultInst.success>
            <#assign successCounter = successCounter + 1 />
        </#if>
    </#list>

    <table rules="all" border="1">
        <thead>
            <tr>
                <th width="90">Услуга</th>
                <th width="90">Отправлено всего</th>
                <th width="90">успешно</th>
                <th width="90">с ошибками</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Всего</td>
                <td>${results?size}</td>
                <td>${successCounter}</td>
                <td>${results?size - successCounter}</td>
            </tr>
        </tbody>
    </table>

</#macro>

<#macro printStackTrace
exception>
${exception}
    <#list exception.stackTrace as stElem>
    at ${stElem}
    </#list>
    <#if exception.cause??>
        <@printStackTrace exception.cause/>
    </#if>
</#macro>
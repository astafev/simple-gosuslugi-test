<!DOCTYPE html>
<#-- @ftlvariable name="results" type="java.util.Map" -->
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>${startDate?string("HH:mm dd.MM.yyyy")}</title>
</head>

<body>
<h1>удаляем</h1>
<table rules="all" border="1">
    <thead>
    <tr>
        <th>input</th>
        <th>result</th>
        <th>exceptionMessage</th>
        <th>exception</th>
    </tr>
    </thead>
    <tbody>
    <#list results?keys as input>
    <tr>
        <#if !results[input].success>
            <#assign bckgrColor = "red"/>
        <#else>
            <#assign bckgrColor = "green"/>
        </#if>
        <td style="background-color:${bckgrColor};">${input}</td>

        <td>${results[input].status!'???'}</td>
        <td><#if results[input].exception??>${results[input].exception.message}<#else>Все ОК!</#if></td>
        <td><#if results[input].exception??>
            <textarea rows="10" cols="70" readonly="true"><@printStackTrace results[input].exception /></textarea></#if></td>
    </tr>
    </#list>
    </tbody>
</table>

</body>
</html>

<#macro printStackTrace
exception>
${exception}
    <#list exception.stackTrace as stElem>
    at ${stElem}
    </#list>
    <#if exception.cause??>
        <@printStackTrace exception.cause/>
    </#if>
</#macro>
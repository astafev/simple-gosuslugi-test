package ru.atc.gosuslugi.minitest.newtestreingeneer;


import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import ru.atc.gosuslugi.minitest.generated.values.ObjectFactory;
import ru.atc.gosuslugi.minitest.generated.values.Service;
import ru.atc.gosuslugi.minitest.generated.values.Services;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Date: 12.09.13
 * Author: astafev
 */
public class Start {
    public static final String USAGE = "";
    public static String directory;
    public static String file = "autotest.xml";
    public static int serviceId = 99999;


    public static void main(String[] args) throws SAXException, IOException, JAXBException {
        for (int i = 0; i < args.length; i++) {
            try {
                if (args[i].equals("-D")) {
                    directory = args[++i];
                }
            } catch (Exception e) {
                System.out.println(USAGE);
                return;
            }
        }

        InputStream is = new BufferedInputStream(ClassLoader.getSystemResourceAsStream(file));
        XMLReader xmlReader = XMLReaderFactory.createXMLReader();
        ObjectFactory objectFactory = new ObjectFactory();
        Services ss = objectFactory.createServices();
        Service service = objectFactory.createService();
        service.setServiceValues(objectFactory.createValuesType());
        service.setServiceid(serviceId);
        ss.getServices().add(service);
        service.setSf(true);
        xmlReader.setContentHandler(new Handler(service));

        xmlReader.parse(new InputSource(is));

        JAXBContext jaxbContext = JAXBContext
                .newInstance("ru.atc.gosuslugi.minitest.generated.values");
//        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Marshaller marsh = jaxbContext.createMarshaller();
        File file1 = new File(serviceId + ".xml");
        marsh.marshal(objectFactory.createServices(ss), file1);

        System.out.println(file1.getPath());

    }
}

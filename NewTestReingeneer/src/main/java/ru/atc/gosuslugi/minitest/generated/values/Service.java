
package ru.atc.gosuslugi.minitest.generated.values;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for Service complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Service">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceid">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="99999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="urls">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="userSelectedRegion" type="{}RegionIerarchy" minOccurs="0"/>
 *         &lt;element name="type" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="find"/>
 *               &lt;enumeration value="send"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="values" type="{}valuesType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="sf" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Service", propOrder = {
    "serviceid",
    "title",
    "description",
    "url",
    "urls",
    "userSelectedRegion",
    "type",
    "serviceValues"
})
public class Service {

    protected int serviceid;
    protected String title;
    protected String description;
    protected String url;
    protected Urls urls;
    protected RegionIerarchy userSelectedRegion;
    @XmlElement(defaultValue = "send")
    protected String type;
    @XmlElement(name = "values", required = true)
    protected ValuesType serviceValues;
    @XmlAttribute(name = "sf")
    protected Boolean sf;

    /**
     * Gets the value of the serviceid property.
     *
     */
    public int getServiceid() {
        return serviceid;
    }

    /**
     * Sets the value of the serviceid property.
     *
     */
    public void setServiceid(int value) {
        this.serviceid = value;
    }

    /**
     * Gets the value of the title property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the url property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the urls property.
     *
     * @return
     *     possible object is
     *     {@link ru.atc.gosuslugi.minitest.generated.values.Service.Urls }
     *
     */
    public Urls getUrls() {
        return urls;
    }

    /**
     * Sets the value of the urls property.
     *
     * @param value
     *     allowed object is
     *     {@link ru.atc.gosuslugi.minitest.generated.values.Service.Urls }
     *
     */
    public void setUrls(Urls value) {
        this.urls = value;
    }

    /**
     * Gets the value of the userSelectedRegion property.
     *
     * @return
     *     possible object is
     *     {@link ru.atc.gosuslugi.minitest.generated.values.RegionIerarchy }
     *
     */
    public RegionIerarchy getUserSelectedRegion() {
        return userSelectedRegion;
    }

    /**
     * Sets the value of the userSelectedRegion property.
     *
     * @param value
     *     allowed object is
     *     {@link ru.atc.gosuslugi.minitest.generated.values.RegionIerarchy }
     *
     */
    public void setUserSelectedRegion(RegionIerarchy value) {
        this.userSelectedRegion = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the serviceValues property.
     *
     * @return
     *     possible object is
     *     {@link ru.atc.gosuslugi.minitest.generated.values.ValuesType }
     *
     */
    public ValuesType getServiceValues() {
        return serviceValues;
    }

    /**
     * Sets the value of the serviceValues property.
     *
     * @param value
     *     allowed object is
     *     {@link ru.atc.gosuslugi.minitest.generated.values.ValuesType }
     *     
     */
    public void setServiceValues(ValuesType value) {
        this.serviceValues = value;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id: " + getServiceid());
        sb.append(" url: " + getUrl());
        sb.append("\nregion: " + getUserSelectedRegion());
        sb.append("\nvalues: " + getServiceValues());
        return sb.toString();
    }

    /**
     * Gets the value of the sf property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSf() {
        if (sf == null) {
            return false;
        } else {
            return sf;
        }
    }

    /**
     * Sets the value of the sf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSf(Boolean value) {
        this.sf = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "url"
    })
    public static class Urls {

        @XmlElement(required = true)
        protected List<String> url;

        /**
         * Gets the value of the url property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the url property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUrl().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getUrl() {
            if (url == null) {
                url = new ArrayList<String>();
            }
            return this.url;
        }

    }

}

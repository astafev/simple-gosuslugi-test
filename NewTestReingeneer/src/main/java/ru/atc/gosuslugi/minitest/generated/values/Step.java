
package ru.atc.gosuslugi.minitest.generated.values;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for Step complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Step">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="value" type="{}Value" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="numStep">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *             &lt;minExclusive value="0"/>
 *             &lt;maxExclusive value="30"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Step", propOrder = {
    "description",
    "stepValues"
})
public class Step {

    protected String description;
    @XmlElement(name = "value")
    protected List<Value> stepValues;
    @XmlAttribute(name = "numStep")
    protected Byte numStep;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the stepValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stepValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStepValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ru.atc.gosuslugi.minitest.generated.values.Value }
     * 
     * 
     */
    public List<Value> getStepValues() {
        if (stepValues == null) {
            stepValues = new ArrayList<Value>();
        }
        return this.stepValues;
    }

    /**
     * Gets the value of the numStep property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getNumStep() {
        return numStep;
    }

    /**
     * Sets the value of the numStep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setNumStep(Byte value) {
        this.numStep = value;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nnumStep: " + getNumStep());
        sb.append("\n    values: " + getStepValues());
        return sb.toString();
    }

}

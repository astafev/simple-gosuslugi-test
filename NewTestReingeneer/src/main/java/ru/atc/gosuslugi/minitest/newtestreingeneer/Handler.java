package ru.atc.gosuslugi.minitest.newtestreingeneer;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;
import ru.atc.gosuslugi.minitest.generated.values.Service;
import ru.atc.gosuslugi.minitest.generated.values.Step;
import ru.atc.gosuslugi.minitest.generated.values.Value;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Date: 12.09.13
 * Author: astafev
 */
class Handler extends DefaultHandler2 {
    final Service service;
    StringBuilder sb;
    Step step;
    Value value;
    boolean inset = false;
    byte stepNum = 1;

    String localName;

    public Handler(Service service) {
        this.service = service;
    }


    @Override
    public void startDocument()
            throws SAXException {


    }

    @Override
    public void startElement(String namespaceURI,
                             String localName,
                             String qName,
                             Attributes atts)
            throws SAXException {
        sb = new StringBuilder();
        switch (localName) {
            case "serviceUrl":
                sb.append("https://66.gosuslugi.ru");
                break;
            case "set":
                step = new Step();
                step.setNumStep(stepNum++);
                inset = true;
                break;
            case "FieldDropdown":
                value = new Value();
                for(int i = 0 ; i<atts.getLength(); i++) {
                    if(atts.getLocalName(i).equals("id")) {
                        value.setName(atts.getValue(i));
                        break;
                    }
                }
                value.setType("fieldDropDown");
                break;

            case "FieldText":
            case "FieldTextArea":
                value = createNewValue(atts);
                value.setType("text");
                break;
            case "FieldTextDate":
                value = createNewValue(atts);
                value.setType("date");
                break;
            case "FieldCheckbox":
                value = createNewValue(atts);
                value.setType("checkbox");
                break;
            case "FieldRadio":
                value = createNewValue(atts);
                value.setType("radio");
                break;
            case "FieldUpload":
                value = createNewValue(atts);
                value.setType("file");
                break;
        }
    }
    private Value createNewValue(Attributes atts) {
        Value val = new Value();

        for(int i = 0 ; i<atts.getLength(); i++) {
            if(atts.getLocalName(i).equals("id")) {
                val.setName(atts.getValue(i));
                break;
            }
        }
        return val;
    }




    @Override
    public void characters (char ch[], int start, int length){
        if(sb!=null) {
            sb.append(ch, start, length);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        switch (localName) {
            case "serviceUrl":
                service.setUrls(new Service.Urls());
                service.getUrls().getUrl().add(sb.toString());
                break;
            case "FieldDropdown":
            case "FieldRadio":
            case "FieldText":
            case "FieldTextArea":
            case "FieldCheckbox":
            case "FieldTextDate":
                if (inset) {
                    step.getStepValues().add(value);
                    value = null;
                } else System.err.println(localName + " not inset");
                break;
            case "set":
                service.getServiceValues().getSteps().add(step);
                inset = false;
                break;
            case "value":
                if(value.getType().equals("fieldDropDown") || value.getType().equals("radio"))
                    value.setDicVal(sb.toString());
                else if(value.getType().equals("file")) {

                } else value.setValue(sb.toString());
                break;
            case "text_value":
                if(value.getType().equals("fieldDropDown") || value.getType().equals("radio"))
                    value.setValue(sb.toString());

        }
    }

    @Override
    public void endDocument() {
    }
}
